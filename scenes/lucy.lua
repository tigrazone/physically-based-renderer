Film:Image(512, 512)

Camera:Perspective(90)--,0,0,1.75,0.01)
--Camera:SetTransform(Transform:New():LookAt({0,9,15},{0,-2,15},{0,0,-1}))
Camera:SetTransform(Transform:New():LookAt({0.75,1.75,1.1},{0.2,1.5,0},{0,1,0}))
Integrator:Path(7)
Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0,0,10):Scale(25,1,25),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Glass({0.75,0.8,0.9},{0.75,0.8,0.9},1.492)
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(1,0.9,0):RotateY(math.rad(180)):RotateX(math.rad(90)):Scale(0.0015,0.0015,0.0015), "geometry/lucy.ply"),
		Material:Matte(0.8, 1.6)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/forest.exr",1),100)
	):SetTransform(Transform:New():RotateX(math.rad(90)))--]]
	--[[
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(-2, 10, 0):Scale(5, 1, 5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 2, 1,
			0, 3, 2
		}),
		AreaLight:Diffuse(10)
	)--]]
}))