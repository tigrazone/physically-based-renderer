Film:Image(960, 540)

Camera:Perspective(50)
Camera:SetTransform(Transform:New():LookAt({0,4,5},{0,1.85,0},{0,1,0}))
			
require("geometry/happybuddha")

Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0,0,0):Scale(30,40,4),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5,
			-0.5, 1.0, -2,
			0.5, 1.0, -2
		},
		{
			0, 1, 2,
			0, 2, 3,
			3, 2, 5,
			3, 4, 5
		}),
		Material:Matte(1, 1.8)
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,-1.0,0):Scale(20,20,20), happybuddhaPts, happybuddhaVert), Material:Glossy({Color.RGB, {0.1, 0.002, 0.001}}, 0.2, 0.1, 0.1, {0.6,0.8,0.9}, 3)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/grace-new_latlong.exr"))
	):SetTransform(Transform:New():RotateX(math.rad(90)))
}))