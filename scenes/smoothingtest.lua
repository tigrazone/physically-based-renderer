Film:Image(1920, 1080)

Camera:Perspective(45)
--Camera:SetTransform(Transform:New():LookAt({0,10,0},{0,0,0},{0,0,-1}))
Camera:SetTransform(Transform:New():LookAt({0,4,6},{0,1,1},{0,1,0}))
Integrator:Path(4)

Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Scale(50,1,50),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Matte({0.02,0.12,0.2})
	),--[[
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(1,0.85,3):RotateY(math.rad(250)):RotateX(math.rad(90)):Scale(0.0015,0.0015,0.0015), "geometry/lucy.ply"), Material:Plastic(0.5, 0.25, 0.001)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,0,0):RotateY(math.rad(120)):RotateX(math.rad(90)):Scale(0.5,0.5,0.5), "geometry/teapot.ply", 0, 2, 0), Material:Glass(1,1,1.3)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(-2.5,0,0):RotateY(math.rad(120)):RotateX(math.rad(90)):Scale(0.5,0.5,0.5), "geometry/teapot.ply", 0, 1, 0), Material:SchlickMetal("spds/metals/Ag.k.spd", "spds/metals/Ag.eta.spd",.7)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(2.5,0,0):RotateY(math.rad(120)):RotateX(math.rad(90)):Scale(0.5,0.5,0.5), "geometry/teapot.ply", 0, 2, 0), Material:Substrate(0.8,0.2,0.00001, 0)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/ennis.exr",0.5),500)
	):SetTransform(Transform:New():RotateY(math.rad(180)):RotateX(math.rad(90)))--]]
	--[[
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(-2, 10, 0):Scale(5, 1, 5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 2, 1,
			0, 3, 2
		}),
		AreaLight:Diffuse(10)
	)--]]
}))