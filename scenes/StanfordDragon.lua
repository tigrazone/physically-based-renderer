Film:Image(1920, 1080)

Camera:Perspective(25)
Camera:SetTransform(Transform:New():LookAt({0,7.5,7},{0,0,1},{0,1,0}))

Integrator:Path(3)
require("geometry/dragon")
Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Scale(25,1,25),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 3, 2
		},
		{
			0,0,
			1,0,
			1,1,
			0,1
		}),
		Material:Matte(0.75)
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,-1.2,2):Scale(20,20,20), dragonPts, dragonVert, 0, 2),
		Material:Metal("spds/metals/au.k.spd", "spds/metals/au.eta.spd", 0.01)
	)):SetAccelerator(Accelerator.BINNEDKDTREE),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/ueno.exr"),500)
	):SetTransform(Transform:New():RotateX(math.rad(90)))--]]
--[[
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(-2, 10, 0):Scale(5, 1, 5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 2, 1,
			0, 3, 2
		}),
		AreaLight:Diffuse(10)
	)--]]
}))