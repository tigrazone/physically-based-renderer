Film:Image(512, 512)

Camera:Perspective(30)
Camera:SetTransform(Transform:New():LookAt({-5,10,20},{0,0,0},{0,1,0}))

require("geometry/happybuddha")
Scene:AddChildren(SceneNode:AnimatedNode
({

	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Scale(25,0,25),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Metal(1,1,0.00002)
	),--]]
	
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0,-1.1,0):RotateY(math.rad(10)):Scale(20,20,20), happybuddhaPts, happybuddhaVert), Material:Metal(1,1,0.002)--({Color.RGB, {0.1, 1.0, 0.6}},{Color.RGB, {0.1, 1.0, 0.6}},1.5)
	)),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(EnvMap.VERTICALCROSS, TextureInfo:New("textures/envmaps/uffizi_cross.exr", 0.2))
	):SetTransform(Transform:New():RotateX(math.rad(90)))
}))