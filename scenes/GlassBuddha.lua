Film:Image(960, 540)

Camera:Perspective(80)
Camera:SetTransform(Transform:New():LookAt({0,5,4},{0,0,-1},{0,1,0}))
Sampler:Random()
require("geometry/happybuddha")

Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Scale(200,0,200),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 2, 3
		}),
		Material:Matte(1)
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0.1,-1.1,0):RotateY(math.rad(10)):Scale(20,20,20), happybuddhaPts, happybuddhaVert), Material:Glass({Color.RGB, {0.1, 1.0, 0.6}},{Color.RGB, {0.1, 1.0, 0.6}},1.5)
	)),--]]
	SceneNode:PointLight
	(
		{0, 3, -5},
		20
	)--]]
	--[[SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0, 7, -5):Scale(5, 1, 5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 2, 1,
			0, 3, 2
		}),
		AreaLight:Diffuse(5)
	)--]]
}))