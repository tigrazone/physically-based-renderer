Film:Image(512, 512)

Camera:Perspective(60,0,0,3.4,0.0275)
--Camera:SetTransform(Transform:New():LookAt({0,10,0},{0,0,0},{0,0,-1}))
Camera:SetTransform(Transform:New():LookAt({0,1.25,5},{0,0.7,0},{0,1,0}))
Integrator:Bidir(3,4)
Scene:AddChildren(SceneNode:AnimatedNode
({
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh
		(
			Transform:New():Translate(0,0,1):Scale(5,1,5),
		{
			-0.5, 0.0, 0.5,
			0.5, 0.0, 0.5,
			0.5, 0.0, -0.5,
			-0.5, 0.0, -0.5
		},
		{
			0, 1, 2,
			0, 3, 2
		},
		{
			0,1.5,
			1.5,1.5,
			1.5,0,
			0,0
		}),
		--Material:Glossy({0.4, 0.001, 0.0005},1,0.1,0.1,{0,0.5,0.5},2.5,1.5)
		Material:Substrate({0.4, 0.001, 0.0005},1,0.01,0.01,{0,0.5,0.5},2.5,1.5)
		--Material:Substrate(SpectrumTexture:Image(TextureInfo:New("textures/checkerboard.exr",1)),SpectrumTexture:Image(TextureInfo:New("textures/checkerboard.exr",1)),0.01,0.01)
	),
	SceneNode:AnimatedNode(
	SceneNode:GeometricNode
	(
		Shape:TriangleMesh(Transform:New():Translate(0.25,0.8,0.5):RotateY(math.rad(125)):Scale(0.02,0.02,0.02), "geometry/xyzrgb_dragon.ply", 0, 2), Material:SchlickMetal("spds/metals/Au.k.spd", "spds/metals/Au.eta.spd", 0.2, 0.1)
	)),--]]
	SceneNode:AnimatedNode(
		SceneNode:InfiniteAreaLight(TextureInfo:New("textures/envmaps/ueno.exr",1,0.8),500)
	):SetTransform(Transform:New():RotateY(math.rad(90)):RotateX(math.rad(90)))--]]
}))