/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "API.h"
#include "gui/RenderWindow.h"

int main()
{
	Render::LoadOptions();

	getcwd(Render::rootdir, 4096);
	Render::RenderWindow *window = NULL;
	try
	{
		window = new Render::RenderWindow();
		window->Run();
	}
	catch(int)
	{
	}
	chdir(Render::rootdir);
	Render::SaveOptions();
	delete window;
}
