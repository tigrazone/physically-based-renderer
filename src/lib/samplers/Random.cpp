/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "samplers/Random.h"

namespace Render
{
	RandomSampler::RandomSampler(unsigned int width, unsigned int height)
		:	Sampler(width, height)
	{
	}
	float RandomSampler::GetSample(Sample *sample, uint32_t offset, unsigned int x, unsigned int y) const
	{
		return sample->rng.RandF();
	}
	float RandomSampler::GetSample(Sample *sample, uint32_t offset) const
	{
		return sample->rng.RandF();
	}
	bool RandomSampler::GetNextSample(Sample *sample, SampleRegionData *region) const
	{
		if(region->data[2] >= region->data[3])
		{
			region->data[2] = 0;
			region->xPos = region->data[0];
			region->yPos = region->data[1];
			return false;
		}

		RNG *rng = &sample->rng;
		sample->x = region->xPos + rng->RandF();
		sample->y = region->yPos + rng->RandF();
		sample->u = rng->RandF();
		sample->v = rng->RandF();

		++region->data[2];
		++region->xPos;
		if(region->xPos >= width)
		{
			region->xPos = 0;
			++region->yPos;
		}

		return true;
	}
}