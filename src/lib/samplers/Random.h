/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_SAMPLERS_RANDOM_H
#define RENDERLIB_SAMPLERS_RANDOM_H

#include "Sampler.h"

namespace Render
{
	class RandomSampler : public Sampler
	{
	public:
		RandomSampler(unsigned int width, unsigned int height);
		float GetSample(Sample *sample, uint32_t offset, unsigned int x, unsigned int y) const;
		float GetSample(Sample *sample, uint32_t offset) const;
		bool GetNextSample(Sample *sample, SampleRegionData *region) const;
	};
}

#endif