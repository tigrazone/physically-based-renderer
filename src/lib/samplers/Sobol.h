/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_SAMPLERS_SOBOL_H
#define RENDERLIB_SAMPLERS_SOBOL_H

#include "Sampler.h"
#define SOBOL_BITS 32
#define SOBOL_MAX_NUMBER 32
#define SOBOL_MAX_DIMENSIONS 21201

namespace Render
{
	class SobolSampler : public Sampler
	{
		struct SamplerData
		{
			uint32_t pass;
			float rng0, rng1;
		};
		static void GenerateDirectionVectors(uint32_t *vectors, uint32_t dimensions);
		unsigned int dimensions;
		uint32_t *directions;
		SamplerData *data;
		float sobol(uint32_t index, uint32_t d) const;
	public:
		SobolSampler(unsigned int width, unsigned int height);
		~SobolSampler();
		void RequestSamples(const unsigned int *d, const unsigned int *dDecorrelated, unsigned int *offsets, unsigned int *offsetsDec, size_t n);
		float GetSample(Sample *sample, uint32_t offset, unsigned int x, unsigned int y) const;
		float GetSample(Sample *sample, uint32_t offset) const;
		bool GetNextSample(Sample *sample, SampleRegionData *region) const;
		void IncrementPass(void *samplerData) const;
	};
}

#endif