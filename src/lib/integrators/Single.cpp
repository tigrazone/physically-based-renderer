/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "integrators/Single.h"

namespace Render
{
	SingleIntegrator::SingleIntegrator(float stepSize) : stepSize(stepSize){}
	
	void SingleIntegrator::requiredSamples(const Sampler *sampler, const Scene *scene, unsigned int *n, unsigned int *nDecorrelated)
	{
		*n = 2;
		*nDecorrelated = 0;
	}
	void SingleIntegrator::setSampleOffsets(unsigned int n, unsigned int nDecorrelated)
	{
		sampleOffset = n;
	}
	void SingleIntegrator::Li(const Renderer *renderer, const Scene *scene, Sample *sample, const Ray &ray, Spectrum &L, Spectrum &T, MemoryArena &arena) const
	{
		T = 1.f;
		float t0, t1;
		if(!scene->volume || !scene->volume->IntersectP(ray, &t0, &t1) || t0 == t1)
			return;

		L = 0.f;
		int nSamples = Ceil2Int((t1 - t0) / stepSize);
		float step = (t1 - t0) / nSamples;
		Point p = ray(t0), pPrev;
		Vector w = -ray.d;
		t0 += sample->sampler->GetSample(sample, sampleOffset + 1) * step;
		for(int i = 0; i < nSamples; ++i, t0 += step)
		{
			pPrev = p;
			p = ray(t0);
			T *= Exp(-scene->volume->Tau(Ray(pPrev, p - pPrev, 0.f, 1.f, ray.time)));

			L += T * scene->volume->Lve(p, w, ray.time);
		}
		L *= step;
	}
	void SingleIntegrator::Transmittance(const Renderer *renderer, const Scene *scene, Sample *sample, const Ray &ray, Spectrum &T, MemoryArena &arena, bool useSample) const
	{
		if(!scene->volume) return;

		float off = useSample ? sample->sampler->GetSample(sample, sampleOffset) : sample->rng.RandF();
		T *= Exp(-scene->volume->Tau(ray, stepSize, off));
	}
}