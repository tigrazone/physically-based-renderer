/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "TriangleMesh.h"
#include "MonteCarlo.h"
#include "geometryparsers/PLY.h"

namespace Render
{
	void ReadMeshGeometry(const string &filename, unsigned int *nV, unsigned int *nT, unsigned int **vi, Point **v, float **uv, Normal **n, Vector **s)
	{
		if(filename.size() > 4 && EndsWithIgnoreCase(filename, ".ply"))
			ReadPLY(filename.c_str(), nV, nT, vi, v, uv, n, s);
		else
		{
			*nV = 0; *nT = 0; *vi = 0; *v = 0;
			Error("Unsupported type \"%s\".", STRNAMESPACE, "ReadImage", 1, filename.c_str());
		}
	}

	Normal *GetSmoothNormals(const Point *v, const unsigned int *vi, unsigned int nV, unsigned int nT, unsigned int smooth, const float *uv)
	{
		if(smooth == 0)
			return NULL;

		Normal *n = (Normal *) AllocAligned(sizeof(Normal) * nV);
		memset(n, 0, sizeof(Normal) * nV);
		float *weights = new float[nV];
		memset(weights, 0, sizeof(float) * nV);
		bool scaleArea = (smooth & 0x2) != 0;
		for(unsigned int i = 0; i < nT; ++i)
		{
			const Point &p0 = v[vi[i * 3]];
			const Point &p1 = v[vi[i * 3 + 1]];
			const Point &p2 = v[vi[i * 3 + 2]];

			Vector e1 = p1 - p0;
			Vector e2 = p2 - p0;
			Normal ni = Cross(e1, e2);
			if(uv)
			{
				float uv0[2] = {uv[vi[i * 3] * 2], uv[vi[i * 3] * 2 + 1]};
				float uv1[2] = {uv[vi[i * 3 + 1] * 2], uv[vi[i * 3 + 1] * 2 + 1]};
				float uv2[2] = {uv[vi[i * 3 + 2] * 2], uv[vi[i * 3 + 2] * 2 + 1]};
				float du1 = uv1[0] - uv0[0];
				float du2 = uv2[0] - uv0[0];
				float dv1 = uv1[1] - uv0[1];
				float dv2 = uv2[1] - uv0[1];
				float det = du1 * dv2 - du2 * dv1;
				if(det != 0.f)
				{
					det = 1.f / det;
					Vector dpdu = det * (dv2 * e1 - dv1 * e2);
					Vector dpdv = det * (du1 * e2 - du2 * e1);
					ni = Cross(dpdu, dpdv);
				}
			}
			float length = ni.Length();
			if(length <= 0.f)
				continue;
			float weight = scaleArea ? length * 0.5f : 1.f;
			ni /= length;
			n[vi[i * 3]] += ni * weight;
			n[vi[i * 3 + 1]] += ni * weight;
			n[vi[i * 3 + 2]] += ni * weight;
			weights[vi[i * 3]] += weight;
			weights[vi[i * 3 + 1]] += weight;
			weights[vi[i * 3 + 2]] += weight;
		}

		for(unsigned int i = 0; i < nV; ++i)
			n[i] /= weights[i];

		delete[] weights;

		return n;
	}

	Mesh::Mesh(Point *v, unsigned int *vi, float *uv, Normal *n, Vector *s)
		:	v(v), vi(vi), uv(uv), n(n), s(s)
	{
	}
	Mesh::~Mesh()
	{
		FreeAligned(v);
		FreeAligned(vi);
		FreeAligned(uv);
		FreeAligned(n);
		FreeAligned(s);
		if(!refinedShapes.empty())
			FreeAligned(refinedShapes.front());
	}
	BBox Mesh::Bounds() const
	{
		BBox b;
		for(unsigned int i = 0; i < refinedShapes.size(); ++i)
			b = Union(b, refinedShapes[i]->Bounds());

		return b;
	}
	bool Mesh::CanIntersect() const
	{
		return false;
	}
	void Mesh::Refine(vector<const Shape *> &refined) const
	{
		refined.insert(refined.end(), refinedShapes.begin(), refinedShapes.end());
	}

	Triangle::Triangle(const Mesh *mesh, unsigned int *vi)
		:	mesh(mesh), vi(vi)
	{
	}
	BBox Triangle::Bounds() const
	{
		const Point &p0 = mesh->v[vi[0]];
		const Point &p1 = mesh->v[vi[1]];
		const Point &p2 = mesh->v[vi[2]];
		return Union(BBox(p0, p1), p2);
	}
	bool Triangle::Intersect(const Ray &ray, Intersection *isect) const
	{
		const Point &p0 = mesh->v[vi[0]];
		const Point &p1 = mesh->v[vi[1]];
		const Point &p2 = mesh->v[vi[2]];
		Vector e1 = p1 - p0;
		Vector e2 = p2 - p0;

		Vector s1 = Cross(ray.d, e2);
		float divisor = Dot(s1, e1);
		if(divisor == 0.f)
			return false;
		
		divisor = 1.f / divisor;

		Vector s = ray.o - p0;
		float b1 = Dot(s, s1) * divisor;
		if(b1 < 0.f || b1 > 1.f)
			return false;

		Vector s2 = Cross(s, e1);
		float b2 = Dot(s2, ray.d) * divisor;
		if(b2 < 0.f || b2 + b1 > 1.f)
			return false;

		float t = Dot(s2, e2) * divisor;
		if(t <= ray.mint || t > ray.maxt)
			return false;

		ray.maxt = t;
		isect->primitiveData[0] = b1;
		isect->primitiveData[1] = b2;
		isect->toWorld.Identity();

		return true;
	}
	bool Triangle::IntersectP(const Ray &ray) const
	{
		const Point &p0 = mesh->v[vi[0]];
		const Point &p1 = mesh->v[vi[1]];
		const Point &p2 = mesh->v[vi[2]];
		Vector e1 = p1 - p0;
		Vector e2 = p2 - p0;

		Vector s1 = Cross(ray.d, e2);
		float divisor = Dot(s1, e1);
		if(divisor == 0.f)
			return false;
		
		divisor = 1.f / divisor;

		Vector s = ray.o - p0;
		float b1 = Dot(s, s1) * divisor;
		if(b1 < 0.f || b1 > 1.f)
			return false;

		Vector s2 = Cross(s, e1);
		float b2 = Dot(s2, ray.d) * divisor;
		if(b2 < 0.f || b2 + b1 > 1.f)
			return false;

		float t = Dot(s2, e2) * divisor;
		if(t < ray.mint || t > ray.maxt)
			return false;

		return true;
	}
	Point Triangle::Sample(float u0, float u1, Intersection *isect) const
	{
		float b1, b2;
		UniformSampleTriangle(u0, u1, &b1, &b2);
		
		const Point &p0 = mesh->v[vi[0]];
		const Point &p1 = mesh->v[vi[1]];
		const Point &p2 = mesh->v[vi[2]];
		Vector e1 = p1 - p0;
		Vector e2 = p2 - p0;

		if(isect)
		{
			isect->primitiveData[0] = b1;
			isect->primitiveData[1] = b2;
			isect->shape = this;
		}
		return (1.f - b2 - b1) * p0 + b1 * p1 + b2 * p2;
	}
	float Triangle::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		const Point &p0 = mesh->v[vi[0]];
		const Point &p1 = mesh->v[vi[1]];
		const Point &p2 = mesh->v[vi[2]];
		Vector e1 = p1 - p0;
		Vector e2 = p2 - p0;

		Vector s1 = Cross(ray.d, e2);
		float divisor = Dot(s1, e1);
		if(divisor == 0.f)
			return 0.f;
		
		divisor = 1.f / divisor;

		Vector s = ray.o - p0;
		float b1 = Dot(s, s1) * divisor;
		if(b1 < 0.f || b1 > 1.f)
			return 0.f;

		Vector s2 = Cross(s, e1);
		float b2 = Dot(s2, ray.d) * divisor;
		if(b2 < 0.f || b2 + b1 > 1.f)
			return 0.f;
		
		float t = Dot(s2, e2) * divisor;
		if(t > ray.mint && t < ray.maxt)
		{
			rayWorld.maxt = ray.maxt = t;
			if(isect)
			{
				isect->primitiveData[0] = b1;
				isect->primitiveData[1] = b2;
				isect->shape = this;
			}
		}

		float b0 = 1.f - b2 - b1;
		if(mesh->n)
		{
			Normal ns = Normalize(toWorld->operator()( b0 * mesh->n[vi[0]] + b1 * mesh->n[vi[1]] + b2 * mesh->n[vi[2]] ));
			return Shape::Pdf(rayWorld.o, rayWorld.d, rayWorld(t), ns);
		}

		float uv0[2], uv1[2], uv2[2];
		GetUVs(uv0, uv1, uv2);

		float du1 = uv1[0] - uv0[0];
		float du2 = uv2[0] - uv0[0];
		float dv1 = uv1[1] - uv0[1];
		float dv2 = uv2[1] - uv0[1];
		float det = du1 * dv2 - du2 * dv1;

		if(det == 0.f)
			return Shape::Pdf(rayWorld.o, rayWorld.d, rayWorld(t), Normalize(Cross(e1,e2)));
		
		det = 1.f / det;
		Vector dpdu = det * (dv2 * e1 - dv1 * e2);
		Vector dpdv = det * (du1 * e2 - du2 * e1);
		
		return Shape::Pdf(rayWorld.o, rayWorld.d, rayWorld(t), Normalize(Cross(dpdu,dpdv)));
	}
	void Triangle::GetDifferentialGeometry(const Ray &ray, Intersection *isect, const Transform *toWorld) const
	{
		if(!toWorld)
			toWorld = &isect->toWorld;
		float uv0[2], uv1[2], uv2[2];
		GetUVs(uv0, uv1, uv2);

		Vector &dpdu = isect->dg.dpdu, &dpdv = isect->dg.dpdv;
		{
			float du1 = uv1[0] - uv0[0];
			float du2 = uv2[0] - uv0[0];
			float dv1 = uv1[1] - uv0[1];
			float dv2 = uv2[1] - uv0[1];
			float det = du1 * dv2 - du2 * dv1;
			const Point &p0 = mesh->v[vi[0]];
			const Point &p1 = mesh->v[vi[1]];
			const Point &p2 = mesh->v[vi[2]];
			Vector e1 = p1 - p0;
			Vector e2 = p2 - p0;
			if(det == 0.f)
			{
				isect->ng = Normalize(toWorld->operator()(Cross(e1, e2)));
				CoordinateSystem((Vector) isect->ng, &dpdu, &dpdv);
			}
			else
			{
				det = 1.f / det;
				dpdu = toWorld->operator()(det * (dv2 * e1 - dv1 * e2));
				dpdv = toWorld->operator()(det * (du1 * e2 - du2 * e1));
				isect->ng = Normalize(Cross(dpdu, dpdv));
			}
		}
		
		float b1 = isect->primitiveData[0], b2 = isect->primitiveData[1], b0 = 1.f - b2 - b1;
		Normal &ns = isect->dg.n;
		ns = mesh->n ? Normalize(toWorld->operator()( b0 * mesh->n[vi[0]] + b1 * mesh->n[vi[1]] + b2 * mesh->n[vi[2]] )) : isect->ng;
		/*
		Normal ns = isect->ng;
		Normal dndu, dndv;
		
		if(mesh->n)
		{
			ns = Normalize(toWorld->operator()( b0 * mesh->n[vi[0]] + b1 * mesh->n[vi[1]] + b2 * mesh->n[vi[2]] ));
			if(det != 0.f)
			{
				Normal dn1 = mesh->n[vi[1]] - mesh->n[vi[0]];
				Normal dn2 = mesh->n[vi[2]] - mesh->n[vi[0]];
				toWorld->operator()(det * (dv2 * dn1 - dv1 * dn2), &dndu);
				toWorld->operator()(det * (du1 * dn2 - du2 * dn1), &dndv);
			}
		}
		*/
		if(mesh->s)
		{
			dpdu = Normalize(toWorld->operator()( b0 * mesh->s[vi[0]] + b1 * mesh->s[vi[1]] + b2 * mesh->s[vi[2]]));
			dpdv = Cross(dpdu, ns);
			dpdu = Cross(ns, dpdv);
		}
		else if(mesh->uv)
		{
			float uu = dpdu.Length();
			float vv = dpdv.Length();
			Vector ts = Normalize(Cross(ns, dpdv));
			dpdu = Cross(ts, ns);
			if(Dot(dpdv, ts) < 0.f)
				ts *= -1.f;
			dpdv = ts;

			dpdu *= uu;
			dpdv *= vv;
		}
		else if(mesh->n)
			CoordinateSystem((Vector) ns, &dpdu, &dpdv);

		isect->dg.p = ray(ray.maxt);
		isect->dg.u = b0 * uv0[0] + b1 * uv1[0] + b2 * uv2[0];
		isect->dg.v = b0 * uv0[1] + b1 * uv1[1] + b2 * uv2[1];
	}
	/*
	void Triangle::GetDifferentialGeometry(const RayDifferential &ray, Intersection *isect, DifferentialGeometry *dgShading, const Transform *toWorld) const
	{
		if(!toWorld)
			toWorld = &isect->toWorld;
		float uv0[2], uv1[2], uv2[2];
		GetUVs(uv0, uv1, uv2);

		float du1 = uv1[0] - uv0[0];
		float du2 = uv2[0] - uv0[0];
		float dv1 = uv1[1] - uv0[1];
		float dv2 = uv2[1] - uv0[1];
		float det = du1 * dv2 - du2 * dv1;

		Vector dpdu, dpdv;
		{
			const Point &p0 = mesh->v[vi[0]];
			const Point &p1 = mesh->v[vi[1]];
			const Point &p2 = mesh->v[vi[2]];

			Vector e1 = p1 - p0;
			Vector e2 = p2 - p0;
			if(det == 0.f)
				CoordinateSystem(Normalize(Cross(e2, e1)), &dpdu, &dpdv);
			else
			{
				det = 1.f / det;
				dpdu = det * (dv2 * e1 - dv1 * e2);
				dpdv = det * (du1 * e2 - du2 * e1);
			}
		}
		
		float b1 = isect->primitiveData[0], b2 = isect->primitiveData[1], b0 = 1.f - b2 - b1;
		float u = b0 * uv0[0] + b1 * uv1[0] + b2 * uv2[0];
		float v = b0 * uv0[1] + b1 * uv1[1] + b2 * uv2[1];

		isect->dg = DifferentialGeometry(ray(ray.maxt), toWorld->operator()(dpdu), toWorld->operator()(dpdv), Normal(), Normal(), u, v);
		if(ray.hasDifferentials)
			isect->dg.ComputeDifferentials(ray);

		if(!dgShading)
			return;

		Normal ns;
		Normal dndu, dndv;
		if(mesh->n)
		{
			ns = Normalize(toWorld->operator()( b0 * mesh->n[vi[0]] + b1 * mesh->n[vi[1]] + b2 * mesh->n[vi[2]] ));
			if(det != 0.f)
			{
				Normal dn1 = mesh->n[vi[1]] - mesh->n[vi[0]];
				Normal dn2 = mesh->n[vi[2]] - mesh->n[vi[0]];
				dndu = det * (dv2 * dn1 - dv1 * dn2);
				dndv = det * (du1 * dn2 - du2 * dn1);
			}
		}
		else
			ns = isect->dg.n;

		Vector ss = mesh->s ? toWorld->operator()( b0 * mesh->s[vi[0]] + b1 * mesh->s[vi[1]] + b2 * mesh->s[vi[2]]) : isect->dg.dpdu;
		Vector ts = Cross(ss, ns);

		if(ts.Length2() > 0.f)
		{
			ts.Normalize();
			ss = Cross(ts, ns);
		}
		else
			CoordinateSystem((Vector) ns, &ss, &ts);
		
		*dgShading = DifferentialGeometry(isect->dg.p, ss, ts, ns, toWorld->operator()(dndu), toWorld->operator()(dndv), u, v);
		dgShading->dpdx = isect->dg.dpdx; dgShading->dpdy = isect->dg.dpdy;
		dgShading->dudx = isect->dg.dudx; dgShading->dudy = isect->dg.dudy;
		dgShading->dvdx = isect->dg.dvdx; dgShading->dvdy = isect->dg.dvdy;
	}*/
	void Triangle::GetUVs(float uv0[2], float uv1[2], float uv2[2]) const
	{
		if(!mesh->uv)
		{
			uv0[0] = uv0[1] = uv1[0] = uv1[1] = uv2[0] = uv2[1] = 0.f;
			//uv0[0] = uv0[1] = uv1[1] = 0.f;
			//uv1[0] = uv2[0] = uv2[1] = 1.f;
			return;
		}
		uv0[0] = mesh->uv[vi[0] * 2];
		uv0[1] = mesh->uv[vi[0] * 2 + 1];
		uv1[0] = mesh->uv[vi[1] * 2];
		uv1[1] = mesh->uv[vi[1] * 2 + 1];
		uv2[0] = mesh->uv[vi[2] * 2];
		uv2[1] = mesh->uv[vi[2] * 2 + 1];
	}
	float Triangle::Area() const
	{
		const Point &p0 = mesh->v[vi[0]];
		const Point &p1 = mesh->v[vi[1]];
		const Point &p2 = mesh->v[vi[2]];
		return 0.5f * Cross(p1 - p0, p2 - p0).Length();
	}

	TriangleMesh::TriangleMesh(const Transform &toWorld, unsigned int nV, unsigned int nT, unsigned int *vi, Point *v, float *uv, Normal *n, Vector *s, bool makeCopy)
		:	Mesh(	makeCopy ? AllocAligned<Point>(nV) : v,
					makeCopy ? AllocAligned<unsigned int>(3 * nT) : vi,
					makeCopy && uv != NULL ? AllocAligned<float>(nV * 2) : uv,
					makeCopy && n != NULL ? AllocAligned<Normal>(nV) : n,
					makeCopy && s != NULL ? AllocAligned<Vector>(nV) : s)
	{
		bool isIdentity = toWorld == Mat4::identity;
		if(makeCopy)
		{
			memcpy(this->v, v, sizeof(Point) * nV);
			memcpy(&this->v[0].x, &v[0].x, sizeof(Point) * nV);

			if(uv)
				memcpy(this->uv, uv, sizeof(float) * nV * 2);

			if(n)
				memcpy(this->n, n, sizeof(Normal) * nV);

			if(s)
				memcpy(this->s, s, sizeof(Vector) * nV);
		}

		if(!isIdentity)
		{
			for(unsigned int i = 0; i < nV; ++i)
				this->v[i] = toWorld(v[i]);

			if(n)
			{
				Normal nn;
				for(unsigned int i = 0; i < nV; ++i)
				{
					nn = this->n[i];
					toWorld(nn, &this->n[i]);
				}
			}

			if(s)
			{
				Vector ss;
				for(unsigned int i = 0; i < nV; ++i)
				{
					ss = this->s[i];
					toWorld(ss, &this->s[i]);
				}
			}
		}

		Triangle *shapes = AllocAligned<Triangle>(nT);

		for(unsigned int i = 0, *index = vi; i < nT; ++i, index += 3)
		{
			new (&shapes[i]) Triangle(this, index);
			refinedShapes.push_back(&shapes[i]);
		}
	}
}
