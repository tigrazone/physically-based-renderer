/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_LIGHTS_DIRECTIONAL_H
#define RENDERLIB_LIGHTS_DIRECTIONAL_H

#include "Light.h"

namespace Render
{
	class DirectionalLight : public DeltaLight
	{
		Vector dir;
		Spectrum L;
	public:
		DirectionalLight(const Point &from, const Point &to, const Spectrum &L, const Transform *t = NULL);
		DirectionalLight(const Vector &dir, const Spectrum &L, const Transform *t = NULL);
		float Power(const Scene *scene, const Transform *t) const;
		bool SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay = NULL) const;
		bool SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const;
		float Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const;
	};
}

#endif