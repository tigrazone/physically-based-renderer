/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_LIGHTS_DIFFUSE_H
#define RENDERLIB_LIGHTS_DIFFUSE_H

#include "Light.h"
#include "Texture.h"

namespace Render
{
	class DiffuseAreaLight : public AreaLight
	{
		ShapeSet shapeSet;
		RSpectrumTexture emit;
	public:
		DiffuseAreaLight(const Reference<Shape> &shape, const Reference<Primitive> &accel, const RSpectrumTexture &emit);
		float Power(const Scene *scene, const Transform *t) const;
		bool L(const Vector &w, const DifferentialGeometry &dg, const Normal &ng, Spectrum &s) const;
		bool SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay = NULL) const;
		bool SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const;
		float Pdf(const Transform *t, const Point &p, const Vector &wi) const;
		float Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const;
	};
}

#endif