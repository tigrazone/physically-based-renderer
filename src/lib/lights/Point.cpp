/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "lights/Point.h"

namespace Render
{
	PointLight::PointLight(const Point &p, const Spectrum &L, const Transform *t)
		:	p(t ? t->operator()(p) : p), L(L){}

	float PointLight::Power(const Scene *scene, const Transform *t) const
	{
		return 4.f * M_PI * L.y();
	}
	bool PointLight::SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay) const
	{
		*wi = t->operator()(this->p) - p;
		float dist2 = wi->Length2();
		float dist = sqrt(dist2);
		*wi /= dist;
		isect->ng = isect->dg.n = -*wi;
		*pdf = dist2;
		if(visRay)
			SetSegment(visRay, p, *wi, dist);
		s = L;

		return true;
	}
	bool PointLight::SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const
	{
		Sampler *sampler = sample->sampler;
		*ray = Ray(t->operator()(p), UniformSampleSphere(sampler->GetSample(sample, offset), sampler->GetSample(sample, offset + 1)), 0, INFINITY, ray->time);
		isect->ng = isect->dg.n = ray->d;
		*pdf = UniformSpherePDF;
		s = L;
		return true;
	}
	float PointLight::Pdf(const Transform *t, const Point &p, const Vector &wi) const
	{
		return Distance2(p, (*t)(this->p));
	}

	float PointLight::Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const
	{
		return UniformHemispherePDF;
	}
}