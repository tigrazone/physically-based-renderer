/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "accelerators/BVH.h"

#define NBVHBINS 12
namespace Render
{
	struct BucketInfo
	{
		int count;
		BBox bounds;
		BucketInfo() : count(0) {}
	};

	struct BVHPrimitiveInfo
	{
		uint32_t index, bucketNumber;
		Point centroid;
		BBox bounds;

		BVHPrimitiveInfo(uint32_t index, const BBox &bounds)
			:	index(index), bounds(bounds), centroid(bounds.Lerp(0.5f, 0.5f, 0.5f)), bucketNumber(0){}
	};

	struct BVHNode
	{
		BVHNode *children[2];
		uint32_t nPrimitives, offset, axis;
		BBox bounds;
		//Leaf
		BVHNode(uint32_t nPrimitives, uint32_t offset, const BBox &bounds) : nPrimitives(nPrimitives), offset(offset), bounds(bounds), axis(3){ children[0] = children[1] = NULL; }
		//Inner nodes
		BVHNode(uint32_t axis, BVHNode *c0, BVHNode *c1)
			:	axis(axis), bounds(Union(c0->bounds, c1->bounds)), nPrimitives(0)
		{
			children[0] = c0;
			children[1] = c1;
		}
		BVHNode(uint32_t axis, BVHNode *c0, BVHNode *c1, const BBox &bounds)
			:	axis(axis), bounds(bounds), nPrimitives(0)
		{
			children[0] = c0;
			children[1] = c1;
		}

		BVHNode(){}
	};
	struct BVHLinearNode
	{
		BVHLinearNode(uint32_t offset, uint8_t nPrimitives, uint8_t axis, const BBox &bounds)
			:	primitivesOffset(offset), nPrimitives(nPrimitives), axis(axis), bounds(bounds){}
		BBox bounds;
		union
		{
			uint32_t primitivesOffset; //Leaf
			uint32_t secondChildOffset; //Interior
		};
		uint8_t nPrimitives;
		uint8_t axis;
		uint8_t pad[2];
	};

	struct CompareToMid
	{
		float middle;
		uint32_t axis;
		CompareToMid(float middle, uint32_t axis):middle(middle),axis(axis){}
		bool operator()(const BVHPrimitiveInfo& a)const
		{
			return a.centroid[axis] < middle;
		}
	};

	struct ComparePoints
	{
		uint32_t axis;
		ComparePoints(uint32_t axis):axis(axis){}
		bool operator()(const BVHPrimitiveInfo& a, const BVHPrimitiveInfo& b) const
		{
			return a.centroid[axis] < b.centroid[axis];
		}
	};

	//Compare function for splitting by SAH using buckets
	struct CompareToBucket
	{
		uint32_t splitBucket;

		CompareToBucket(uint32_t splitBucket)
			: splitBucket(splitBucket)
		{}

		bool operator()(const BVHPrimitiveInfo &p) const
		{
			return p.bucketNumber <= splitBucket;
		}
	};

	BVHAccel::BVHAccel(const vector<Reference<Primitive>> &p, uint32_t maxPrimsInNode, BVHSplitMethod sm)
		:	maxPrimsInNode(min(255u, maxPrimsInNode)), sm(sm)
	{
		if(p.empty())
		{
			nodes = NULL;
			return;
		}

		for(size_t i = 0; i < p.size(); ++i)
		{
			rPrims.push_back(p[i]);
			if(!p[i]->CanIntersect())
				p[i]->Refine(primitives);
			else
				primitives.push_back(p[i].GetPtr());
		}

		vector<BVHPrimitiveInfo> buildData;
		buildData.reserve(primitives.size());
		for(uint32_t i = 0; i < primitives.size(); ++i)
			buildData.push_back(BVHPrimitiveInfo(i, primitives[i]->Bounds()));

		MemoryArena arena;
		uint32_t total = 0;
		vector<const Primitive *> orderedPrimitives;
		orderedPrimitives.reserve(primitives.size());
		BVHNode *root = recursiveBuild(arena, buildData, 0, primitives.size(), &total, orderedPrimitives);
		primitives.swap(orderedPrimitives);

		nodes = AllocAligned<BVHLinearNode>(total);
		uint32_t offset = 0;
		recursiveFlatten(root, &offset);
	}

	BVHAccel::~BVHAccel()
	{
		FreeAligned(nodes);
	}

	BVHNode *BVHAccel::recursiveBuild(MemoryArena &arena, vector<BVHPrimitiveInfo> &buildData, uint32_t start, uint32_t end, uint32_t *totalNodes, vector<const Primitive *> &ordered)
	{
		BVHNode *node = arena.Alloc<BVHNode>();
		(*totalNodes)++;
	
		BBox bounds = buildData[start].bounds;
		for(uint32_t i = start + 1; i < end; ++i)
			bounds = Union(bounds, buildData[i].bounds);

		uint32_t nPrimitives = end - start;
		if(nPrimitives == 1)
		{
			initLeaf(node, buildData, start, end, nPrimitives, ordered, bounds);
			return node;
		}

		BBox centroidBounds(buildData[start].centroid);
		for(uint32_t i = start + 1; i < end; ++i)
			centroidBounds = Union(centroidBounds, buildData[i].centroid);

		//Split on the dimension with the highest length.
		uint32_t axis = centroidBounds.MaximumExtent();

		//If the maximum extent is 0, then just add everything.
		if(centroidBounds.pMax[axis] == centroidBounds.pMin[axis])
		{
			initLeaf(node, buildData, start, end, nPrimitives, ordered, bounds);
			return node;
		}

		uint32_t mid;
		switch(sm)
		{
		case MIDDLE:
			{
				float pMid = 0.5f * (centroidBounds.pMin[axis] + centroidBounds.pMax[axis]);
				BVHPrimitiveInfo *midPtr = partition(&buildData[start], &buildData[end - 1] + 1, CompareToMid(pMid, axis));
				mid = midPtr - &buildData[0];
				//For large numbers of primitives, partitioning may fail.  In that case, fall through to SPLIT_EQUAL.
				if(mid != start && mid != end)
					break;
			}
		case EQUAL:
			{
				mid = (start + end) / 2;
				nth_element(&buildData[start], &buildData[mid], &buildData[end - 1] + 1, ComparePoints(axis));
				break;
			}
			case SAH:
			{
				//At a certain point, SAH becomes somewhat ineffective, and splitting by equal parts is preferred
				if(nPrimitives > 4)
				{
					BucketInfo buckets[NBVHBINS];
					float invCentroidExtent = 1.f / (centroidBounds.pMax[axis] - centroidBounds.pMin[axis]);
					for(unsigned int i = start; i < end; i++)
					{
						int b = Floor2Int((float) NBVHBINS * (buildData[i].centroid[axis] - centroidBounds.pMin[axis]) * invCentroidExtent);
						if(b >= NBVHBINS) b = NBVHBINS - 1;
					
						buildData[i].bucketNumber = b;
						buckets[b].count++;
						buckets[b].bounds = Union(buckets[b].bounds, buildData[i].bounds);
					}

					//We want to compute the costs of splitting at bucket i.  The cost estimate requires, as data,
					//the union of bounding boxes and the sum of primitive counts between two bucket sets:
					//buckets with indices [0 to i] and buckets with indices [i+1 to nBuckets - 1].  Their respective
					//cost contribution can be precomputed linearly and stored into arrays.

					unsigned int nBucketsMinus1 = NBVHBINS - 1;

					//Stores the numerator of the cost part for each set
					float costF[NBVHBINS - 1]; //Bucket indices [0 to i]
					float costB[NBVHBINS - 1]; //Bucket indices [i+1 to nBuckets-1]

					BBox b0, b1;
					int count0 = 0, count1 = 0;
					for(unsigned int i = 0, j = nBucketsMinus1; i < nBucketsMinus1; i++, j--)
					{
						count0 += buckets[i].count;
						count1 += buckets[j].count;
						b0 = Union(b0, buckets[i].bounds);
						b1 = Union(b1, buckets[j].bounds);
						costF[i] = (float) count0 * b0.SurfaceArea();
						costB[j - 1] = (float) count1 * b1.SurfaceArea();
					}

					float minCost = INFINITY;
					int minCostBucket = 0;
					float invBoundsSA = 1.f / bounds.SurfaceArea();
					for(unsigned int i = 0; i < nBucketsMinus1; i++)
					{
						//Arbitrarily assume that traversal cost is 1/8 the cost of a bounding box intersection.
						float cost = 0.125f + (costF[i] + costB[i]) * invBoundsSA;
						if(minCost > cost)
						{
							minCost = cost;
							minCostBucket = i;
						}
					}

					if(nPrimitives > maxPrimsInNode || minCost < nPrimitives)
					{
						BVHPrimitiveInfo *midPtr = ::partition(&buildData[start], &buildData[end-1] + 1, CompareToBucket(minCostBucket));
						mid = midPtr - &buildData[0];
					}
					else
					{
						initLeaf(node, buildData, start, end, nPrimitives, ordered, bounds);
						return node;
					}
					break;
				}

				mid = (start + end) / 2;
				nth_element(&buildData[start], &buildData[mid], &buildData[end - 1] + 1, ComparePoints(axis));
			}
		}

		new (node) BVHNode(	axis,
							recursiveBuild(arena, buildData, start, mid, totalNodes, ordered),
							recursiveBuild(arena, buildData, mid, end, totalNodes, ordered),
							bounds);

		return node;
	}

	void BVHAccel::initLeaf(BVHNode *node, vector<BVHPrimitiveInfo> &buildData, uint32_t start, uint32_t end, uint32_t n, vector<const Primitive *> &ordered, const BBox &b)
	{
		uint32_t offset = ordered.size();
		for(uint32_t i = start; i < end; ++i)
			ordered.push_back(primitives[buildData[i].index]);

		new (node) BVHNode(n, offset, b);
	}

	uint32_t BVHAccel::recursiveFlatten(BVHNode *buildNode, uint32_t *offset)
	{
		uint32_t myOffset = (*offset)++;
		BVHLinearNode *linearNode = &nodes[myOffset];

		if(buildNode->nPrimitives == 0)
		{
			recursiveFlatten(buildNode->children[0], offset);
			new (linearNode) BVHLinearNode( recursiveFlatten(buildNode->children[1], offset), 
											0, buildNode->axis, buildNode->bounds);
		}
		else
			new (linearNode) BVHLinearNode(buildNode->offset, buildNode->nPrimitives, 3, buildNode->bounds);

		return myOffset;
	}

	bool BVHAccel::Intersect(const Ray &ray, Intersection *intersection) const
	{
		if(nodes == NULL)
			return false;

		bool hit = false;

		Vector invDir(1.f / ray.d.x, 1.f / ray.d.y, 1.f / ray.d.z);
		uint32_t isNeg[3] = {invDir.x < 0, invDir.y < 0, invDir.z < 0};

		uint32_t todo[64];
		uint32_t nodeNum = 0, todoIndex = 0;

		while(true)
		{
			const BVHLinearNode &node = nodes[nodeNum];
			if(Render::QuickBoxIntersect(node.bounds, ray, invDir, isNeg))
			{
				if(node.nPrimitives > 0)
				{
					for(uint32_t i = node.primitivesOffset; i < node.primitivesOffset + node.nPrimitives; ++i)
						hit = primitives[i]->Intersect(ray, intersection) || hit;

					if(todoIndex == 0) break;
					nodeNum = todo[--todoIndex];
				}
				else
				{
					if(isNeg[node.axis])
					{
						todo[todoIndex++] = nodeNum + 1;
						nodeNum = node.secondChildOffset;
					}
					else
					{
						todo[todoIndex++] = node.secondChildOffset;
						++nodeNum;
					}
				}
			}
			else
			{
				if(todoIndex == 0) break;
				nodeNum = todo[--todoIndex];
			}
		}
		return hit;
	}

	bool BVHAccel::IntersectP(const Ray &ray) const
	{	
		if(nodes == NULL)
			return false;

		Vector invDir(1.f / ray.d.x, 1.f / ray.d.y, 1.f / ray.d.z);
		uint32_t isNeg[3] = {invDir.x < 0, invDir.y < 0, invDir.z < 0};
		uint32_t todo[64];
		uint32_t nodeNum = 0, todoIndex = 0;

		while(true)
		{
			const BVHLinearNode &node = nodes[nodeNum];
			if(Render::QuickBoxIntersect(node.bounds, ray, invDir, isNeg))
			{
				if(node.nPrimitives > 0)
				{
					for(uint32_t i = node.primitivesOffset; i < node.primitivesOffset + node.nPrimitives; ++i)
						if(primitives[i]->IntersectP(ray))
							return true;

					if(todoIndex == 0) return false;
					nodeNum = todo[--todoIndex];
				}
				else
				{
					if(isNeg[node.axis])
					{
						todo[todoIndex++] = nodeNum + 1;
						nodeNum = node.secondChildOffset;
					}
					else
					{
						todo[todoIndex++] = node.secondChildOffset;
						++nodeNum;
					}
				}
			}
			else
			{
				if(todoIndex == 0) return false;
				nodeNum = todo[--todoIndex];
			}
		}
	}

	float BVHAccel::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		if(nodes == NULL)
			return 0.f;

		float pdf = 0.f;

		Vector invDir(1.f / ray.d.x, 1.f / ray.d.y, 1.f / ray.d.z);
		uint32_t isNeg[3] = {invDir.x < 0, invDir.y < 0, invDir.z < 0};

		uint32_t todo[64];
		uint32_t nodeNum = 0, todoIndex = 0;

		while(true)
		{
			BVHLinearNode &node = nodes[nodeNum];
			if(Render::QuickBoxIntersect(node.bounds, ray, invDir, isNeg))
			{
				if(node.nPrimitives > 0)
				{
					for(uint32_t i = node.primitivesOffset; i < node.primitivesOffset + node.nPrimitives; ++i)
						pdf += primitives[i]->Pdf(rayWorld, ray, toWorld, isect);

					if(todoIndex == 0) break;
						nodeNum = todo[--todoIndex];
				}
				else
				{
					//We must intersect with all primitives, so ensure that maxt isn't updated such that farther boxes cannot be intersected with.
					if(isNeg[node.axis])
					{
						todo[todoIndex++] = node.secondChildOffset;
						++nodeNum;
					}
					else
					{
						todo[todoIndex++] = nodeNum + 1;
						nodeNum = node.secondChildOffset;
					}
				}
			}
			else
			{
				if(todoIndex == 0) break;
				nodeNum = todo[--todoIndex];
			}
		}

		return pdf;
	}
	BBox BVHAccel::Bounds() const
	{
		return nodes ? nodes->bounds : BBox();
	}
}