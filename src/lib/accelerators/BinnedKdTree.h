/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_ACCELERATORS_BINNEDKDTREE_H
#define RENDERLIB_ACCELERATORS_BINNEDKDTREE_H

#include "Primitive.h"

namespace Render
{
	struct BoundEdge;
	struct KdAccelNode;

	class BinnedKdTreeAccel  : public Aggregate
	{
		unsigned int isectCost, traverseCost, maxDepth;
		uint32_t nAlloc, nextFreeNode, maxPrimsInNode;
		float emptyBonus;

		KdAccelNode *nodes;
		BBox bounds;

		void recursiveBuild(uint32_t nodeNum, uint32_t nPrimitives, unsigned int depth,
							uint32_t *indices, uint32_t *aux0, uint32_t *aux1,
							BoundEdge *events[3], BoundEdge *auxEvents0[3], BoundEdge *auxEvents1[3],
							const vector<BBox> &primBounds, const BBox &nodeBounds,
							int *bins, unsigned int badSplits = 0, bool isSorted = false);

		MemoryArena arena;
	public:
		BinnedKdTreeAccel(	const vector<Reference<Primitive>> &primitives, unsigned int isectCost = 80, unsigned int traverseCost = 1,
							float emptyBonus = 0.5f, unsigned int maxDepth = 0, uint32_t maxPrimsInNode = 1);
		~BinnedKdTreeAccel();
		bool Intersect(const Ray &ray, Intersection *isect) const;
		bool IntersectP(const Ray &ray) const;
		BBox Bounds() const;
	};
}

#endif