/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "accelerators/BinnedKdTree.h"

//Maxim Shevtsov et al. (2007) determined this was an optimum number of bins.
#define NBINS 32
#define NBINS2 64

namespace Render
{
	struct KdAccelNode
	{
		//Leaf node
		KdAccelNode(uint32_t *indices, unsigned int nPrims, MemoryArena &arena) : axis(3)
		{
			this->nPrims |= nPrims << 2;
			if(nPrims == 0)
				onePrimitive = 0;
			else if(nPrims == 1)
				onePrimitive = indices[0];
			else
			{
				this->indices = arena.Alloc<uint32_t>(nPrims);
				memcpy(this->indices, indices, sizeof(uint32_t) * nPrims);
			}
		}
		//Interior node
		KdAccelNode(uint32_t rightChild, uint32_t axis, float split) : axis(axis), split(split)
		{
			this->rightChild |= rightChild << 2;
		}

		bool IsLeaf() const { return (axis & 3) == 3; }
		uint32_t Count() const { return nPrims >> 2; }
		uint32_t Axis() const { return axis & 3; }
		uint32_t Right() const { return Count(); }

		union {
			float split;
			uint32_t onePrimitive;
			uint32_t *indices;
		};
	private:
		union {
			uint32_t axis;
			uint32_t nPrims;
			uint32_t rightChild;
		};
	};

	struct KdTodo
	{
		const KdAccelNode *node;
		float min, max;
	};

	struct BoundEdge
	{
		size_t index;
		float t;
		bool isStart;//Bounding box start edge

		BoundEdge(){}
		BoundEdge(size_t index, float t, bool isStart):index(index), t(t), isStart(isStart){}
		bool operator<(const BoundEdge &other) const
		{
			if(t == other.t)
				return isStart && !other.isStart;

			return t < other.t;
		}
	};

	BinnedKdTreeAccel::BinnedKdTreeAccel(	const vector<Reference<Primitive>> &prims, unsigned int isectCost, unsigned int traverseCost,
											float emptyBonus, unsigned int maxDepth, uint32_t maxPrimsInNode)
		:	isectCost(isectCost), traverseCost(traverseCost), emptyBonus(emptyBonus),
			maxPrimsInNode(maxPrimsInNode), nAlloc(0), nextFreeNode(0), nodes(NULL)
	{
		if(prims.size() == 0)
			return;

		for(size_t i = 0; i < prims.size(); ++i)
		{
			rPrims.push_back(prims[i]);
			if(!prims[i]->CanIntersect())
				prims[i]->Refine(primitives);
			else
				primitives.push_back(prims[i].GetPtr());
		}

		maxDepth = maxDepth == 0 ? unsigned int( 8.5f + 1.3f * Log2((float) primitives.size()) ) : maxDepth;
		this->maxDepth = maxDepth;

		vector<BBox> primBounds;
		primBounds.reserve(primitives.size());
		for(size_t i = 0; i < primitives.size(); ++i)
		{
			primBounds.push_back(primitives[i]->Bounds());
			bounds = Union(bounds, primBounds.back());
		}

		uint32_t *indices = new uint32_t[primitives.size()];
		for(uint32_t i = 0; i < primitives.size(); ++i)
			indices[i] = i;

		int bins[NBINS2];

		//Event lists will be sorted and operated upon when nPrimitives <= nBins.  This set of events are meant to be used similarly to indices and auxIndices respectively.
		BoundEdge *events[3];
		events[0] = AllocAligned<BoundEdge>(NBINS2 * 3);
		events[1] = &events[0][NBINS2];
		events[2] = &events[1][NBINS2];

		BoundEdge *auxEvents[3];
		auxEvents[0] = AllocAligned<BoundEdge>(NBINS2 * (maxDepth + 1) * 3);
		auxEvents[1] = &auxEvents[0][NBINS2 * (maxDepth + 1)];
		auxEvents[2] = &auxEvents[1][NBINS2 * (maxDepth + 1)];

		uint32_t *auxIndices = (uint32_t *)AllocAligned(sizeof(uint32_t) * (maxDepth + 1) * primitives.size());

		recursiveBuild(0, primitives.size(), 0, indices, indices, auxIndices, events, events, auxEvents, primBounds, bounds, bins);

		delete[] indices;
		FreeAligned(auxIndices);
		FreeAligned(events[0]);
		FreeAligned(auxEvents[0]);
	}

	BinnedKdTreeAccel::~BinnedKdTreeAccel()
	{
		FreeAligned(nodes);
	}

	void BinnedKdTreeAccel::recursiveBuild(uint32_t nodeNum, uint32_t nPrimitives, unsigned int depth,
							uint32_t *indices, uint32_t *aux0, uint32_t *aux1,
							BoundEdge *events[3], BoundEdge *auxEvents0[3], BoundEdge *auxEvents1[3],
							const vector<BBox> &primBounds, const BBox &nodeBounds,
							int *bins, unsigned int badSplits, bool isSorted)
	{
		if(nodeNum == nAlloc)
		{
			uint32_t newSize = max(1024u, 2 * nAlloc);
			KdAccelNode *n = AllocAligned<KdAccelNode>(newSize);
			if(nAlloc != 0)
			{
				memcpy(n, nodes, sizeof(KdAccelNode) * nAlloc);
				FreeAligned(nodes);
			}
			nodes = n;
			nAlloc = newSize;
		}

		++nextFreeNode;

		if(depth == maxDepth || nPrimitives <= maxPrimsInNode)
		{
			new (&nodes[nodeNum]) KdAccelNode(indices, nPrimitives, arena);
			return;
		}

		float split;
		int bestAxis = -1;

		{	//Stack overflow paranoia.  The same number of allocations/deallocations should occur in the end, hopefully allowing for more depth.
			float bestCost = INFINITY;
			float noSplitCost = float(isectCost) * float(nPrimitives);

			float totalSA = nodeBounds.SurfaceArea();
			float invTotalSA = 1.f / totalSA;

			unsigned int axis = nodeBounds.MaximumExtent();
			Vector d = nodeBounds.pMax - nodeBounds.pMin;

			uint32_t bestSplitIndex = nPrimitives;

			for(unsigned int retries = 0; retries < 3; ++retries)
			{
				unsigned int axis1 = axis + 1;
				unsigned int axis2 = axis + 2;
				if(axis1 > 2)
					axis1 -= 3;
				if(axis2 > 2)
					axis2 -= 3;

				if(nPrimitives > NBINS)
				{
					unsigned int step = 1;
					unsigned int multiple = 10;
					//Supposedly faster than Floor2Uint(log10(nPrimitives))?
					while((multiple *= 10) <= nPrimitives)
						++step;

					memset(bins, 0, sizeof(int) * NBINS2);

					float binsPerExtent = float(NBINS) / d[axis];

					for(uint32_t i = 0; i < nPrimitives; i += step)
					{
						//Min bins
						int bin = Clamp(Floor2Int((primBounds[indices[i]].pMin[axis] - nodeBounds.pMin[axis]) * binsPerExtent), 0, NBINS - 1);
						++bins[bin];

						//Max bins - backwards so as to get the right bin on edge cases.
						bin = Clamp(NBINS - Floor2Int((nodeBounds.pMax[axis] - primBounds[indices[i]].pMax[axis]) * binsPerExtent) - 1, 0, NBINS - 1);
						++bins[bin + NBINS];
					}

					unsigned int leftCount = bins[0], rightCount = bins[NBINS2 - 1];
					for(int i = 1, j = NBINS2 - 2; i < NBINS; ++i, --j)
					{
						leftCount += bins[i];
						rightCount += bins[j];

						bins[i] = leftCount;
						bins[j] = rightCount;
					}

					//Expanding out the cost calculation allows for precalculation of some terms.
					float commonCoeff1 = isectCost * 2.f * invTotalSA;
					float commonCoeff2 = commonCoeff1 * (d[axis1] + d[axis2]) / binsPerExtent;
					commonCoeff1 *= d[axis1] * d[axis2];
					for(int i = 0; i < NBINS - 1; ++i)
					{
						leftCount = bins[i];
						rightCount = bins[NBINS + i + 1];
						float bonus = leftCount == 0 || rightCount == 0 ? 1.f - emptyBonus : 1.f;
						float cost = traverseCost + bonus * (	commonCoeff1 *	float(leftCount + rightCount) +
																commonCoeff2 * (float(leftCount * (i + 1)) + 
																				float(rightCount * (NBINS - i - 1)))	);

						if(cost < bestCost)
						{
							bestSplitIndex = i;
							bestCost = cost;
						}
					}

					if(bestSplitIndex != nPrimitives)
					{
						bestAxis = axis;
						split = nodeBounds.pMin[axis] + float(bestSplitIndex + 1) / binsPerExtent;
						break;
					}
				}
				else
				{
					////Exact SAH calculations.  Sorting is performed once at some depth over all nodes of that depth that do not become children.
					if(!isSorted)
					{
						isSorted = true;
						events = auxEvents0;
						for(uint32_t i = 0; i < nPrimitives; ++i)
						{
							for(int j = 0; j < 3; j++)
							{
								events[j][2 * i] = BoundEdge(indices[i], primBounds[indices[i]].pMin[j], true);
								events[j][2 * i + 1] = BoundEdge(indices[i], primBounds[indices[i]].pMax[j], false);
							}
						}

						//Sort all axes here for future use.
						sort(&events[0][0], &events[0][2 * nPrimitives]);
						sort(&events[1][0], &events[1][2 * nPrimitives]);
						sort(&events[2][0], &events[2][2 * nPrimitives]);
					}

					uint32_t leftCount = 0, rightCount = nPrimitives;
					float commonCoeff1 = isectCost * 2.f * invTotalSA;
					float commonCoeff2 = commonCoeff1 * (d[axis1] + d[axis2]);
					commonCoeff1 *= d[axis1] * d[axis2];
					for(uint32_t i = 0; i < nPrimitives * 2; ++i)
					{
						if(!events[axis][i].isStart)
							--rightCount;

						if(	events[axis][i].t > nodeBounds.pMin[axis] &&
							events[axis][i].t < nodeBounds.pMax[axis])
						{
							float bonus = leftCount == 0 || rightCount == 0 ? 1.f - emptyBonus : 1.f;
							float cost = traverseCost + bonus * (	commonCoeff1 * (leftCount + rightCount) +
																	commonCoeff2 * (float(leftCount) * (events[axis][i].t - nodeBounds.pMin[axis]) +
																					float(rightCount)* (nodeBounds.pMax[axis] - events[axis][i].t))	);

							if(cost < bestCost)
							{
								bestSplitIndex = i;
								bestCost = cost;
							}
						}

						if(events[axis][i].isStart)
							++leftCount;
					}
						
					if(bestSplitIndex != nPrimitives)
					{
						bestAxis = axis;
						split = events[axis][bestSplitIndex].t;
						break;
					}
				}
					
				axis++;
				if(axis == 3)
					axis = 0;
			}//for retries
			
			if(noSplitCost < bestCost) ++badSplits;

			if(bestAxis == -1 || (4 * noSplitCost < bestCost && nPrimitives < 16) || badSplits == 3)
			{
				new (&nodes[nodeNum]) KdAccelNode(indices, nPrimitives, arena);
				return;
			}
		}//code block

		//The indices array can be updated on the spot without disturbing its integrity in the case that aux0 == indices.
		uint32_t n0 = 0, n1 = 0;
		for(uint32_t i = 0; i < nPrimitives; ++i)
		{
			float primMin = primBounds[indices[i]].pMin[bestAxis];
			float primMax = primBounds[indices[i]].pMax[bestAxis];

			if(primMax <= split)
				aux0[n0++] = indices[i];
			else if(primMin >= split)
				aux1[n1++] = indices[i];
			else
			{
				aux0[n0++] = indices[i];
				aux1[n1++] = indices[i];
			}
		}

		BBox bounds0 = nodeBounds, bounds1 = nodeBounds;
		bounds0.pMax[bestAxis] = bounds1.pMin[bestAxis] = split;

		if(isSorted)
		{
			uint32_t ne0[3], ne1[3];
			ne0[0] = ne0[1] = ne0[2] = ne1[0] = ne1[1] = ne1[2] = 0;

			for(int j = 0; j < 3; ++j)
			{
				for(uint32_t i = 0; i < 2 * nPrimitives; ++i)
				{
					float primMin = primBounds[events[j][i].index].pMin[bestAxis];
					float primMax = primBounds[events[j][i].index].pMax[bestAxis];

					//TODO: Plane clipping.  If that is performed, then objects that end on both sides require resorting the list.
					//Beware, plane-object intersection is needed for all primitive and shape types.
					if(primMax <= split)
						auxEvents0[j][ne0[j]++] = events[j][i];
					else if(primMin >= split)
						auxEvents1[j][ne1[j]++] = events[j][i];
					else
					{
						if(j == bestAxis)
						{
							if(events[j][i].isStart)
							{
								auxEvents0[j][ne0[j]++] = events[j][i];
								auxEvents1[j][ne1[j]++] = BoundEdge(events[j][i].index, split, true);
							}
							else
							{
								auxEvents0[j][ne0[j]++] = BoundEdge(events[j][i].index, split, false);
								auxEvents1[j][ne1[j]++] = events[j][i];
							}
						}
						else
						{
							auxEvents0[j][ne0[j]++] = events[j][i];
							auxEvents1[j][ne1[j]++] = BoundEdge(events[j][i].index, events[j][i].t, events[j][i].isStart);
						}
					}
				}
			}

			{
				BoundEdge *auxEvents2[3];
				auxEvents2[0] = auxEvents1[0] + 2 * nPrimitives;
				auxEvents2[1] = auxEvents1[1] + 2 * nPrimitives;
				auxEvents2[2] = auxEvents1[2] + 2 * nPrimitives;

				recursiveBuild(nextFreeNode, n0, depth + 1, aux0, aux0, aux1 + n1, auxEvents0, auxEvents0, auxEvents2, primBounds, bounds0, bins, badSplits, isSorted);
			}

			new (&nodes[nodeNum]) KdAccelNode(nextFreeNode, bestAxis, split);
			recursiveBuild(nextFreeNode, n1, depth + 1, aux1, aux0, aux1, auxEvents1, auxEvents0, auxEvents1, primBounds, bounds1, bins, badSplits, isSorted);

			return;
		}//if isSorted

		recursiveBuild(nextFreeNode, n0, depth + 1, aux0, aux0, aux1 + n1, events, auxEvents0, auxEvents1, primBounds, bounds0, bins, badSplits);

		new (&nodes[nodeNum]) KdAccelNode(nextFreeNode, bestAxis, split);
		recursiveBuild(nextFreeNode, n1, depth + 1, aux1, aux0, aux1, events, auxEvents0, auxEvents1, primBounds, bounds1, bins, badSplits);
	}

	bool BinnedKdTreeAccel::Intersect(const Ray &ray, Intersection *isect) const
	{
		float tMin, tMax;
		if(nodes == NULL || !bounds.IntersectP(ray, &tMin, &tMax))
			return false;

		bool hit = false;

		Vector invDir(1.f / ray.d.x, 1.f / ray.d.y, 1.f / ray.d.z);

		KdTodo todo[64];
		uint32_t todoIndex = 0;

		const KdAccelNode *node = nodes;

		while(ray.maxt >= tMin)
		{
			if(!node->IsLeaf())
			{
				uint32_t axis = node->Axis();
				float tPlane = (node->split - ray.o[axis]) * invDir[axis];

				const KdAccelNode *first, *second;
				if(ray.o[axis] < node->split || (ray.o[axis] == node->split && ray.d[axis] <= 0))
				{
					first = &node[1];
					second = &nodes[node->Right()];
				}
				else
				{
					first = &nodes[node->Right()];
					second = &node[1];
				}

				if(tPlane > tMax || tPlane <= 0)
					node = first;
				else if(tPlane < tMin)
					node = second;
				else
				{
					todo[todoIndex].node = second;
					todo[todoIndex].min = tPlane;
					todo[todoIndex].max = tMax;
					todoIndex++;
					node = first;
					tMax = tPlane;
				}
			}
			else
			{
				uint32_t n = node->Count();
				if(n == 1)
					hit = primitives[node->onePrimitive]->Intersect(ray, isect) || hit;
				else
					for(uint32_t i = 0; i < n; ++i)
						hit = primitives[node->indices[i]]->Intersect(ray, isect) || hit;

				if(todoIndex == 0) break;

				--todoIndex;
				tMax = todo[todoIndex].max;
				tMin = todo[todoIndex].min;
				node = todo[todoIndex].node;
			}
		}

		return hit;
	}

	bool BinnedKdTreeAccel::IntersectP(const Ray &ray) const
	{
		float tMin, tMax;
		if(nodes == NULL || !bounds.IntersectP(ray, &tMin, &tMax))
			return false;

		Vector invDir(1.f / ray.d.x, 1.f / ray.d.y, 1.f / ray.d.z);

		KdTodo todo[64];
		uint32_t todoIndex = 0;

		const KdAccelNode *node = nodes;

		while(ray.maxt >= tMin)
		{
			if(!node->IsLeaf())
			{
				uint32_t axis = node->Axis();
				float tPlane = (node->split - ray.o[axis]) * invDir[axis];

				const KdAccelNode *first, *second;
				if(ray.o[axis] < node->split || (ray.o[axis] == node->split && ray.d[axis] <= 0))
				{
					first = &node[1];
					second = &nodes[node->Right()];
				}
				else
				{
					first = &nodes[node->Right()];
					second = &node[1];
				}

				if(tPlane > tMax || tPlane <= 0)
					node = first;
				else if(tPlane < tMin)
					node = second;
				else
				{
					todo[todoIndex].node = second;
					todo[todoIndex].min = tPlane;
					todo[todoIndex].max = tMax;
					todoIndex++;
					node = first;
					tMax = tPlane;
				}
			}
			else
			{
				uint32_t n = node->Count();
				if(n == 1)
				{
					if(primitives[node->onePrimitive]->IntersectP(ray))
						return true;
				}
				else
				{
					for(uint32_t i = 0; i < n; ++i)
						if(primitives[node->indices[i]]->IntersectP(ray))
							return true;
				}

				if(todoIndex == 0) return false;

				--todoIndex;
				tMax = todo[todoIndex].max;
				tMin = todo[todoIndex].min;
				node = todo[todoIndex].node;
			}
		}

		return false;
	}

	BBox BinnedKdTreeAccel::Bounds() const
	{
		return bounds;
	}
}