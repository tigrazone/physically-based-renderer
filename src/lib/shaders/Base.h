/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_SHADERS_BASE_H
#define RENDERLIB_SHADERS_BASE_H

#include "GLPrograms.h"

namespace Render
{
	extern GLShaderSource vsBaseUV;
	extern GLShaderSource fsBaseRectTex;
	extern GLShaderSource fsBaseRectTexNormalizeAlpha;

	extern GLShaderSource *const baseTextureProgram[2];

	struct BaseUVGlobals
	{
		GLuint toRasterIndex, toWorldIndex;
		BaseUVGlobals(GLuint program);
		inline void Bind(GLuint toRasterBuffer, GLuint toWorldBuffer) const
		{
			if(toRasterBuffer)
				glBindBufferBase(GL_UNIFORM_BUFFER, 0, toRasterBuffer);

			if(toWorldBuffer)
				glBindBufferBase(GL_UNIFORM_BUFFER, 1, toWorldBuffer);
		}
	};

	struct TexturedGlobals : public GLProgramGlobals, BaseUVGlobals
	{
		inline TexturedGlobals(GLuint handle) : GLProgramGlobals(handle), BaseUVGlobals(handle) {}
		inline void Bind(	GLuint toRaster, GLuint toWorld, GLuint texture, GLuint vertexArray, 
							GLuint vertexBuffer, GLuint uvBuffer, 
							GLuint nDimVert, GLuint nDimUV) const
		{
			glUseProgram(handle);

			BaseUVGlobals::Bind(toRaster, toWorld);

			glBindTexture(GL_TEXTURE_RECTANGLE, texture);

			glBindVertexArray(vertexArray);

			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glVertexAttribPointer(0, nDimVert, GL_FLOAT, GL_FALSE, 0, 0);

			glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
			glVertexAttribPointer(1, nDimUV, GL_FLOAT, GL_FALSE, 0, 0);
		}
	};

	class XYZAlphaNormalizedTextureProgram
	{
		explicit XYZAlphaNormalizedTextureProgram();
	public:
		static GLShaderSource *const sources[2];

		static void GetSources(GLShaderSource *const **sources, int *n)
		{
			*sources = XYZAlphaNormalizedTextureProgram::sources;
			*n = 2;
		}

		static GLProgramGlobals GetProgramGlobals(GLuint program)
		{
			return TexturedGlobals(program);
		}
	};

	class TextureProgram
	{
		explicit TextureProgram();
	public:
		static GLShaderSource *const sources[2];

		static void GetSources(GLShaderSource *const **sources, int *n)
		{
			*sources = TextureProgram::sources;
			*n = 2;
		}

		static GLProgramGlobals GetProgramGlobals(GLuint program)
		{
			return TexturedGlobals(program);
		}
	};

	class AlphaNormalizedTextureProgram
	{
		explicit AlphaNormalizedTextureProgram();
	public:
		static GLShaderSource *const sources[2];

		static void GetSources(GLShaderSource *const **sources, int *n)
		{
			*sources = AlphaNormalizedTextureProgram::sources;
			*n = 2;
		}

		static GLProgramGlobals GetProgramGlobals(GLuint program)
		{
			return TexturedGlobals(program);
		}

	};
}

#endif