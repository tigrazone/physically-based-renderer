/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "shaders/Base.h"

namespace Render
{
	//Basic UV Vertex Shader.
	GLShaderSource vsBaseUV
	(
		GLSL
		(
			330,
			
			layout(std140) uniform RasterMatrix
			{
				mat4 toRaster;
			};

			layout(std140) uniform WorldMatrix
			{
				mat4 toWorld;
			};

			layout(location = 0) in vec3 in_Position;
			layout(location = 1) in vec2 in_uv;
			out vec2 ex_uv;

			void main(void)
			{
				gl_Position = toRaster * toWorld * vec4(in_Position, 1.0);
				ex_uv = in_uv;
			}
		),
		false,
		GLShader::VERTEX
	);

	//Basic UV fragment shader.
	GLShaderSource fsBaseRectTex
	(
		GLSL
		(
			330,
			in  vec2 ex_uv;
			out vec4 out_Color;

			uniform sampler2DRect sampler;
			void main(void)
			{
				out_Color = texture( sampler, ex_uv ).rgba;
			}
		),
		false,
		GLShader::FRAGMENT
	);

	//Basic UV fragment shader using alpha as a weight.
	GLShaderSource fsBaseRectTexNormalizeAlpha
	(
		GLSL
		(
			330,
			in  vec2 ex_uv;
			out vec4 out_Color;

			uniform sampler2DRect sampler;
			void main(void)
			{
				out_Color = texture( sampler, ex_uv ).rgba;
				out_Color /= out_Color.a;
			}
		),
		false,
		GLShader::FRAGMENT
	);

	GLShaderSource fsBaseXYZtoRGBRectTexNormalizeAlpha
	(
		GLSL
		(
			330,
			in  vec2 ex_uv;
			out vec4 out_Color;

			uniform sampler2DRect sampler;
			void main(void)
			{
				vec4 xyzs = texture( sampler, ex_uv ).rgba;

				out_Color.r = 3.240479f*xyzs.r - 1.537150f*xyzs.g - 0.498535f*xyzs.b;
				out_Color.g = -0.969256f*xyzs.r + 1.875991f*xyzs.g + 0.041556f*xyzs.b;
				out_Color.b = 0.055648f*xyzs.r - 0.204043f*xyzs.g + 1.057311f*xyzs.b;
				out_Color.a = xyzs.a;

				if(out_Color.a > 0.f)
					out_Color /= out_Color.a;
				out_Color.r = pow(out_Color.r, 1.f / 2.2f);
				out_Color.g = pow(out_Color.g, 1.f / 2.2f);
				out_Color.b = pow(out_Color.b, 1.f / 2.2f);
			}
		),
		false,
		GLShader::FRAGMENT
	);

	GLuint sharedIconTexture = 0;

	GLShaderSource *const TextureProgram::sources[2] = {&vsBaseUV, &fsBaseRectTex};
	GLShaderSource *const AlphaNormalizedTextureProgram::sources[2] = {&vsBaseUV, &fsBaseRectTexNormalizeAlpha};
	GLShaderSource *const XYZAlphaNormalizedTextureProgram::sources[2] = {&vsBaseUV, &fsBaseXYZtoRGBRectTexNormalizeAlpha};

	BaseUVGlobals::BaseUVGlobals(GLuint program)
		:	toWorldIndex(0), toRasterIndex(0)
	{
		if(!program)
			return;

		glUseProgram(program);

		toRasterIndex = glGetUniformBlockIndex(program, "RasterMatrix");
		glUniformBlockBinding(program, toRasterIndex, 0);

		toWorldIndex = glGetUniformBlockIndex(program, "WorldMatrix");
		glUniformBlockBinding(program, toWorldIndex, 1);
	}
}