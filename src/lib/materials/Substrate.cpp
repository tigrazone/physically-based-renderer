/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "materials/Substrate.h"

namespace Render
{
	SubstrateMaterial::SubstrateMaterial(	const RSpectrumTexture &rd, const RSpectrumTexture &rs, const RFloatTexture &roughnessU, const RFloatTexture &roughnessV,
											const RSpectrumTexture &alpha, const RFloatTexture &depth, const RFloatTexture &ior, const RFloatTexture &bump)
		:	rd(rd), rs(rs), roughnessU(roughnessU), roughnessV(roughnessV), alpha(alpha), depth(depth), ior(ior), bump(bump), coatParams(alpha && depth)
	{
		if(!roughnessU)
			this->roughnessU = new ConstantTexture<float>(0.1f);
		if(!roughnessV)
			this->roughnessV = this->roughnessU;
	}
	BSDF *SubstrateMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Bump(bump, *dg, ng, dg);

		unsigned int nBxDFs = 0;
		
		Spectrum r = rd->Evaluate(*dg);
		Spectrum s = rs->Evaluate(*dg);
		if(r.IsBlack() && s.IsBlack())
			return NULL;

		if(ior)
		{
			float i = ior->Evaluate(*dg);
			if(i > 0.f)
			{
				i = (i - 1.f) / (i + 1.f);
				s *= (i * i);
			}
		}

		Spectrum a = coatParams ? alpha->Evaluate(*dg).Clamp(0.f, 1.f) : 0.f;
		float ld = coatParams ? depth->Evaluate(*dg) : 0.f;

		float ru = roughnessU->Evaluate(*dg);
		float rv = roughnessV->Evaluate(*dg);

		MicrofacetDistribution *md = ru == rv ? (MicrofacetDistribution *) ARENA_ALLOC(arena, Blinn)(1.f / ru) : (MicrofacetDistribution *) ARENA_ALLOC(arena, Anisotropic)(1.f / ru, 1.f / rv);
		BxDF *bxdf = ARENA_ALLOC(arena, FresnelBlend)(r, s, a, ld, md);

		return ARENA_ALLOC(arena, SingleBxDF)(bxdf, *dg, ng);
	}
}