/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_METAL_H
#define RENDERLIB_MATERIALS_METAL_H

#include "Material.h"

namespace Render
{
	class MetalMaterial : public Material
	{
		RSpectrumTexture kr;
		RSpectrumTexture eta;
		RFloatTexture nu, nv;
		RFloatTexture bump;
	public:
		MetalMaterial(const RSpectrumTexture &kr, const RSpectrumTexture &eta, const RFloatTexture &nu, const RFloatTexture &nv, const RFloatTexture &bump = NULL);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};

	class SchlickMetalMaterial : public Material
	{
		RSpectrumTexture kr;
		RSpectrumTexture eta;
		RFloatTexture nu, nv;
		RFloatTexture bump;
	public:
		SchlickMetalMaterial(const RSpectrumTexture &kr, const RSpectrumTexture &eta, const RFloatTexture &nu, const RFloatTexture &nv, const RFloatTexture &bump = NULL);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif