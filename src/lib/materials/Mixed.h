/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_MIXED_H
#define RENDERLIB_MATERIALS_MIXED_H

#include "Material.h"

namespace Render
{
	class MixedMaterial : public Material
	{
		Reference<Material> m1;
		Reference<Material> m2;
		RFloatTexture scale;
	public:
		MixedMaterial(const Reference<Material> &m1, const Reference<Material> &m2, const RFloatTexture &scale);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif