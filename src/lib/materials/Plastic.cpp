/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "materials/Plastic.h"

namespace Render
{
	PlasticMaterial::PlasticMaterial(const RSpectrumTexture &kd, const RSpectrumTexture &ks, RFloatTexture roughness, const RFloatTexture &bump)
		:	kd(kd), ks(ks), roughness(roughness), bump(bump)
	{
		if(!roughness)
			this->roughness = new ConstantTexture<float>(0.1f);
	}

	BSDF *PlasticMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Bump(bump, *dg, ng, dg);

		unsigned int nBxDFs = 0;
		BxDF **bxdfs = (BxDF **) arena.Alloc(sizeof(BxDF *) * 2);

		Spectrum r = kd->Evaluate(*dg);
		if(!r.IsBlack())
			bxdfs[nBxDFs++] = ARENA_ALLOC(arena, Lambertian)(r);

		r = ks->Evaluate(*dg);
		if(!r.IsBlack())
			bxdfs[nBxDFs++] = ARENA_ALLOC(arena, Microfacet)(r, 
											ARENA_ALLOC(arena, SchlickDielectric)(0.66666666f),// 1 / 1.5
											ARENA_ALLOC(arena, Blinn)(1.f / roughness->Evaluate(*dg)));
		
		if(nBxDFs == 2)
			return ARENA_ALLOC(arena, MultiBxDF)(bxdfs, nBxDFs, *dg, ng);
		if(nBxDFs == 1)
			return ARENA_ALLOC(arena, SingleBxDF)(bxdfs[0], *dg, ng);
		else
			return NULL;
	}
}