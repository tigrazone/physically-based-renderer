/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "materials/Glossy.h"

namespace Render
{
	GlossyMaterial::GlossyMaterial(	const RSpectrumTexture &rd, const RSpectrumTexture &rs, const RFloatTexture &nu, const RFloatTexture &nv, 
									const RSpectrumTexture &alpha, const RFloatTexture &depth, const RFloatTexture &ior, bool multiBounce, const RFloatTexture &bump)
		:	rd(rd), rs(rs), nu(nu), nv(nv), alpha(alpha), depth(depth), ior(ior), multiBounce(multiBounce), bump(bump), coatParams(alpha && depth)
	{
		if(!nu)
			this->nu = new ConstantTexture<float>(0.1f);
		if(!nv)
			this->nv = this->nu;
	}

	BSDF *GlossyMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Bump(bump, *dg, ng, dg);
		Spectrum d = rd->Evaluate(*dg).Clamp(0.f, 1.f);
		Spectrum s = rs->Evaluate(*dg).Clamp(0.f, 1.f);

		if(d.IsBlack() && s.IsBlack())
			return NULL;

		if(ior)
		{
			float i = ior->Evaluate(*dg);
			if(i > 0.f)
			{
				i = (i - 1.f) / (i + 1.f);
				s *= (i * i);
			}
		}

		Spectrum a = coatParams ? alpha->Evaluate(*dg).Clamp(0.f, 1.f) : 0.f;
		float ld = coatParams ? depth->Evaluate(*dg) : 0.f;
		float u = Clamp(nu->Evaluate(*dg), 1e-3f, 1.f);
		float v = Clamp(nv->Evaluate(*dg), 1e-3f, 1.f);
		float sig = max(u * v, 1e-7f);
		u *= u;
		v *= v;
		u = u < v ? 1.f - u / v : v / u - 1.f;
		BxDF *bxdf = ARENA_ALLOC(arena, SchlickBRDF)(d, s, sig, u, a, ld, multiBounce);
		return ARENA_ALLOC(arena, SingleBxDF)(bxdf, *dg, ng);
	}
}