/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_SUBSTRATE_H
#define RENDERLIB_MATERIALS_SUBSTRATE_H

#include "Material.h"

namespace Render
{
	class SubstrateMaterial : public Material
	{
		RSpectrumTexture rd;
		RSpectrumTexture rs;
		RFloatTexture roughnessU;
		RFloatTexture roughnessV;
		RSpectrumTexture alpha;
		RFloatTexture depth;
		RFloatTexture ior;
		bool multiBounce;
		bool coatParams;
		RFloatTexture bump;
	public:
		SubstrateMaterial(	const RSpectrumTexture &rd, const RSpectrumTexture &rs, const RFloatTexture &roughnessU, const RFloatTexture &roughnessV,
							const RSpectrumTexture &alpha, const RFloatTexture &depth, const RFloatTexture &ior, const RFloatTexture &bump = NULL);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif