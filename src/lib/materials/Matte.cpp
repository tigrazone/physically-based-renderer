/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "materials/Matte.h"

namespace Render
{
	MatteMaterial::MatteMaterial(const RSpectrumTexture &kr, const RFloatTexture &sig, const RFloatTexture &bump)
		:	kr(kr), sig(sig), bump(bump)
	{
	}
	BSDF *MatteMaterial::GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const
	{
		Bump(bump, *dg, ng, dg);

		Spectrum r = kr->Evaluate(*dg);
		if(!r.IsBlack())
		{
			float sigma;
			if(sig && (sigma = sig->Evaluate(*dg)) > 0)
				return ARENA_ALLOC(arena, SingleBxDF)(ARENA_ALLOC(arena, OrenNayar)(r, sigma), *dg, ng);
			else
				return ARENA_ALLOC(arena, SingleBxDF)(ARENA_ALLOC(arena, Lambertian)(r), *dg, ng);
		}
		else
			return NULL;
	}
}