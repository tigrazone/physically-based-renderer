/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_MIRROR_H
#define RENDERLIB_MATERIALS_MIRROR_H

#include "Material.h"

namespace Render
{
	class MirrorMaterial : public Material
	{
		RSpectrumTexture kr;
		RFloatTexture bump;
	public:
		MirrorMaterial(const RSpectrumTexture &kr, const RFloatTexture &bump = NULL);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif