/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_MATERIALS_GLASS_H
#define RENDERLIB_MATERIALS_GLASS_H

#include "Material.h"

namespace Render
{
	class GlassMaterial : public Material
	{
		RSpectrumTexture kr;
		RSpectrumTexture kt;
		RFloatTexture ior;
		RFloatTexture bump;
	public:
		GlassMaterial(const RSpectrumTexture &kr, const RSpectrumTexture &kt, const RFloatTexture &ior, const RFloatTexture &bump = NULL);
		BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const;
	};
}

#endif