/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "ImageFilm.h"
#include "Memory.h"
#include "ImageIO.h"
#include "Sampler.h"
#include "Parallel.h"

namespace Render
{
	ImageFilm::ImageFilm(unsigned int width, unsigned int height)
		:	Film(width, height), image(AllocAligned<Pixel>(width * height))
	{
		memset(image, 0, sizeof(Pixel) * width * height);
	}

	ImageFilm::~ImageFilm()
	{
		FreeAligned(image);
	}

	void ImageFilm::WriteImage(const char *filename) const
	{
		float *img = new float[3 * width * height];
		unsigned int i = 0;
		for(unsigned int y = 0; y < height; ++y)
		{
			unsigned int row = y * width;
			for(unsigned int x = 0; x < width; ++x, i += 3)
			{
				Pixel *p = &image[row + x];
				float invWeight = p->weight == 0 ? 0.f : 1.f / p->weight;
				if(invWeight == 0.f)
					img[i] = img[i + 1] = img[i + 2] = 0.f;
				else
				{
					XYZToRGB(p->xyz, &img[i]);
					img[i] *= invWeight; img[i + 1] *= invWeight; img[i + 2] *= invWeight;
				}
			}
		}

		Render::WriteImage(filename, img, NULL, width, height, width, height, 0, 0);
		delete[] img;
	}

	const float *ImageFilm::GetPixelData() const
	{
		return floatData;
	}

	void ImageFilm::AddSample(const Sample *sample, const Spectrum &s)
	{
		unsigned int x = unsigned int(sample->x);
		unsigned int y = unsigned int(sample->y);
		if(x < 0 || x >= width || y < 0 || y >= height)
			return;

		Pixel &p = image[y * width + x];

		float xyz[3];
		s.ToXYZ(xyz);
		/*
			p.xyz[0] += xyz[0];
			p.xyz[1] += xyz[1];
			p.xyz[2] += xyz[2];
			++p.weight;
		*/
		if(isnanorinf(xyz[0]) || isnanorinf(xyz[1]) || isnanorinf(xyz[2]))
		{
			Warning("Bad radiance value.  Seed: %d.\n\n", sample->sampler->GetSeed());
			return;
		}
		AtomicAdd(&p.xyz[0], xyz[0]);
		AtomicAdd(&p.xyz[1], xyz[1]);
		AtomicAdd(&p.xyz[2], xyz[2]);
		AtomicAdd(&p.weight, 1.f);
	}

	void ImageFilm::SplatSample(float x, float y, const Spectrum &s)
	{
		unsigned int xPos = unsigned int(x);
		unsigned int yPos = unsigned int(y);
		if(xPos >= width || yPos >= height)
			return;

		Pixel &p = image[yPos * width + xPos];
		
		float xyz[3];
		s.ToXYZ(xyz);
		AtomicAdd(&p.xyz[0], xyz[0]);
		AtomicAdd(&p.xyz[1], xyz[1]);
		AtomicAdd(&p.xyz[2], xyz[2]);
	}
}