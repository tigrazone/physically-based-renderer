/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_FILMS_IMAGEFILM_H
#define RENDERLIB_FILMS_IMAGEFILM_H

#include "Film.h"

namespace Render
{
	//TODO: Eventually blocked arrays should be used (when pixel filters can be introduced), which will require an update to the preview OpenGL shader.
	class ImageFilm : public Film
	{
		struct Pixel
		{
			float xyz[3];
			float weight;
			Pixel() : weight(0.f) { xyz[0] = xyz[1] = xyz[2] = 0.f; }
		};

		union
		{
			Pixel *image;
			float *floatData;
		};
	public:
		ImageFilm(unsigned int width, unsigned int height);
		~ImageFilm();
		
		const float *GetPixelData() const;
		void WriteImage(const char *filename) const;
		void AddSample(const Sample *sample, const Spectrum &s);
		void SplatSample(float x, float y, const Spectrum &s);
	};
}

#endif