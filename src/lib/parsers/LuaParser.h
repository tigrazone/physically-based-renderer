/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_PARSERS_LUAPARSER_H
#define RENDERLIB_PARSERS_LUAPARSER_H

#include "Parser.h"
#include <cctype>
#include <cstring>
#include <cstdio>

extern "C"
{
#	include "lua.h"
#	include "lualib.h"
#	include "lauxlib.h"
}

namespace Render
{
	class LuaParser : public Parser
	{
		void initMetatable(lua_State *L, const char *name, luaL_Reg *methods) const;
		void initMetatables(lua_State *L);
		void registerGlobals(lua_State *L);
	public:
		LuaParser();
		~LuaParser();
		void Parse(const char *filename, RenderInstance *instance);
	};
};

#endif