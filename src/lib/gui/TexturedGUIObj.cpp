/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "gui/TexturedGUIObj.h"
#include "gui/CustomEventBinders.h"

namespace Render
{
	bool TexturedGUIObj::ProgramLinkEvent(GLLinkMap *data)
	{
		if(alphaNormalized)
			programGlobals = data->Link<AlphaNormalizedTextureProgram, TexturedGlobals>();
		else
			programGlobals = data->Link<TextureProgram, TexturedGlobals>();

		return true;
	}

	TexturedGUIObj::TexturedGUIObj(GUIEventBinder *evb, const GLuint *toRaster, const GLuint *texture, bool alphaNormalized)
		:	GUIObj(evb), alphaNormalized(alphaNormalized)
	{
		bufs.toRaster = toRaster; bufs.texture = texture;
		evb->Bind<GLProgramLinkListener<TexturedGUIObj>>(this, &TexturedGUIObj::ProgramLinkEvent);
	}

	void TexturedGUIObj::Draw()
	{
		programGlobals->Bind(*bufs.toRaster, bufs.toWorld, *bufs.texture, bufs.vertexArray, bufs.vertex, bufs.uv, 3, 2);
		glDrawArrays(bufs.vaMode, 0, bufs.nVertices);
	}
}