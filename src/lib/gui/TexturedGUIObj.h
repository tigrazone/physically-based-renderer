/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_GUI_TEXTUREDGUIOBJ_H
#define RENDERLIB_GUI_TEXTUREDGUIOBJ_H

#include "shaders/Base.h"

namespace Render
{
	//Done in a rather basic 'get it working' kind of way.  Needs improving.
	class TexturedGUIObj : public GUIObj
	{
	protected:
		const TexturedGlobals *programGlobals;
		bool ProgramLinkEvent(GLLinkMap *data);
	public:
		const bool alphaNormalized;
		TexturedGUIObj(GUIEventBinder *evb, const GLuint *toRaster, const GLuint *texture, bool AlphaNormalized = false);
		virtual void Draw();

		struct Bufs
		{
			GLuint vertexArray;
			GLuint toWorld;
			GLuint uv;
			GLuint vertex;

			GLuint nVertices;
			GLenum vaMode;

			const GLuint *toRaster;
			const GLuint *texture;
		} bufs;
	};
}

#endif