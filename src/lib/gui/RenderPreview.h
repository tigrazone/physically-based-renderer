/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_GUI_RENDERPREVIEW_H
#define RENDERLIB_GUI_RENDERPREVIEW_H

#include "shaders/Base.h"
#include "API.h"

namespace Render
{
	struct RenderThread;

	class RenderPreview : public GUIObj
	{
		RenderThread *renderThread;
		const RenderWindow &win;
		RenderInstance instance;
		Mat4 toWorld;

		struct Bufs
		{
			GLuint uv;
			GLuint toWorld;
			const GLuint *toRaster;
			const GLuint *vertexArray;
			const GLuint *vertices;
		} bufs;

		GLuint previewTexture;

		const TexturedGlobals *programGlobals;

		float imgWidth, imgHeight, aspect;
		const float *pixels;

		bool CreateEvent();
		bool SizeEvent(const SizeData *data);
		bool ProgramLinkEvent(GLLinkMap *data);
		UserEventSignal updateSignal;
	public:
		RenderPreview(GUIEventBinder *evb, const GLuint *toRasterBuf, const GLuint *vertexArray, const GLuint *vertexBuffer, const RenderWindow &win);
		~RenderPreview();

		bool HasScene() const;
		void Save(const char *filename);
		void Load(const char *filename);
		void Pause();
		void Resume();
		bool Update();

		void Draw();
	};
}

#endif