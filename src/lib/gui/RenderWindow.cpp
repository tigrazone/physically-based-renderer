/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "gui/RenderWindow.h"
#include "gui/RenderPreview.h"
#include "gui/TexturedGUIObj.h"
#include "gui/CustomEventBinders.h"
#include "ImageIO.h"

#define nIconButtons 7
#define nMenuButtons 3
#define nSysButtons 4
#define nBackPanels 2
#define iconHeight 16.f
#define iconWidth 16.f
#define iconMarginWidth 4.f
#define iconMarginHeight 4.f

//These extensions use a windows format.  The open file dialog method should handle this for other systems.
#define RENDERSCRIPTEXTS "Script Files (*.lua)\0*.lua\0\0"
#define RENDERIMGEXTS "EXR (*.exr)\0*.exr\0PNG (*.png)\0*.png\0\0"
namespace Render
{
	//TODO: Alot of the GUI elements are done with a most basic and non-extensible just-get-it-done-and-working mentality.  They are prevalent in the size-change and mouse-move events.
	//If necessary, may require a bit of overhauling to promote extensibility when more complex controls are introduced.

	bool RenderWindow::DrawEvent()
	{
		if(windowState & State::MINIMIZED)
			return false;

		Draw();
		return false;
	}

	bool RenderWindow::InitEvent()
	{
		ctx.Acquire();
		glClearColor(0, 0, 0, 0);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		GLfloat vert[12] =
		{
			0.f, 0.f, 0.f,
			1.f, 0.f, 0.f,
			1.f, 1.f, 0.f,
			0.f, 1.f, 0.f
		};

		glGenVertexArrays(1, &vertexArray);
		glGenBuffers(3, &bufs.vertex);

		glBindBuffer(GL_UNIFORM_BUFFER, bufs.toRaster);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(float) * 16, toRaster.mm, GL_DYNAMIC_DRAW);

		glBindVertexArray(vertexArray);

		glBindBuffer(GL_ARRAY_BUFFER, bufs.vertex);
		glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), vert, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);

		GLfloat uv[8] = {};
		glBindBuffer(GL_ARRAY_BUFFER, bufs.uv);
		glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), uv, GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);

		GLShaderObjectMap shaderMap;

		GLLinkMap m = {&shaderMap, &programMap};
		evb->Signal(GLProgramLinkListener<RenderWindow>::eventID, &m);
		return true;
	}

	bool RenderWindow::CreateEvent()
	{
		//These elements are statically sized, and very easy to move about, so just do sizing initially, and pass in translations later on as simple 2D float arrays.
		Mat4 toWorld;
		toWorld[0][0] = float(pixelsX);
		toWorld[1][1] = iconHeight + 2.f * iconMarginHeight;

		for(int i = 0; i < nBackPanels; ++i)
		{
			if(i == 1)
				toWorld[0][0] = nMenuButtons * (iconMarginWidth * 2.f + iconWidth);

			backPanels[i]->bufs.nVertices = 4;
			backPanels[i]->bufs.uv = bufs.uv;
			backPanels[i]->bufs.vaMode = GL_QUADS;
			backPanels[i]->bufs.vertex = bufs.vertex;
			backPanels[i]->bufs.vertexArray = vertexArray;
			glGenBuffers(1, &backPanels[i]->bufs.toWorld);
			glBindBuffer(GL_UNIFORM_BUFFER, backPanels[i]->bufs.toWorld);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(float) * 16, toWorld.mm, GL_DYNAMIC_DRAW);
		}

		toWorld[0][0] = iconWidth;
		toWorld[1][1] = iconHeight;
		toWorld[3][1] = iconMarginHeight;

		float uv[8] =
		{
			0.f, 0.f,
			iconWidth, 0.f,
			iconWidth, iconHeight,
			0.f, iconHeight
		};

		for(int i = 0; i < nIconButtons; ++i)
		{
			if(i == nSysButtons)
				toWorld[3][0] = iconMarginWidth;
			else if(i > nSysButtons)
				toWorld[3][0] += iconMarginWidth * 2 + iconWidth;

			iconButtons[i]->bufs.nVertices = 4;
			iconButtons[i]->bufs.vaMode = GL_QUADS;
			iconButtons[i]->bufs.vertex = bufs.vertex;
			iconButtons[i]->bufs.vertexArray = vertexArray;
			glGenBuffers(2, &iconButtons[i]->bufs.toWorld);
			glBindBuffer(GL_UNIFORM_BUFFER, iconButtons[i]->bufs.toWorld);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(float) * 16, toWorld.mm, GL_DYNAMIC_DRAW);
			if(i >= nSysButtons)
			{
				uv[0] = uv[6] = iconWidth * (i + 2);
				uv[2] = uv[4] = iconWidth * (i + 3);
			}
			else
			{
				uv[0] = uv[6] = iconWidth * i;
				uv[2] = uv[4] = iconWidth * (i + 1);
			}
			glBindBuffer(GL_ARRAY_BUFFER, iconButtons[i]->bufs.uv);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8, uv, GL_DYNAMIC_DRAW);
		}

		toWorld[0][0] = 2 * iconMarginWidth + iconWidth;
		toWorld[1][1] = 2 * iconMarginHeight + iconHeight;

		buttonHighlight->bufs.nVertices = 4;
		buttonHighlight->bufs.vaMode = GL_QUADS;
		buttonHighlight->bufs.vertex = bufs.vertex;
		buttonHighlight->bufs.vertexArray = vertexArray;
		buttonHighlight->bufs.uv = bufs.uv;
		glGenBuffers(1, &buttonHighlight->bufs.toWorld);
		glBindBuffer(GL_UNIFORM_BUFFER, buttonHighlight->bufs.toWorld);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(float) * 16, toWorld.mm, GL_DYNAMIC_DRAW);

		glGenTextures(4, &bufs.iconsTexture);
		glBindTexture(GL_TEXTURE_RECTANGLE, bufs.iconsTexture);
		
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		int width, height;
		float *icons = ReadImage("./icons/Icons16.png", &width, &height);

		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, icons);

		delete[] icons;

		glBindTexture(GL_TEXTURE_RECTANGLE, bufs.paneColor);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		float color[4] = {0.9f, 0.9f, 0.9f, 0.7f};

		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA16F, 1, 1, 0, GL_RGBA, GL_FLOAT, color);

		glBindTexture(GL_TEXTURE_RECTANGLE, bufs.hoverColor);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		color[0] = color[1] = color[2] = 0.95f;
		color[3] = 0.8f;

		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA16F, 1, 1, 0, GL_RGBA, GL_FLOAT, color);

		glBindTexture(GL_TEXTURE_RECTANGLE, bufs.downColor);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		color[0] = color[1] = color[2] = 0.05f;
		color[3] = 0.8f;

		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA16F, 1, 1, 0, GL_RGBA, GL_FLOAT, color);

		return true;
	}

	bool RenderWindow::LMouseDownEvent(const ScreenPoint *data, EventResponse *response)
	{
		downButton = activeButton;
		activeButton = -1;
		return HitTestEvent(data, response);
	}

	bool RenderWindow::LMouseUpEvent(const ScreenPoint *data, EventResponse *response)
	{
		if(activeButton == downButton)
		{
			switch(activeButton)
			{
				//Each of the screen state changes updates the resulting new point.
				case 0:
				{
					Minimize();
					break;
				}
				case 1:
					if(windowState == State::NORMAL)
					{
						Maximize();
					}
					else
					{
						GUIWindowRestore();
					}
					break;
				case 2:
					if(windowState & State::FULLSCREEN)
					{
						GUIWindowRestore();
					}
					else
					{
						FullScreen();
					}
					break;
				case 3:
					GenericEventSignal(this, Event::CLOSE).Signal();
					break;
				case 4:
				{
					string filename;
					if(GetFile(RENDERSCRIPTEXTS, filename))
						preview->Load(filename.c_str());

					break;
				}
				case 5:
				{
					string filename;
					if(SaveFile(RENDERIMGEXTS, filename))
						preview->Save(filename.c_str());
					break;
				}
				case 6:
					preview->Update();
			}
		}
		
		ScreenPoint newPoint = {};
		GetCursorPos(&newPoint);
		
		downButton = -1;
		
		if(activeButton != -1)
		{
			float v1 = 0, v2 = iconHeight;
			glBindBuffer(GL_ARRAY_BUFFER, iconButtons[activeButton]->bufs.uv);
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(float), sizeof(float), &v1);
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 3, sizeof(float), &v1);
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 5, sizeof(float), &v2);
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 7, sizeof(float), &v2);
		
			buttonHighlight->bufs.texture = &bufs.hoverColor;
			
			activeButton = nIconButtons;//force a redraw
			
			HitTestEvent(&newPoint, response);
			return false;
		}

		return true;
	}

	bool RenderWindow::HitTestEvent(const ScreenPoint *data, EventResponse *response)
	{
		float iconButtonstartX = currentPixelsX - nSysButtons * (iconMarginWidth * 2.f + iconWidth);
		if(windowState & State::FULLSCREEN)
			iconButtonstartX += iconWidth + 2.f * iconMarginWidth;

		int newActiveButton = -1;
		float iconButtonstartY = BORDERPADDING;
		bool requiresRedraw = false;

		if(data->y >= iconButtonstartY && data->y < iconHeight + iconMarginHeight * 2)
		{
			if(data->x >= iconButtonstartX && data->x < currentPixelsX - BORDERPADDING)
			{
				if(windowState & State::FULLSCREEN)
				{
					newActiveButton = int(float(nSysButtons - 1) * (data->x - iconButtonstartX) / (currentPixelsX - iconButtonstartX));
					if(newActiveButton > 0)
						++newActiveButton;
				}
				else
					newActiveButton = int(float(nSysButtons) * (data->x - iconButtonstartX) / (currentPixelsX - iconButtonstartX));
			}
			else
				newActiveButton = 4 + int(float(nMenuButtons) * (data->x) / (nMenuButtons * (2.f * iconMarginWidth + iconWidth)));
		}

		bool onDisabled = (!preview->HasScene() && newActiveButton > nSysButtons && newActiveButton < nSysButtons + 3);

		if(newActiveButton < 0 || newActiveButton >= nIconButtons || onDisabled)
				newActiveButton = -1;

		if(newActiveButton != activeButton)
		{
			float v[2];
			
			if(newActiveButton != -1 && (downButton == -1 || newActiveButton == downButton))
			{
				if(newActiveButton < nIconButtons)
				{
					v[0] = downButton == -1 ? iconHeight : iconHeight * 2.f;
					v[1] = v[0] + iconHeight;
					glBindBuffer(GL_ARRAY_BUFFER, iconButtons[newActiveButton]->bufs.uv);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float), sizeof(float), &v[0]);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 3, sizeof(float), &v[0]);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 5, sizeof(float), &v[1]);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 7, sizeof(float), &v[1]);

					buttonHighlight->bufs.texture = downButton == -1 ? &bufs.hoverColor : &bufs.downColor;
					v[0] = newActiveButton >= nSysButtons ? (newActiveButton - 4) * (2 * iconMarginWidth + iconWidth) : iconButtonstartX + newActiveButton * (2 * iconMarginWidth + iconWidth);
					if(windowState & State::FULLSCREEN && newActiveButton > 0 && newActiveButton < nSysButtons)
						v[0] -= 2 * iconMarginWidth + iconWidth;
					v[1] = 0;

					glBindBuffer(GL_UNIFORM_BUFFER, buttonHighlight->bufs.toWorld);
					glBufferSubData(GL_UNIFORM_BUFFER, sizeof(float) * 12, sizeof(float) * 2, v);
				}

				requiresRedraw = true;
			}
			
			if(activeButton != -1)
			{
				if(activeButton < nIconButtons)
				{
					v[0] = 0, v[1] = iconHeight;

					glBindBuffer(GL_ARRAY_BUFFER, iconButtons[activeButton]->bufs.uv);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float), sizeof(float), &v[0]);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 3, sizeof(float), &v[0]);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 5, sizeof(float), &v[1]);
					glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 7, sizeof(float), &v[1]);
				}

				requiresRedraw = true;
			}
		}

		activeButton = newActiveButton;

		if(requiresRedraw)
			drawEvent.Signal();

		return downButton == -1 && !onDisabled;
	}

	bool RenderWindow::SizeEvent(const SizeData *data)
	{
		if(data->state & State::MINIMIZED)
			return false;

		float nx = float(data->nx), ny = float(data->ny);

		float trans[2] = {nx - nSysButtons *(iconMarginWidth * 2.f + iconWidth)};
		if(data->state & State::FULLSCREEN)
			trans[0] += iconWidth + 2.f * iconMarginWidth;
		
		glBindBuffer(GL_UNIFORM_BUFFER, backPanels[0]->bufs.toWorld);
		glBufferSubData(GL_UNIFORM_BUFFER, sizeof(float) * 12, sizeof(float), &trans[0]);

		trans[0] += iconMarginWidth;
		for(int i = 0; i < nSysButtons; ++i)
		{
			if(i == 1 && data->state & State::FULLSCREEN)
				continue;

			glBindBuffer(GL_UNIFORM_BUFFER, iconButtons[i]->bufs.toWorld);
			glBufferSubData(GL_UNIFORM_BUFFER, sizeof(float) * 12, sizeof(float), trans);
			trans[0] += 2.f * iconMarginWidth + iconWidth;
		}

		if(data->state & State::FULLSCREEN)
		{
			if(!sysIconState[1])
			{
				trans[0] = iconWidth * 5;
				trans[1] = iconWidth * 6;
				glBindBuffer(GL_ARRAY_BUFFER, iconButtons[2]->bufs.uv);
				glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 6, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 2, sizeof(float), &trans[1]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 4, sizeof(float), &trans[1]);
				sysIconState[1] = true;
			}
		}
		else if(data->state & State::MAXIMIZED)
		{
			if(!sysIconState[0])
			{
				trans[0] = iconWidth * 4;
				trans[1] = iconWidth * 5;
				glBindBuffer(GL_ARRAY_BUFFER, iconButtons[1]->bufs.uv);
				glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 6, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 2, sizeof(float), &trans[1]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 4, sizeof(float), &trans[1]);
				sysIconState[1] = true;
			}
			if(sysIconState[1])
			{
				trans[0] = iconWidth * 2;
				trans[1] = iconWidth * 3;
				glBindBuffer(GL_ARRAY_BUFFER, iconButtons[2]->bufs.uv);
				glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 6, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 2, sizeof(float), &trans[1]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 4, sizeof(float), &trans[1]);
				sysIconState[0] = true;
				sysIconState[1] = false;
			}
		}
		else if(data->state == State::NORMAL)
		{
			if(sysIconState[0])
			{
				trans[0] = iconWidth * 1;
				trans[1] = iconWidth * 2;
				glBindBuffer(GL_ARRAY_BUFFER, iconButtons[1]->bufs.uv);
				glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 6, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 2, sizeof(float), &trans[1]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 4, sizeof(float), &trans[1]);
				sysIconState[0] = false;
			}
			if(sysIconState[1])
			{
				trans[0] = iconWidth * 2;
				trans[1] = iconWidth * 3;
				glBindBuffer(GL_ARRAY_BUFFER, iconButtons[2]->bufs.uv);
				glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 6, sizeof(float), &trans[0]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 2, sizeof(float), &trans[1]);
				glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 4, sizeof(float), &trans[1]);
				sysIconState[1] = false;
			}
		}

		glViewport(0, 0, data->nx, data->ny);

		toRaster[0][0] = 2.f / float(data->nx);
		toRaster[1][1] = -2.f / float(data->ny);
		toRaster[3][0] = -1.f;
		toRaster[3][1] = 1.f;

		glBindBuffer(GL_UNIFORM_BUFFER, bufs.toRaster);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(float) * 14, toRaster.mm);

		return GUIWindowSize(data);
	}

	RenderWindow::RenderWindow()
		:	GUIWindow("Renderer", static_cast<State>(opts.windowState)), drawEvent(this, Event::DRAW), activeButton(-1), downButton(-1)
	{
		sysIconState[0] = sysIconState[1] = false;
		int sX, sY;
		if(opts.pixelsX == 0 || opts.pixelsY == 0 || !GetScreenSize(opts.posX, opts.posY, &sX, &sY))
		{
			opts.posX = opts.posY = 1;
			if(!GetScreenSize(opts.posX, opts.posY, &sX, &sY))
			{
				Warning("Could not retrieve screen size.  Using default metrics.");
				opts.posX = opts.posY = 0;
				opts.pixelsX = opts.pixelsY = 500;
			}
			else
				CenterQuarterScreen(sX, sY, &opts.posX, &opts.posY, &opts.pixelsX, &opts.pixelsY);
		}

		posX = opts.posX; pixelsX = opts.pixelsX;
		posY = opts.posY; pixelsY = opts.pixelsY;

		evb->Bind<GenericListener<Event::INIT, RenderWindow>>(this, &RenderWindow::InitEvent);
		evb->Bind<GenericListener<Event::DRAW, RenderWindow>>(this, &RenderWindow::DrawEvent);
		evb->Bind<SizeListener<RenderWindow>>(this, &RenderWindow::SizeEvent);
		evb->Bind<GenericListener<Event::CREATE, RenderWindow>>(this, &RenderWindow::CreateEvent);

		evb->Bind<GenericListener<Event::RESTORE, GUIWindow>>(this, &GUIWindow::GUIWindowRestore);
		evb->Bind<GenericListener<Event::MAXIMIZE, GUIWindow>>(this, &GUIWindow::GUIWindowMaximize);
		evb->Bind<GenericListener<Event::MINIMIZE, GUIWindow>>(this, &GUIWindow::GUIWindowMinimize);

		evb->Bind<MouseEventListener<Event::LMOUSEDOWN, RenderWindow>>(this, &RenderWindow::LMouseDownEvent);
		evb->Bind<MouseEventListener<Event::LMOUSEUP, RenderWindow>>(this, &RenderWindow::LMouseUpEvent);
		evb->Bind<HitTestListener<RenderWindow>>(this, &RenderWindow::HitTestEvent);
		evb->Bind<HitTestListener<GUIWindow>>(this, &GUIWindow::GUIWindowHitTest);
		evb->Bind<MouseEventListener<Event::LMOUSEDOWN, GUIWindow>>(this, &GUIWindow::GUIWindowHitTest);
		evb->Bind<MoveListener<GUIWindow>>(this, &GUIWindow::GUIWindowMove);

		preview = new RenderPreview(evb, &bufs.toRaster, &vertexArray, &bufs.vertex, *this);

		for(int i = 0; i < nBackPanels; ++i)
			backPanels[i] = new TexturedGUIObj(evb, &bufs.toRaster, &bufs.paneColor);

		for(int i = 0; i < nIconButtons; ++i)
			iconButtons[i] = new TexturedGUIObj(evb, &bufs.toRaster, &bufs.iconsTexture);

		buttonHighlight = new TexturedGUIObj(evb, &bufs.toRaster, &bufs.hoverColor);
	}

	RenderWindow::~RenderWindow()
	{
		delete preview;

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindTexture(GL_TEXTURE_RECTANGLE, 0);
		glBindTexture(GL_TEXTURE_1D, 0);

		glDeleteTextures(4, &bufs.iconsTexture);
		glDeleteVertexArrays(1, &vertexArray);
		glDeleteBuffers(3, &bufs.vertex);

		for(int i = 0; i < nBackPanels; ++i)
		{
			glDeleteBuffers(1, &backPanels[i]->bufs.toWorld);
			delete backPanels[i];
		}

		for(int i = 0; i < nIconButtons; ++i)
		{
			glDeleteBuffers(2, &iconButtons[i]->bufs.toWorld);
			delete iconButtons[i];
		}

		glDeleteBuffers(1, &buttonHighlight->bufs.toWorld);

		delete buttonHighlight;
	}

	void RenderWindow::Draw()
	{
		glClear(GL_COLOR_BUFFER_BIT);
		
		preview->Draw();

		for(int i = 0; i < nBackPanels; ++i)
			backPanels[i]->Draw();

		if(activeButton != -1 && (activeButton == downButton || downButton == -1))
			buttonHighlight->Draw();

		for(int i = 0; i < nIconButtons; ++i)
		{
			if(windowState & State::FULLSCREEN && i == 1)
				continue;
			iconButtons[i]->Draw();
		}

		ctx.Swap();
		glFinish();
	}

	void RenderWindow::Run()
	{
		evb->Bind<GenericListener<Event::CREATE, GUIWindow>>(this, &GUIWindow::GUIWindowCreate);
		GUIWindow::Run();
		
		opts.posX = posX;
		opts.posY = posY;
		opts.pixelsX = pixelsX;
		opts.pixelsY = pixelsY;
		opts.windowState = windowState;
	}
}