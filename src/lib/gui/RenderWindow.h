/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_GUI_RENDERWINDOW_H
#define RENDERLIB_GUI_RENDERWINDOW_H

#include "gui/RenderPreview.h"
#include "gui/TexturedGUIObj.h"

namespace Render
{
	class RenderWindow : public GUIWindow
	{
		//In the case of gui elements, raster space is world space, where the bottom-left if -width/2, -height/2.
		Mat4 toRaster;

		//Standard single-pixel quad.  Lower left corner is at the center of the screen.
		GLuint vertexArray;
		struct Bufs
		{
			GLuint vertex;
			GLuint toRaster;
			//Single pixels texture uv.
			GLuint uv;
			GLuint iconsTexture;
			GLuint paneColor;
			GLuint hoverColor;
			GLuint downColor;
		} bufs;

		GLProgramLinkMap programMap;

		//Temporary lazy implementation of GUI elements.
		RenderPreview *preview;
		TexturedGUIObj *backPanels[3];
		TexturedGUIObj *iconButtons[7];
		TexturedGUIObj *buttonHighlight;
		
		int activeButton, downButton;//Related to the system buttons
		bool sysIconButtonState[4];//false - not pressed, true - pressed
		bool sysIconState[2];//false - maximize/toggle-fullscreen, true - restore/toggle-windowed

		bool LMouseDownEvent(const ScreenPoint *data, EventResponse *response);
		bool LMouseUpEvent(const ScreenPoint *data, EventResponse *response);
		bool InitEvent();
		bool CreateEvent();
		bool DrawEvent();
		bool HitTestEvent(const ScreenPoint *data, EventResponse *response);
		bool SizeEvent(const SizeData *);
	public:
		const GenericEventSignal drawEvent;

		RenderWindow();
		~RenderWindow();

		void Run();
		void Draw();
	};
}

#endif