/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include <boost/thread.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <limits>

#include "gui/RenderPreview.h"
#include "gui/RenderWindow.h"

#include "ImageIO.h"
#include "gui/CustomEventBinders.h"
namespace Render
{
	class RenderPreviewStatus : public RenderStatus
	{
		unsigned int nThreads;
		unsigned int nWorking;
		unsigned int nextUpdate;
		unsigned int updateInterval;
		boost::mutex *update;
		boost::mutex *working;
		boost::condition_variable *stopped;
		bool stop;
		const UserEventSignal *updateSignal;
	public:
		RenderPreviewStatus(MemoryArena &arena, const UserEventSignal *updateSignal, unsigned int nThreads)
			:	updateSignal(updateSignal), nextUpdate(1), updateInterval(1), nThreads(nThreads), nWorking(0), stop(false)
		{
			update = arena.Alloc<boost::mutex>(1);
			working = arena.Alloc<boost::mutex>(1);
			stopped = arena.Alloc<boost::condition_variable>(1);
		}
		~RenderPreviewStatus()
		{
			update->~mutex();
			working->~mutex();
			stopped->~condition_variable();
		}
		void SetResumeState()
		{
			if(nWorking != 0)
				return;

			stop = false;
			nWorking = 0;
		}
		void StopAndWait()
		{
			stop = true;
			boost::unique_lock<boost::mutex> lock(*working);
			while(nWorking > 0)
				stopped->wait(lock);
		}

		void StartSignal()
		{
			boost::unique_lock<boost::mutex> lock(*working);
			++nWorking;
		}
		void StopSignal()
		{
			boost::unique_lock<boost::mutex> lock(*working);
			if(--nWorking == 0)
				stopped->notify_all();
		}

		void Reset()
		{
			boost::lock_guard<boost::mutex> lock(*update);
			stop = false;
			nextUpdate = 1;
			updateInterval = 1;
			nWorking = 0;
		}
		bool Update(unsigned int nPasses)
		{
			bool doUpdate;
			{
				boost::lock_guard<boost::mutex> lock(*update);
				doUpdate = nPasses >= nextUpdate;
				if(doUpdate)
				{
					nextUpdate += updateInterval;
					updateInterval += 1;
				}
			}

			if(doUpdate)
				updateSignal->Signal();

			return nPasses < UINT_MAX && !stop;
		}
	};

	struct RenderThread
	{
		struct Callable
		{
			RenderThread *rThread;
			Callable(RenderThread *rThread)
				:	rThread(rThread){}
			void operator()()
			{
				while(true)
				{
					rThread->fileLoaded->wait();
					if(rThread->stop)
						return;

					rThread->renderStatus->StartSignal();

					{
						Renderer *r = rThread->renderer;

						auto now = boost::posix_time::microsec_clock::local_time();
						Info("Rendering started %s %d, %d %02d:%02d:%02d.", 
							now.date().month().as_short_string(), 
							now.date().day().as_number(), 
							unsigned int(now.date().year()), 
							unsigned int(now.time_of_day().hours()), 
							unsigned int(now.time_of_day().minutes()), 
							unsigned int(now.time_of_day().seconds()));
						r->Render(rThread->renderStatus);
					}

					rThread->renderStatus->StopSignal();
				}
			}
		};

		Renderer *renderer;
		boost::interprocess::interprocess_semaphore *fileLoaded;
		boost::thread *thread;
		RenderPreviewStatus *renderStatus;
		bool stop;
		MemoryArena arena;
		RenderThread(const UserEventSignal *updateSignal)
			:	stop(false)
		{
			fileLoaded = (boost::interprocess::interprocess_semaphore *) arena.Alloc(sizeof(boost::interprocess::interprocess_semaphore));
			new (fileLoaded) boost::interprocess::interprocess_semaphore(0);
			renderStatus = (RenderPreviewStatus *) arena.Alloc(sizeof(RenderPreviewStatus));
			new (renderStatus) RenderPreviewStatus(arena, updateSignal, 1);
			thread = (boost::thread*) arena.Alloc(sizeof(boost::thread));
			new (thread) boost::thread(Callable(this));
		}
		~RenderThread()
		{
			stop = true;
			fileLoaded->post();
			renderStatus->StopAndWait();
			if(thread->joinable()) thread->join();
			thread->~thread();
			fileLoaded->~interprocess_semaphore();
			renderStatus->~RenderPreviewStatus();
		}
	};

	bool RenderPreview::CreateEvent()
	{
		glGenBuffers(2, (GLuint *) &bufs);
		glBindBuffer(GL_UNIFORM_BUFFER, bufs.toWorld);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(float) * 16, toWorld.mm, GL_DYNAMIC_DRAW);

		glGenTextures(1, &previewTexture);
		glBindTexture(GL_TEXTURE_RECTANGLE, previewTexture);
		
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		GLfloat uv[8] =
		{
			0.f, 0.f,
			imgWidth, 0.f,
			imgWidth, imgHeight,
			0.f, imgHeight
		};
		
		glBindVertexArray(*bufs.vertexArray);
		
		glBindBuffer(GL_ARRAY_BUFFER, bufs.uv);
		glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), uv, GL_STATIC_DRAW);
		
		return true;
	}

	bool RenderPreview::SizeEvent(const SizeData *data)
	{
		float wndAspect = float(data->nx) / float(data->ny);
		if(aspect == 0)
			toWorld[0][0] = toWorld[1][1] = 0.f;
		else if(wndAspect > aspect)
		{
			toWorld[1][1] = float(data->ny);
			toWorld[0][0] = aspect * float(data->ny);
		}
		else
		{
			toWorld[0][0] = float(data->nx);
			toWorld[1][1] = float(data->nx) / aspect;
		}

		//OpenGL is column major.
		toWorld[3][0] = 0.5f * (float(data->nx) - toWorld[0][0]);
		toWorld[3][1] = 0.5f * (float(data->ny) - toWorld[1][1]);

		glBindBuffer(GL_UNIFORM_BUFFER, bufs.toWorld);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(float) * 14, toWorld.mm);

		return true;
	}

	bool RenderPreview::ProgramLinkEvent(GLLinkMap *data)
	{
		programGlobals = data->Link<XYZAlphaNormalizedTextureProgram, TexturedGlobals>();
		return true;
	}

	RenderPreview::RenderPreview(GUIEventBinder *evb, const GLuint *toRasterBuf, const GLuint *vertexArray, const GLuint *vertexBuffer, const RenderWindow &win)
		:	GUIObj(evb), imgWidth(50.f), imgHeight(50.f), aspect(1.f), win(win), renderThread(new RenderThread(&updateSignal)), updateSignal(&win)
	{
		bufs.toRaster = toRasterBuf; bufs.vertexArray = vertexArray; bufs.vertices = vertexBuffer;
		evb->Bind<GLProgramLinkListener<RenderPreview>>(this, &RenderPreview::ProgramLinkEvent);
		evb->Bind<GenericListener<Event::CREATE, RenderPreview>>(this, &RenderPreview::CreateEvent);
		evb->Bind<SizeListener<RenderPreview>>(this, &RenderPreview::SizeEvent);
		evb->Bind<RenderPreviewUpdateListener>(this, &RenderPreview::Update);
		updateSignal.SetParams(RenderPreviewUpdateListener::eventID, NULL);
	}

	bool RenderPreview::HasScene() const
	{
		return instance.IsValid();
	}

	void RenderPreview::Load(const char *filename)
	{
		renderThread->renderStatus->StopAndWait();
		instance.Parse(filename);

		if(!instance.IsValid())
			return;

		renderThread->renderStatus->Reset();

		Renderer *r = instance.GetRenderer();
		pixels = r->camera->film->GetPixelData();

		renderThread->renderer = r;

		imgWidth = float(r->camera->film->width);
		imgHeight = float(r->camera->film->height);

		aspect = imgWidth / imgHeight;
		
		GLfloat uv[8] =
		{
			0.f, 0.f,
			imgWidth, 0.f,
			imgWidth, imgHeight,
			0.f, imgHeight
		};
		
		glBindBuffer(GL_ARRAY_BUFFER, bufs.uv);
		glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(GLfloat), uv);

		SizeData d = {};
		win.GetWindowSize(&d.nx, &d.ny);

		SizeEvent(&d);

		glBindTexture(GL_TEXTURE_RECTANGLE, previewTexture);
		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA16F, GLsizei(imgWidth), GLsizei(imgHeight), 0, GL_RGBA, GL_FLOAT, NULL);

		renderThread->fileLoaded->post();
	}

	void RenderPreview::Save(const char *filename)
	{
		if(!HasScene())
			return;

		renderThread->renderStatus->StopAndWait();
		Info("Rendering paused, saving to file %s.", filename);
		instance.GetRenderer()->camera->film->WriteImage(filename);
		Info("Rendering resumed.");
		renderThread->renderStatus->SetResumeState();
		renderThread->fileLoaded->post();
	}

	void RenderPreview::Pause()
	{
		renderThread->renderStatus->StopAndWait();
		Info("Rendering paused.");
	}
	void RenderPreview::Resume()
	{
		renderThread->renderStatus->SetResumeState();
		Info("Rendering resumed.");
		renderThread->fileLoaded->post();
	}

	bool RenderPreview::Update()
	{
		if(!HasScene())
			return true;

		glBindTexture(GL_TEXTURE_RECTANGLE, previewTexture);
		glTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, GLsizei(imgWidth), GLsizei(imgHeight), GL_RGBA, GL_FLOAT, pixels);
		win.drawEvent.Signal();

		return true;
	}

	RenderPreview::~RenderPreview()
	{
		delete renderThread;

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDeleteBuffers(2, (GLuint *) &bufs);
	}

	void RenderPreview::Draw()
	{
		if(!programGlobals->handle || aspect == 0)
			return;

		programGlobals->Bind(*bufs.toRaster, bufs.toWorld, previewTexture, *bufs.vertexArray, *bufs.vertices, bufs.uv, 3, 2);
		glDrawArrays(GL_QUADS, 0, 4);
	}
}