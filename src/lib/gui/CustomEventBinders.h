/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_GUI_CUSTOMEVENTLISTENERS_H
#define RENDERLIB_GUI_CUSTOMEVENTLISTENERS_H

#include "GUI.h"
namespace Render
{
	struct GLLinkMap;
	template<class GUIType>
	class GLProgramLinkListener : public CustomEventListener<0, GUIType, GLLinkMap*>
	{
	public:
		GLProgramLinkListener(GUIType *obj, Handler f) : CustomEventListener(obj, f){}
	};

	class RenderPreview;
	class RenderPreviewUpdateListener : public SimpleCustomEventListener<1, RenderPreview>
	{
	public:
		RenderPreviewUpdateListener(RenderPreview *obj, Handler f) : SimpleCustomEventListener(obj, f){}
	};
}
#endif