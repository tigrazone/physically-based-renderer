/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_RENDERERS_PROGRESSIVERENDERER_H
#define RENDERLIB_RENDERERS_PROGRESSIVERENDERER_H

#include "Renderer.h"
namespace Render
{
	class ProgressiveRenderer : public Renderer
	{
		vector<Task *> tasks;
		TaskSet *taskset;
		unsigned int passes;
		float time;
	public:
		ProgressiveRenderer(Scene *scene, Camera *camera, Sampler *sampler, SurfaceIntegrator *surf, VolumeIntegrator *vol);
		~ProgressiveRenderer();
		void ProgressiveRenderer::Render(RenderStatus *status);
		Spectrum Li(Intersection *isect, Sample *sample, Ray &ray, MemoryArena &arena) const;
		friend struct ProgressiveRenderTask;
	};
}
#endif