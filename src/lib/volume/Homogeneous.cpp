#include "volume/Homogeneous.h"

namespace Render
{
	HomogeneousVolume::HomogeneousVolume(const Transform &toWorld, const BBox &bbox, const Spectrum &sig_a, const Spectrum &sig_s, const Spectrum &le, float g)
		:	VolumeRegion(toWorld, bbox), sig_a(sig_a), sig_s(sig_s), le(le), g(g){}

	Spectrum HomogeneousVolume::SigmaA(const Point &p, const Vector &w, float time) const
	{
		return bounds.Inside(toWorld(p, true)) ? sig_a : 0.f;
	}
	Spectrum HomogeneousVolume::SigmaS(const Point &p, const Vector &w, float time) const
	{
		return bounds.Inside(toWorld(p, true)) ? sig_s : 0.f;
	}
	Spectrum HomogeneousVolume::SigmaT(const Point &p, const Vector &w, float time) const
	{
		return bounds.Inside(toWorld(p, true)) ? sig_s + sig_a : 0.f;
	}
	Spectrum HomogeneousVolume::Lve(const Point &p, const Vector &w, float time) const
	{
		return bounds.Inside(toWorld(p, true)) ? le : 0.f;
	}
	float HomogeneousVolume::P(const Point &p, const Vector &w, const Vector &wp, float time) const
	{
		return bounds.Inside(toWorld(p, true)) ? PhaseSchlick(w, wp, g) : 0.f;
	}
	Spectrum HomogeneousVolume::Tau(const Ray &ray, float step, float offset) const
	{
        float t0, t1;
        if (!IntersectP(ray, &t0, &t1)) return 0.f;
		return Distance(ray(t0), ray(t1)) * (sig_a + sig_s);
	}
}