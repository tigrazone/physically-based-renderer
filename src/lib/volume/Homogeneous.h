/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_HOMOGENEOUS_H
#define RENDERLIB_CORE_HOMOGENEOUS_H

#include "Volume.h"

namespace Render
{
	class HomogeneousVolume : public VolumeRegion
	{
		Spectrum sig_a, sig_s, le;
		float g;
	public:
		HomogeneousVolume(const Transform &toWorld, const BBox &bbox, const Spectrum &sig_a, const Spectrum &sig_s, const Spectrum &le, float g);
		Spectrum SigmaA(const Point &p, const Vector &w, float time) const;
		Spectrum SigmaS(const Point &p, const Vector &w, float time) const;
		Spectrum SigmaT(const Point &p, const Vector &w, float time) const;
		Spectrum Lve(const Point &p, const Vector &w, float time) const;
		float P(const Point &p, const Vector &w, const Vector &wp, float time) const;
		Spectrum Tau(const Ray &ray, float step, float offset) const;
	};
}

#endif