/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "GUI.h"

#define STARTROUTINE { stop = false; evb->Signal(Event::INIT); evb->Signal(Event::CREATE); }
#define ENDROUTINE { stop = true; evb->Signal(Event::CLOSE); }
#define CLEANUPWINDOW { evb->Signal(Event::DESTROY); delete evb; }

namespace Render
{
	char rootdir[4096];
	EventListener::~EventListener(){}
//---------------------------------------------------------------------------------------------------------

	GUIObj::GUIObj(GUIEventBinder *evb)
		:	evb(evb){}
	GUIObj::~GUIObj(){}

//---------------------------------------------------------------------------------------------------------

	GUIGLContext::GUIGLContext():ownsContext(false){ memset(this, 0, sizeof(GUIGLContext)); }

	WindowSignal::WindowSignal(const GUIWindow *window, Message_t ev):window(window), ev(ev){}

//---------------------------------------------------------------------------------------------------------

	GUIEventBinder::GUIEventBinder(unsigned int nCustomEvents)
	{
		if(nCustomEvents > 0)
			customEventPool.resize(nCustomEvents);
	}

	GUIEventBinder::~GUIEventBinder()
	{
		for(int ev = 0; ev < Event::N_EVENTS - 1; ++ev)
		{
			for(EventPool::iterator i = eventPool[ev].begin(); i != eventPool[ev].end(); ++i)
			{
				delete (*i);
				*i = 0;
			}
			eventPool[ev].clear();
		}

		for(vector<EventPool>::iterator cev = customEventPool.begin(); cev != customEventPool.end(); ++cev)
		{
			for(EventPool::iterator i = cev->begin(); i != cev->end(); ++i)
			{
				delete (*i);
				*i = 0;
			}
			cev->clear();
		}
	}

	void GUIEventBinder::ExpandCustomEventPool(size_t n)
	{
		customEventPool.resize(customEventPool.size() + n);
	}

	Response_t GUIEventBinder::Signal(unsigned ev, void *data) const
	{
		EventResponse r = EventResponse::OK;

		const EventPool *pool = GetPool(ev);
		if(!pool)
			return responseMap[r];

		for(EventPool::const_iterator i = pool->begin(); i != pool->end(); ++i)
			if(!(**i)(data, &r))
				break;

		return responseMap[r];
	}

//---------------------------------------------------------------------------------------------------------

#if defined(IS_WINDOWS)
#	define guiWndClass "GUIWindow:Main"

	const Response_t GUIEventBinder::responseMap[N_RESPONSES] =
	{
		E_FAIL,
		S_OK,
		HTLEFT,
		HTRIGHT,
		HTTOPLEFT,
		HTTOPRIGHT,
		HTBOTTOMLEFT,
		HTBOTTOMRIGHT,
		HTCAPTION,
		HTMINBUTTON,
		HTTOP,
		HTMAXBUTTON,
		HTBOTTOM,
		HTCLOSE,
		HTHSCROLL,
		HTVSCROLL,
	};

	WNDCLASSEX wcex = {};
	const HCURSOR hDefaultCursor = LoadCursor(NULL, IDC_ARROW);

	void Init()
	{
		if(wcex.cbSize != 0)
			return;

		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = GUIWindow::Proc;
		wcex.hInstance = HINSTANCE(GetModuleHandle(NULL));
		wcex.lpszClassName = guiWndClass;
		wcex.hbrBackground = NULL;

		if(!RegisterClassEx(&wcex))
			Severe("Failed to register window class '%s'", STRNAMESPACE, "Init", GetLastError());
	}

	LRESULT CALLBACK GUIWindow::Proc(HWND hwnd, UINT m, WPARAM w, LPARAM l)
	{
		GUIWindow *win = (GUIWindow *)GetWindowLongPtr(hwnd, GWLP_USERDATA);
		if(win == NULL)
			return DefWindowProc(hwnd, m, w, l);

		return win->proc(m, w, l);
	}

	LRESULT GUIWindow::proc(UINT m, WPARAM w, LPARAM l)
	{
		switch(m)
		{
			case WM_NCHITTEST:
			{
				ScreenPoint mp = { GET_X_LPARAM(l), GET_Y_LPARAM(l) };
				if(windowState == State::NORMAL)
				{
					mp.x -= posX;
					mp.y -= posY;
				}

				Event mouseEvent = Event::HITTEST;
				USHORT msb = 0x8000;

				bool down = (GetAsyncKeyState(VK_LBUTTON) & msb) > 0;
				if(down && !mouseDown[0])
				{
					mouseEvent = Event::LMOUSEDOWN;
					mouseDown[0] = true;
				}
				else if(!down && mouseDown[0])
				{
					mouseEvent = Event::LMOUSEUP;
					mouseDown[0] = false;
				}

				down = (GetAsyncKeyState(VK_MBUTTON) & msb) > 0;
				if(down && !mouseDown[1])
				{
					mouseEvent = Event::MMOUSEDOWN;
					mouseDown[1] = true;
				}
				else if(!down && mouseDown[1])
				{
					mouseEvent = Event::MMOUSEUP;
					mouseDown[1] = false;
				}

				down = (GetAsyncKeyState(VK_RBUTTON) & msb) > 0;
				if(down && !mouseDown[2])
				{
					mouseEvent = Event::RMOUSEDOWN;
					mouseDown[2] = true;
				}
				else if(!down && mouseDown[2])
				{
					mouseEvent = Event::RMOUSEUP;
					mouseDown[2] = false;
				}

				Response_t r = evb->Signal(mouseEvent, &mp);
				if(r == S_OK)
					r = HTCLIENT;
				return r;
			}
			case WM_MOVE:
			{
				if(IsIconic(hwnd))
					return S_OK;
				ScreenPoint p = { GET_X_LPARAM(l), GET_Y_LPARAM(l) };
				return evb->Signal(Event::MOVE, &p);
			}
			case WM_SYSCOMMAND:
				switch(w & 0xfff0)
				{
					case SC_MINIMIZE:
						return evb->Signal(Event::MINIMIZE);
					case SC_MAXIMIZE:
						return evb->Signal(Event::MAXIMIZE);
					case SC_RESTORE:
						//Some windows functions can bypass the SC_MINIMIZE message yet cause a minimize state.
						if(IsIconic(hwnd))
							windowState = static_cast<State>(windowState | State::MINIMIZED);
						return evb->Signal(Event::RESTORE);
					case SC_MOVE:
						if(windowState != State::NORMAL)
							return S_OK;
						break;
				}
				break;
			case WM_SIZE:
			{
				//There are cases where windows bypasses SC_* and just calls ShowWindow(hwnd, SW_*) directly (or some such shenanigans).
				//So intercept and use our customized maximize method, which will trigger another WM_SIZE message.
				if(w == SIZE_MINIMIZED && !(windowState & State::MINIMIZED))
					return evb->Signal(Event::MINIMIZE);
				if(w != SIZE_MINIMIZED && (windowState & State::MINIMIZED))
					return evb->Signal(Event::RESTORE);
				if(w == SIZE_MAXIMIZED && !(windowState & State::FULLSCREEN))
					return evb->Signal(Event::MAXIMIZE);

				SizeData data = { windowState, posX, posY, GET_X_LPARAM(l), GET_Y_LPARAM(l) };
				return evb->Signal(Event::SIZE, (void *) &data);
			}
			case WM_KEYUP:
				return S_OK;
			case WM_PAINT:
			{
				ValidateRect(hwnd, NULL);
				return evb->Signal(DRAW);
			}
			case WM_SETCURSOR:
				if(LOWORD(l) == HTCLIENT)
				{
					SetCursor(hDefaultCursor);
					return S_OK;
				}
				break;
			case WM_USER:
				return evb->Signal(unsigned int(w), (void *) l);
			case WM_CLOSE:
				ShowWindow(hwnd, SW_HIDE);

				ENDROUTINE
				return S_OK;
		}

		return DefWindowProc(hwnd, m, w, l);
	}

//---------------------------------------------------------------------------------------------------------

	GUIGLContext::GUIGLContext(HWND hwnd)
		:	hdc(GetDC(hwnd)), ownsContext(true)
	{
		PIXELFORMATDESCRIPTOR pfd = 
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
			PFD_TYPE_RGBA,
			32,
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			0,
			0,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
		};
		int iPixelFormat;
		if( !(iPixelFormat = ChoosePixelFormat(hdc, &pfd)) )
			Severe("An error occured selecting pixel format.", "GUIGLContext", "GUIGLContext", GetLastError());

		if( !SetPixelFormat(hdc, iPixelFormat, &pfd) )
			Severe("An error occured setting pixel format.", "GUIGLContext", "GUIGLContext", GetLastError());

		HGLRC dummy;
		if( !(dummy = wglCreateContext(hdc)) )
			Severe("An error occured creating dummy context.", "GUIGLContext", "GUIGLContext", GetLastError());

		wglMakeCurrent(hdc, dummy);
		GLenum result;
		if((result = glewInit()) != GLEW_OK)
			Severe("Could not initialize GLEW.", "GUIGLContext", "GUIGLContext", result);

		int attribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 1,
			WGL_CONTEXT_FLAGS_ARB, 0,
			0
		};

		if(WGLEW_ARB_create_context)
		{
			if( !(hglrc = wglCreateContextAttribsARB(hdc, 0, attribs)) )
				Severe("An error occured creating context from specified attributes.", "GUIGLContext", "GUIGLContext", GetLastError());
		}
		else
		{
			hglrc = dummy;
			Warning("Could not find any OpenGL extensions.  Defaulting to OpenGL version 1.1");
		}

		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(dummy);
	}

	GUIGLContext::GUIGLContext(HDC hdc, GUIGLContext &ctx)
		:	hdc(hdc), hglrc(ctx.hglrc), ownsContext(false){}

	GUIGLContext::~GUIGLContext()
	{
		if(ownsContext)
		{
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(hglrc);
		}
	}

	const GUIGLContext &GUIGLContext::Acquire() const
	{
		wglMakeCurrent(hdc, hglrc);
		return *this;
	}

	void GUIGLContext::Swap() const
	{
		SwapBuffers(hdc);
	}

//---------------------------------------------------------------------------------------------------------

	GUIWindow::GUIWindow(const string &title, State windowState, GUIWindow *parent)
		:	GUIObj(new GUIEventBinder()), parent(parent), title(title), windowState(windowState)
	{
		mouseDown[0] = mouseDown[1] = mouseDown[2] = false;
		Init();

		DWORD style = WS_POPUP | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_BORDER;

		if(parent == NULL)
		{
			if( !(hwnd = CreateWindowEx(0, guiWndClass, title.c_str(), style, 0, 0, 0, 0, 0, 0, wcex.hInstance, NULL)) )
				Severe("Failed to create window \"%s\".", "GUIWindow", "GUIWindow", GetLastError(), title.c_str());

			new (&ctx) GUIGLContext(hwnd);
		}
		else
		{
			style |= WS_CHILD;

			if( !(CreateWindowEx(0, guiWndClass, title.c_str(), style, 0, 0, 0, 0, parent->hwnd, 0, wcex.hInstance, NULL)) )
				Severe("Failed to create window \"%s\".", "GUIWindow", "GUIWindow", GetLastError(), title.c_str());

			new (&ctx) GUIGLContext(GetDC(hwnd), parent->ctx);
		}
	}

	GUIWindow::~GUIWindow()
	{
		CLEANUPWINDOW

		DestroyWindow(hwnd);
	}

	void GUIWindow::Minimize()
	{
		windowState = static_cast<State>(windowState | State::MINIMIZED);
		ShowWindow(hwnd, SW_MINIMIZE);
	}
	
	void GUIWindow::FullScreen()
	{
		SetWindowLongPtr(hwnd, GWL_STYLE, GetWindowStyle(hwnd) | WS_BORDER);
		windowState = static_cast<State>(windowState & ~State::MINIMIZED | State::FULLSCREEN);
		ShowWindow(hwnd, SW_SHOWMAXIMIZED);
	}
	
	void GUIWindow::Maximize()
	{
		SetWindowLongPtr(hwnd, GWL_STYLE, GetWindowStyle(hwnd) & ~WS_BORDER);
		int px, py, nx, ny;
		GetScreenSize(posX, posY, &nx, &ny, &px, &py, true);
		windowState = State::MAXIMIZED;
		ShowWindow(hwnd, SW_SHOWNORMAL);
		SetWindowPos(hwnd, 0, px, py, nx, ny, 0);
	}
	void GUIWindow::GetWindowSize(int *width, int *height) const
	{
		*width = currentPixelsX;
		*height = currentPixelsY;
	}
	void GUIWindow::GetCursorPos(ScreenPoint *p) const
	{
		::GetCursorPos((POINT *) p);
		if(windowState == State::NORMAL)
		{
			p->x -= posX;
			p->y -= posY;
		}
	}

	void GUIWindow::Normal()
	{
		SetWindowLongPtr(hwnd, GWL_STYLE, GetWindowStyle(hwnd) | WS_BORDER);
		
		RECT rect;
		rect.left = posX;
		rect.right = posX + pixelsX;
		rect.top = posY;
		rect.bottom = posY + pixelsY;

		AdjustWindowRect(&rect, GetWindowStyle(hwnd), false);
		windowState = State::NORMAL;

		ShowWindow(hwnd, SW_SHOWNORMAL);
		SetWindowPos(hwnd, 0, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 0);
	}

	bool GUIWindow::GetScreenSize(int xpos, int ypos, int *pixelsX, int *pixelsY, int *x, int *y, bool getWorkArea) const
	{
		POINT p = {xpos, ypos};
		HMONITOR hMonitor = MonitorFromPoint(p, MONITOR_DEFAULTTONEAREST);
		
		if(hMonitor == 0)
			return false;

		MONITORINFO info = {};
		info.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(hMonitor, &info);

		if(getWorkArea)
		{
			*pixelsX = info.rcWork.right - info.rcWork.left;
			*pixelsY = info.rcWork.bottom - info.rcWork.top;
			if(x) *x = info.rcWork.left;
			if(y) *y = info.rcWork.top;
		}
		else
		{
			*pixelsX = info.rcMonitor.right - info.rcMonitor.left;
			*pixelsY = info.rcMonitor.bottom - info.rcMonitor.top;
			if(x) *x = info.rcMonitor.left;
			if(y) *y = info.rcMonitor.top;
		}

		return true;
	}

	bool GUIWindow::GetFile(const char *extensions, string &file, const char *title) const
	{
		OPENFILENAME ofn = {};
		char buf[4096] = {};
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hwnd;
		ofn.lpstrFilter = extensions;
		ofn.Flags = OFN_DONTADDTORECENT | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NONETWORKBUTTON;
		ofn.lpstrFile = buf;
		ofn.nMaxFile = 4096;

		BOOL result = GetOpenFileName(&ofn);

		if(result != 0)
			file = ofn.lpstrFile;

		return result != 0;
	}

	bool GUIWindow::SaveFile(const char *extensions, string &file, const char *title) const
	{
		OPENFILENAME ofn = {};
		char buf[4096] = {};
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hwnd;
		ofn.lpstrFilter = extensions;
		ofn.Flags = OFN_DONTADDTORECENT | OFN_PATHMUSTEXIST | OFN_NONETWORKBUTTON | OFN_OVERWRITEPROMPT;
		ofn.lpstrFile = buf;
		ofn.nMaxFile = 4096;

		BOOL result = GetSaveFileName(&ofn);

		if(result != 0)
			file = ofn.lpstrFile;

		switch(ofn.nFilterIndex)
		{
		case 1:
			if(!EndsWithIgnoreCase(file, ".exr"))
				file += ".exr";
			break;
		case 2:
			if(!EndsWithIgnoreCase(file, ".png"))
				file += ".png";
		}

		return result != 0;
	}

	void GUIWindow::Run()
	{
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);

		SetWindowLongPtr(hwnd, GWLP_USERDATA, LONG(this));

		STARTROUTINE
		MSG msg = {};
		while(GetMessage(&msg, hwnd, 0, 0) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if(stop)
				break;
		}
	}

//---------------------------------------------------------------------------------------------------------

	Message_t WindowSignal::eventMap[Event::N_EVENTS] =
	{
		WM_NULL,
		WM_CREATE,
		WM_CLOSE,
		WM_DESTROY,
		WM_NCHITTEST,
		WM_SIZE,
		WM_SYSCOMMAND,
		WM_SYSCOMMAND,
		WM_SYSCOMMAND,
		WM_NCHITTEST,
		WM_NCHITTEST,
		WM_NCHITTEST,
		WM_NCHITTEST,
		WM_NCHITTEST,
		WM_NCHITTEST,
		WM_VSCROLL,
		WM_KEYDOWN,
		WM_KEYUP,
		WM_PAINT,
		WM_MOVE,
		WM_USER
	};

	void WindowSignal::Signal() const
	{
		PostMessage(window->hwnd, ev, params.wp, params.lp);
	}

//---------------------------------------------------------------------------------------------------------

	GenericEventSignal::GenericEventSignal(const GUIWindow *window, Event ev):WindowSignal(window, eventMap[ev])
	{
		switch(ev)
		{
			case Event::MAXIMIZE:
				params.wp = SC_MAXIMIZE;
				break;
			case Event::MINIMIZE:
				params.wp = SC_MINIMIZE;
				break;
			case Event::RESTORE:
				params.wp = SC_RESTORE;
				break;
			default:
				params.wp = 0;
		}

		params.lp = 0;
	}

//---------------------------------------------------------------------------------------------------------

	void UserEventSignal::SetParams(unsigned int ev, void *obj)
	{
		params.lp = LPARAM(obj);
		params.wp = WPARAM(ev);
	}

//---------------------------------------------------------------------------------------------------------
#endif

	UserEventSignal::UserEventSignal(const GUIWindow *window)
		:	WindowSignal(window, eventMap[Event::USER])
	{
	}

	bool GUIWindow::GUIWindowCreate()
	{	
		if(windowState & State::FULLSCREEN)
			FullScreen();
		else if(windowState & State::MAXIMIZED)
			Maximize();
		else
			Normal();

		return true;
	}

	bool GUIWindow::GUIWindowRestore()
	{
		if(windowState & State::MINIMIZED)
		{
			if(windowState & State::FULLSCREEN)
			{
				ShowWindow(hwnd, SW_SHOWMAXIMIZED);
				windowState = static_cast<State>(windowState & ~State::MINIMIZED);
			}
			else if(windowState & State::MAXIMIZED)
				Maximize();
			else
				Normal();
		}
		else if(windowState & State::FULLSCREEN)
		{
			if(windowState & State::MAXIMIZED)
				Maximize();
			else
				Normal();
		}
		else
			Normal();

		return true;
	}

	bool GUIWindow::GUIWindowMaximize()
	{
		if(windowState & State::MAXIMIZED)
			Normal();
		else
			Maximize();
		return true;
	}

	bool GUIWindow::GUIWindowMinimize()
	{
		Minimize();
		return true;
	}

	bool GUIWindow::GUIWindowHitTest(const ScreenPoint *data, EventResponse *response)
	{
		*response = HITMOVE;
		if(windowState == NORMAL)
		{
			if(data->x >= -BORDERPADDING && data->x < BORDERPADDING)
				*response = HITLEFT;
			else if(data->x >= pixelsX - BORDERPADDING && data->x < pixelsX + BORDERPADDING)
				*response = HITRIGHT;

			if(data->y >= -BORDERPADDING && data->y < BORDERPADDING)
				*response = EventResponse(*response + 2);
			else if(data->y >= pixelsY - BORDERPADDING && data->y < pixelsY + BORDERPADDING)
				*response = EventResponse(*response + 4);

			if(*response != HITMOVE)
				return false;
		}

		return true;
	}

	bool GUIWindow::GUIWindowSize(const SizeData *data)
	{
		if(windowState == NORMAL)
		{
			pixelsX = data->nx;
			pixelsY = data->ny;
		}
		currentPixelsX = data->nx;
		currentPixelsY = data->ny;
		return true;
	}

	bool GUIWindow::GUIWindowMove(const ScreenPoint *data)
	{
		if(windowState == NORMAL)
		{
			posX = data->x;
			posY = data->y;
		}
		return true;
	}
}