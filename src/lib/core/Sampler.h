/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_SAMPLER_H
#define RENDERLIB_CORE_SAMPLER_H

#include "Memory.h"
#include "MonteCarlo.h"

namespace Render
{
	inline float VanDerCorput(uint32_t n, uint32_t scramble)
	{
		n = (n << 16 | n >> 16);
		n = ((n & 0x00ff00ff) << 8) | ((n & 0xff00ff00) >> 8);
		n = ((n & 0x0f0f0f0f) << 4) | ((n & 0xf0f0f0f0) >> 4);
		n = ((n & 0x33333333) << 2) | ((n & 0xcccccccc) >> 2);
		n = ((n & 0x55555555) << 1) | ((n & 0xaaaaaaaa) >> 1);
		n ^= scramble;
		return ((n >> 8) & 0xffffffff) / float(1 << 24);
	}
	inline float Sobol2(uint32_t n, uint32_t scramble)
	{
		for(uint32_t v = 1 << 31; n != 0; n >>=1, v ^= v >> 1)
			if(n & 0x1) scramble ^= v;

		return ((scramble>>8) & 0xffffffff) / float(1 << 24);
	}
	inline double PermutedRadicalInverseBase2(unsigned int n, unsigned int *p)
	{
		double invBi = 0.5;
		double val = 0.0;
		while(n > 0)
		{
			val += double(p[n & 0x1]) * invBi;
			invBi *= 0.5;
			n >>= 1;
		}
		return val;
	}
	inline double PermutedRadicalInverse(unsigned int n, unsigned int base, unsigned int *p)
	{
		if(base == 2)
			return PermutedRadicalInverseBase2(n, p);
		double invBase = 1.0 / double(base);
		double invBi = invBase;
		double val = 0.0;
		while(n > 0)
		{
			val += double(p[n % base]) * invBi;
			invBi *= base;
			n = unsigned int(double(n) * invBase);
		}
		return val;
	}
	inline double RadicalInverseBase2(unsigned int n)
	{
		double invBi = 0.5;
		double val = 0.0;
		while(n > 0)
		{
			if(n & 0x1)
				val += invBi;

			invBi *= 0.5;
			n >>= 1;
		}
		return val;
	}
	inline double RadicalInverse(unsigned int n, unsigned int base)
	{
		if(base == 2)
			return RadicalInverseBase2(n);
		double invBase = 1.0 / double(base);
		double invBi = invBase;
		double val = 0.0;
		while(n > 0)
		{
			val += double(n % base) * invBi;
			invBi *= invBase;
			n = unsigned int(double(n) * invBase);
		}
		return val;
	}

	class PermutedHalton
	{
		uint32_t d;
		uint32_t *permutations;
	public:
		PermutedHalton(uint32_t d, RNG &rng);
		~PermutedHalton();
		void Sample(uint32_t n, float *out) const;
	};

	struct CameraSample
	{
		float x, y, u, v;
	};

	struct Sample : public CameraSample
	{
		Sampler *sampler;
		void *samplerData;
		RNG rng;
	};

	class Sampler
	{
	protected:
		RNG seeder;
		unsigned int taskData;
		unsigned int nTasks;
		//virtual void *allocSamples(float *mem);
		//virtual void *allocPixelSamples(float *mem);
	public:
		struct SampleRegionData
		{
			unsigned int xPos, yPos, data[4];
		};
		const unsigned int width, height, nPixels;
		Sampler(unsigned int width, unsigned int height);
		virtual ~Sampler();
		virtual uint32_t GetSeed() const;
		virtual void RequestSamples(const unsigned int *d, const unsigned int *dDecorrelated, unsigned int *offsets, unsigned int *offsetsDec, size_t n);
		//Normally GetNextSample fills in samplerData in samples.  However, sometimes a sample is needed before that occurs.
		//SamplerData is largely based on pixel position, so this can be used to override default behavior if needed.
		virtual float GetSample(Sample *sample, uint32_t offset, unsigned int x, unsigned int y) const = 0;
		virtual float GetSample(Sample *sample, uint32_t offset) const = 0;
		virtual void GetSamples(Sample *sample, uint32_t offset, float *u, uint32_t n) const;
		virtual Sample *CreateSamples(unsigned int nThreads);
		virtual void FreeSamples(Sample *samples, unsigned int nThreads);
		virtual unsigned int SetNumTasks(unsigned int nTasks);
		virtual SampleRegionData GetSampleRegionData(unsigned int taskNum) const;
		//Returns false when reaching the end of a single pass for a region, true otherwise.
		virtual bool GetNextSample(Sample *sample, SampleRegionData *region) const = 0;
		virtual void IncrementPass(void *samplerData) const;
	};
}

#endif