/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "RNG.h"

#define M 397
#define MATRIX_A 0x9908b0dfUL // Constant vector a.
#define UPPER_MASK 0x80000000UL // Most significant w-r bits.
#define LOWER_MASK 0x7fffffffUL // Least significant r bits.

namespace Render
{
	RNG::RNG() : mti(N + 1) //Uninitialized
	{
	}

	void RNG::Seed(uint32_t seed = 5489UL) const
	{
		mt[0] = seed & 0xffffffffUL;
		for(mti = 1; mti < N; ++mti)
		{
			mt[mti] = (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti);
			/* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier.
			 * In the previous versions, MSBs of the seed affect
			 * only MSBs of the array mt[].
			 * 2002/01/09 modified by Makoto Matsumoto.
			 */
			mt[mti] &= 0xffffffffUL;
		}
	}

	float RNG::RandF() const
	{
		float r = (Rand() & 0xffffff) / float(0x1000000);
		return r;
	}

	uint32_t RNG::Rand() const
	{
		static uint32_t mag01[2] = {0x0UL, MATRIX_A};
		uint32_t y;
		if(mti >= N) // Generate N words at one time.
		{
			if(mti == N + 1)
				Seed();

			
			int kk;
			for(kk = 0; kk < N - M; ++kk)
			{
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1UL];
			}
			for(; kk < N - 1; ++kk)
			{
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
			}
			
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1UL];

			mti = 0;
		}
	
		y = mt[mti++];

		//Tempering.
		y ^= (y >> 11);
		y ^= (y << 7) & 0x9d2c5680UL;
		y ^= (y << 15) & 0xefc60000UL;
		y ^= (y >> 18);
		return y;
	}
}