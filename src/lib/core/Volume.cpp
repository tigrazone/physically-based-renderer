/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Volume.h"

namespace Render
{
	float PhaseRayleigh(const Vector &w, const Vector &wp)
	{
		float costheta = Dot(w, wp);
		return  0.75f * INV_FOURPI * (1.f + costheta * costheta);
	}
	float PhaseMieHazy(const Vector &w, const Vector &wp)
	{
		float costheta = Dot(w, wp);
		return (0.5f + 4.5f * pow(0.5f * (1.f + costheta), 8.f)) * INV_FOURPI;
	}
	float PhaseMieMurky(const Vector &w, const Vector &wp)
	{
		float costheta = Dot(w, wp);
		return (0.5f + 16.5f * pow(0.5f * (1.f + costheta), 32.f)) * INV_FOURPI;
	}
	float PhaseHG(const Vector &w, const Vector &wp, float g)
	{
		float costheta = Dot(w, wp);
		return INV_FOURPI * (1.f - g * g) / pow(1.f + g * g - 2.f * g * costheta, 1.5f);
	}
	float PhaseSchlick(const Vector &w, const Vector &wp, float g)
	{
		float k = 1.45352f * g - 0.45352f * g * g * g;
		float d = 1.f - k * Dot(w, wp);
		return INV_FOURPI * (1.f - k * k) / (d * d);
	}
	
	VolumeRegion::VolumeRegion(){}
	VolumeRegion::VolumeRegion(const Transform &toWorld, const BBox &bounds)
		:	toWorld(toWorld), bounds(bounds){}
	VolumeRegion::~VolumeRegion(){}
	Spectrum VolumeRegion::SigmaT(const Point &p, const Vector &w, float time) const
	{
		return SigmaS(p, w, time) + SigmaT(p, w, time);
	}

	bool VolumeRegion::IntersectP(const Ray &ray, float *t0, float *t1) const
	{
		return bounds.IntersectP(toWorld(ray, true), t0, t1);
	}

	BBox VolumeRegion::WorldBound() const
	{
		return toWorld(bounds);
	}

	VolumeAggregate::VolumeAggregate(const vector<VolumeRegion *> &volumes)
		:	volumes(volumes)
	{
		for(size_t i = 0; i < volumes.size(); ++i)
			bounds = Union(bounds, volumes[i]->WorldBound());
	}
	VolumeAggregate::~VolumeAggregate()
	{
		for(size_t i = 0; i < volumes.size(); ++i)
			delete volumes[i];
	}
	Spectrum VolumeAggregate::SigmaA(const Point &p, const Vector &w, float time) const
	{
		Spectrum sig_a;
		for(size_t i = 0; i < volumes.size(); ++i)
			sig_a += volumes[i]->SigmaA(p, w, time);

		return sig_a;
	}
	Spectrum VolumeAggregate::SigmaS(const Point &p, const Vector &w, float time) const
	{
		Spectrum sig_s;
		for(size_t i = 0; i < volumes.size(); ++i)
			sig_s += volumes[i]->SigmaS(p, w, time);

		return sig_s;
	}
	Spectrum VolumeAggregate::SigmaT(const Point &p, const Vector &w, float time) const
	{
		Spectrum sig_t;
		for(size_t i = 0; i < volumes.size(); ++i)
			sig_t += volumes[i]->SigmaT(p, w, time);

		return sig_t;
	}
	Spectrum VolumeAggregate::Lve(const Point &p, const Vector &w, float time) const
	{
		Spectrum le;
		for(size_t i = 0; i < volumes.size(); ++i)
			le += volumes[i]->Lve(p, w, time);

		return le;
	}
	float VolumeAggregate::P(const Point &p, const Vector &w, const Vector &wp, float time) const
	{
		float ph = 0, weight = 0;
		for(size_t i = 0; i < volumes.size(); ++i)
		{
			float wt = volumes[i]->SigmaS(p, w, time).y(); 
			weight += wt;
			ph += wt * volumes[i]->P(p, w, wp, time);
		}
		return ph / weight;
	}
	Spectrum VolumeAggregate::Tau(const Ray &ray, float step, float offset) const
	{
		Spectrum tau;
		for(size_t i = 0; i < volumes.size(); ++i)
			tau += volumes[i]->Tau(ray, step, offset);

		return tau;
	}
}