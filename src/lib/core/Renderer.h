/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_RENDERER_H
#define RENDERLIB_CORE_RENDERER_H

#include "RenderStatus.h"
#include "Integrator.h"
#include "Scene.h"
#include "Sampler.h"
#include "Parallel.h"
#include "Camera.h"
#include "Film.h"
#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace Render
{
	class Renderer
	{
	protected:
		int nThreads;
		Sample *samples;
		MemoryArena *arenas;
	public:
		Sampler *sampler;
		Camera *camera;
		SurfaceIntegrator *surf;
		VolumeIntegrator *vol;
		Scene *scene;
		Renderer(Scene *scene, Camera *camera, Sampler *sampler, SurfaceIntegrator *surf, VolumeIntegrator *vol);
		virtual ~Renderer();
		virtual void Render(RenderStatus *status) = 0;
	};
}

#endif