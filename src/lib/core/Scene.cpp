/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Scene.h"

namespace Render
{
	Scene::Scene(const Reference<Primitive> &aggregate, VolumeRegion *volume, const Reference<LightCollection> &lightRoot)
		:	aggregate(aggregate), volume(volume), lightRoot(lightRoot), environmentLights(NULL)
	{
		lights = lightRoot->GetLightInstances();
		for(unsigned int i = 0; i < lightRoot->Size(); ++i)
			if(lights[i].light->IsEnvironment())
				environmentLights.push_back(&lights[i]);

		Bounds().BoundingSphere(&c, &r);
	}
	Scene::~Scene()
	{
		FreeAligned(lights);
		delete volume;
	}

	bool Scene::Intersect(const Ray &ray, Intersection *isect) const
	{
		return aggregate ? aggregate->Intersect(ray, isect) : false;
	}
	bool Scene::IntersectP(const Ray &ray) const
	{
		return aggregate ? aggregate->IntersectP(ray) : false;
	}
	BBox Scene::Bounds() const
	{
		return aggregate ? aggregate->Bounds() : BBox();
	}
	bool Scene::Le(const Ray &ray, Spectrum &s) const
	{
		if(environmentLights.size() == 0)
			return false;

		for(auto i = environmentLights.begin(); i != environmentLights.end(); ++i)
			(*i)->light->Le(&(*i)->toWorld, ray, s);

		return true;
	}
	const float &Scene::Radius() const
	{
		return r;
	}
	const Point &Scene::Center() const
	{
		return c;
	}
}