/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_BXDF_H
#define RENDERLIB_CORE_BXDF_H

#include "MonteCarlo.h"
#include "Spectrum.h"
#include "DifferentialGeometry.h"
#include "Sampler.h"

#define SameHemisphere(a, b) (((a).z > 0 && (b).z > 0) || ((a).z < 0 && (b).z < 0))

namespace Render
{
	enum BxDFType
	{
		BxDFNULL		= 0,
		REFLECTION		= 1,
		TRANSMISSION	= REFLECTION << 1,
		DIFFUSE			= TRANSMISSION << 1,
		GLOSSY			= DIFFUSE << 1,
		SPECULAR		= GLOSSY << 1,
		ALL_GLOSSY		= REFLECTION | TRANSMISSION | GLOSSY,
		ALL_SPECULAR	= REFLECTION | TRANSMISSION | SPECULAR,
		NON_SPECULAR	= REFLECTION | TRANSMISSION | DIFFUSE | GLOSSY,
		ALL_TYPES		= DIFFUSE | GLOSSY | SPECULAR,
		ALL_REFLECTION	= REFLECTION | ALL_TYPES,
		ALL_TRANSMISSION= TRANSMISSION | ALL_TYPES,
		ALL				= ALL_TRANSMISSION | ALL_REFLECTION
	};

//---------------------------------------------------------------------------------------------------------

	float FrDiel(float cosi, float cost, float etai, float etat);
	Spectrum FrDiel(float cosi, float cost, const Spectrum &etai, const Spectrum &etat);
	Spectrum FrCond(float cosi, const Spectrum &eta, const Spectrum &k);
	float FrSchlick(float cosh, float r0);
	Spectrum FrSchlick(float cosh, const Spectrum &r0);

	class Fresnel
	{
	public:
		virtual ~Fresnel();
		virtual Spectrum Evaluate(float cosi) const = 0;
	};

	class FresnelNoOp : public Fresnel
	{
	public:
		Spectrum Evaluate(float cosi) const
		{
			return 1.f;
		}
	};

	class FresnelDielectric : public Fresnel
	{
		float etai;
		float etat;
	public:
		FresnelDielectric(float etai, float etat);
		Spectrum Evaluate(float cosi) const;
	};

	class SchlickDielectric : public Fresnel
	{
		Spectrum r0;
	public:
		SchlickDielectric(Spectrum r0);
		Spectrum Evaluate(float cosi) const;
	};

	class FresnelConductor : public Fresnel
	{
		Spectrum eta;
		Spectrum k;
	public:
		FresnelConductor(const Spectrum &eta, const Spectrum &k);
		Spectrum Evaluate(float cosi) const;
	};

//---------------------------------------------------------------------------------------------------------

	class BxDF
	{
	public:
		const BxDFType type;
		BxDF(BxDFType type);
		virtual ~BxDF();
		bool BxDF::MatchesFlags(BxDFType flags) const;
		virtual void f(const Vector &wo, const Vector &wi, Spectrum &s) const = 0;
		virtual bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		virtual float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------

	class SpecularReflection : public BxDF
	{
		Spectrum r;
		Fresnel *fr;
	public:
		SpecularReflection(const Spectrum &r, Fresnel *fr);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

	class SpecularTransmission : public BxDF
	{
		Spectrum t;
		float etai, etat;
	public:
		SpecularTransmission(const Spectrum &t, float etai, float etat);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------

	class Lambertian : public BxDF
	{
		Spectrum r;
	public:
		Lambertian(const Spectrum &r);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
	};

//---------------------------------------------------------------------------------------------------------

	class OrenNayar : public BxDF
	{
		Spectrum r;
		float A, B;
	public:
		OrenNayar::OrenNayar(const Spectrum &r, float sigma);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
	};

//---------------------------------------------------------------------------------------------------------

	class SchlickBRDF : public BxDF
	{
		Spectrum rd, rs, alpha;
		float sigma;
		float anisotropy;
		float depth;
		bool multiBounce;
		float G(float cos) const;
		float A(float cosphi2) const;
		float Z(float costheta2) const;
		float AZ(const Vector &wh) const;
		float Phi(float u, float p) const;
	public:
		SchlickBRDF(const Spectrum &rd, const Spectrum &rs, float sigma, float anisotropy, const Spectrum &alpha, float depth, bool multiBounce);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------

	class MicrofacetDistribution
	{
	public:
		virtual ~MicrofacetDistribution();
		virtual float Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const = 0;
		virtual float D(const Vector &wh) const = 0;
		virtual float Pdf(const Vector &wo, const Vector &wi) const = 0;
		virtual float G(const Vector &wo, const Vector &wi, const Vector &wh) const;
	};

//---------------------------------------------------------------------------------------------------------

	class Microfacet : public BxDF
	{
		Spectrum r;
		MicrofacetDistribution *md;
		Fresnel *fr;
	public:
		Microfacet(const Spectrum &r, Fresnel *fr, MicrofacetDistribution *md);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------

	class Blinn : public MicrofacetDistribution
	{
		float e;
	public:
		Blinn(float e);
		float Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const;
		float D(const Vector &wh) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------
	
	class Anisotropic : public MicrofacetDistribution
	{
		float x, y, sd, sp;
		void sampleFirstQuadrant(float u[2], float *phi, float *cosh) const;
	public:
		Anisotropic(float x, float y);
		float Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const;
		float D(const Vector &wh) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------

	class SchlickDistribution : public MicrofacetDistribution
	{
		float sigma, anisotropy;
		float G(float cos) const;
		float A(float cosphi2) const;
		float Z(float costheta2) const;
		float AZ(const Vector &wh) const;
		float Phi(float u, float p) const;
	public:
		SchlickDistribution(float sigma, float anisotropy);
		float G(const Vector &wo, const Vector &wi, const Vector &wh) const;
		float Sample_w(const Vector &wo, Vector *wi, Vector *wh, float u[2], float *pdf) const;
		float D(const Vector &wh) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------
	class FresnelBlend : public BxDF
	{
		Spectrum rd;
		Spectrum rs;
		Spectrum alpha;
		float depth;
		MicrofacetDistribution *md;
	public:
		FresnelBlend(const Spectrum &rd, const Spectrum &rs, const Spectrum &alpha, float depth, MicrofacetDistribution *md);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};

//---------------------------------------------------------------------------------------------------------
	/*
	class RegularHalfAngle : public BxDF
	{
		const float *brdf;
		const uint32_t nThetaH, nThetaD, nPhiD;
	public:
		RegularHalfAngle(const float *brdf, uint32_t nThetaH, uint32_t nThetaD, uint32_t nPhiD);
		void f(const Vector &wo, const Vector &wi, Spectrum &s) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[2], float *pdf, Spectrum &s) const;
		float Pdf(const Vector &wo, const Vector &wi) const;
	};*/

//---------------------------------------------------------------------------------------------------------

	class BSDF
	{
	public:
		const bool specularOnly;
		BSDF(bool specularOnly);
		virtual ~BSDF();
		virtual unsigned int NumComponents() const = 0;
		virtual unsigned int NumComponents(BxDFType flags) const = 0;
		virtual bool Sample_f(const Vector &wo, Vector *wi, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType = NULL) const = 0;
		virtual Spectrum f(const Vector &wo, const Vector &wi, BxDFType flags) const = 0;
		virtual float Pdf(const Vector &wo, const Vector &wi, BxDFType flags) const = 0;
		virtual float PdfWtoA(const Vector &w, float dist2) const = 0;
	};

	class SingleBxDF : public BSDF
	{
		const BxDF *bxdf;
	public:
		const Normal ns, ng;
		const Vector ss, ts;
		SingleBxDF(BxDF *bxdf, const DifferentialGeometry &dg, const Normal &ng, bool specularOnly = false);
		Vector ToWorld(const Vector &w) const;
		Vector ToLocal(const Vector &w) const;
		unsigned int NumComponents() const;
		unsigned int NumComponents(BxDFType flags) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType = NULL) const;
		Spectrum f(const Vector &wo, const Vector &wi, BxDFType flags) const;
		float Pdf(const Vector &wo, const Vector &wi, BxDFType flags) const;
		float PdfWtoA(const Vector &w, float dist2) const;
	};

	class MultiBxDF : public BSDF
	{
		const unsigned int nBxDFs;
		BxDF **bxdfs;
	public:
		const Normal ns, ng;
		const Vector ss, ts;
		MultiBxDF(BxDF **bxdfs, unsigned int nBxDFs, const DifferentialGeometry &dg, const Normal &ng, bool specularOnly = false);
		Vector ToWorld(const Vector &w) const;
		Vector ToLocal(const Vector &w) const;
		unsigned int NumComponents() const;
		unsigned int NumComponents(BxDFType flags) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType = NULL) const;
		Spectrum f(const Vector &wo, const Vector &wi, BxDFType flags) const;
		float Pdf(const Vector &wo, const Vector &wi, BxDFType flags) const;
		float PdfWtoA(const Vector &w, float dist2) const;
	};

	class MixedBSDF : public BSDF
	{
		const unsigned int nBSDFs;
		BSDF **bsdfs;
		float *weights;
		float totalWeight;
		float invTotalWeight;
	public:
		MixedBSDF(BSDF **bsdfs, unsigned int nBSDFs, float *weights, float totalWeight, bool specularOnly = false);
		unsigned int NumComponents() const;
		unsigned int NumComponents(BxDFType flags) const;
		bool Sample_f(const Vector &wo, Vector *wi, float u[3], float *pdf, BxDFType flags, Spectrum &s, BxDFType *sampledType = NULL) const;
		Spectrum f(const Vector &wo, const Vector &wi, BxDFType flags) const;
		float Pdf(const Vector &wo, const Vector &wi, BxDFType flags) const;
		float PdfWtoA(const Vector &w, float dist2) const;
	};
}

#endif