/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_OPTIONS_H
#define RENDERLIB_CORE_OPTIONS_H

#include <string>

using namespace std;

namespace Render
{
	struct Options
	{
		uint32_t windowState;
		int posX, posY, pixelsX, pixelsY;
		unsigned int nThreads;
		unsigned int eob;
		//Past this point, dynamic data (ie pointers to data) must be loaded more intelligently
	};
}
#endif
