/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_RNG_H
#define RENDERLIB_CORE_RNG_H

#include "Global.h"

namespace Render
{
	class RNG
	{
		static const int N = 624;
		mutable uint32_t mt[N]; //The array for the state vector.
		mutable int mti;
	public:
		RNG();
		void Seed(uint32_t seed) const;
		uint32_t Rand() const;
		float RandF() const;
	};
}

#endif