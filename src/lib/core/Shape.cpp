/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Shape.h"

namespace Render
{
	Shape::~Shape(){}

	bool Shape::Intersect(const Ray &ray, Intersection *isect) const
	{
		Severe("Unimplemented function called.", "Shape", "Intersect", 1);
		return false;
	}

	bool Shape::IntersectP(const Ray &ray) const
	{
		Severe("Unimplemented function called.", "Shape", "IntersectP", 1);
		return false;
	}

	Point Shape::Sample(float u0, float u1, Intersection *isect) const
	{
		Severe("Unimplemented function called.", "Shape", "Sample", 1);
		return Point();
	}
	float Shape::Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect) const
	{
		Severe("Unimplemented function called.", "Shape", "Pdf", 1);
		return 0.f;
	}
	float Shape::Pdf(const Point &p, const Vector &wi, const Point &pShape, const Normal &n) const
	{
		float pdf = Distance2(p, pShape) / AbsDot(-wi, n);
		if(isinf(pdf)) pdf = 0.f;
		return pdf;
	}

	float Shape::Pdf(const Point &p) const
	{
		return 1.f / Area();
	}

	void Shape::GetDifferentialGeometry(const Ray &ray, Intersection *isect, const Transform *toWorld) const
	{
		Severe("Unimplemented function called.", "Shape", "GetDifferentialGeometry", 1);
	}
	/*
	void Shape::GetDifferentialGeometry(const RayDifferential &ray, Intersection *isect, DifferentialGeometry *dgShading, const Transform *toWorld) const
	{
		Severe("Unimplemented function called.", "Shape", "GetDifferentialGeometry", 1);
	}*/

	bool Shape::CanIntersect() const
	{
		return true;
	}

	float Shape::Area() const
	{
		Severe("Unimplemented function called.", "Shape", "Area", 1);
		return 1.f;
	}

	void Shape::Refine(vector<const Shape *> &refined) const
	{
		Severe("Unimplemented function called.", "Shape", "Refine", 1);
	}
}