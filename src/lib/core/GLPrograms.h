/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_GLPROGRAMLINKS_H
#define RENDERLIB_CORE_GLPROGRAMLINKS_H

#include "GUI.h"
#include <unordered_map>

#define GLSL(version, glsl) "#version " #version "\n" #glsl "\0"

namespace Render
{
	enum GLShader
	{
		NULLSHADER = NULL,
		FRAGMENT = GL_FRAGMENT_SHADER,
		GEOMETRY = GL_GEOMETRY_SHADER,
		VERTEX = GL_VERTEX_SHADER
	};

	struct GLShaderSource
	{
		GLShaderSource(const char *src, bool isFile, GLShader type);
		const char *operator()();
		const GLShader type;
	private:
		bool isFile;
		string glsl;
	};

//---------------------------------------------------------------------------------------------------------

	/* Map of shaders to promote sharing shader objects.
	 */
	class GLShaderObjectMap
	{
		typedef unordered_map<const GLShaderSource *, GLuint> ShaderMap;
		ShaderMap shaders;
		GLuint Compile(GLShaderSource *key) const;
	public:
		~GLShaderObjectMap();
		GLuint Find(GLShaderSource *key);
	};

	struct GLProgramGlobals
	{
		const GLuint handle;
		inline GLProgramGlobals(GLuint handle) : handle(handle) {}
	};

//---------------------------------------------------------------------------------------------------------

	/* Map of programs to promote sharing of programs.  This and GLShaderObjectMap are passed to GLPrograms
	 * to resolve its handle.  Thus shader sources and program links should generally be declared in a way
	 * to promote their reuse w.r.t. their pointers.
	 */
	class GLProgramLinkMap
	{
		typedef unordered_map<const GLShaderSource *const *, const GLProgramGlobals> ProgramMap;
		ProgramMap programs;
		GLuint Link(GLShaderObjectMap *shaderMap, GLShaderSource *const *sources, int n);
	public:
		~GLProgramLinkMap();

		template <typename GLProgram>
		const GLProgramGlobals *Find(GLShaderObjectMap *shaderMap)
		{
			GLShaderSource *const *sources; int n;
			GLProgram::GetSources(&sources, &n);

			ProgramMap::const_iterator i = programs.find(sources);
			if(i == programs.end())
			{
				programs.emplace( sources, GLProgram::GetProgramGlobals(Link(shaderMap, sources, n)) );
				i = programs.find(sources);
			}

			return &(i->second);
		}
		void DeletePrograms();
	};

//---------------------------------------------------------------------------------------------------------

	struct GLLinkMap
	{
		GLShaderObjectMap *shaderMap;
		GLProgramLinkMap *programMap;

		template <typename GLProgram, typename GlobalType>
		const GlobalType *Link()
		{
			return static_cast<const GlobalType *>(programMap->Find<GLProgram>(shaderMap));
		}
	};
}
#endif