/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_DIFFERENTIALGEOMETRY_H
#define RENDERLIB_CORE_DIFFERENTIALGEOMETRY_H

#include "Algebra.h"

namespace Render
{
	struct DifferentialGeometry
	{
		Point p;
		Normal n;
		Vector dpdu, dpdv;
		float u, v;
		//Commenting these out until they have a very clear use.
		//Normal dndu, dndv;
		/* These are somewhat useless in a global illumination/progressive rendering context.  They mostly help with texture anisotropy which is automatically handled by enough pixel samples.  90% sure about this.
		float dudx, dudy, dvdx, dvdy;
		mutable Vector dpdx, dpdy;
		*/
		DifferentialGeometry();
		DifferentialGeometry(const Point &p, const Vector &dpdu, const Vector &dpdv, float u, float v);
		DifferentialGeometry(const Point &p, const Vector &dpdu, const Vector &dpdv, const Normal &n, float u, float v);
		//void ComputeDifferentials(const RayDifferential &ray);
	};
}

#endif