/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "API.h"

#define PTR_DELETE(x) {delete (x); (x) = NULL;}
namespace Render
{
	Options opts;

	AnimatedNode::~AnimatedNode()
	{
		for(TimedTransforms::const_iterator i = this->transforms.begin(); i != this->transforms.end(); ++i)
			delete i->second;
	}
	void AnimatedNode::Fill(vector<const Transform *> *transforms, vector<float> *times) const
	{
		for(TimedTransforms::const_iterator i = this->transforms.begin(); i != this->transforms.end(); ++i)
		{
			transforms->push_back(i->second);
			times->push_back(i->first);
		}
	}
	bool AnimatedNode::AddTransform(const Transform *transform, float time)
	{
		TimedTransforms::iterator i = transforms.find(time);
		if(i == transforms.end())
		{
			transforms.emplace(time, new Transform(*transform));
			return true;
		}
		
		*i->second = *transform;
		return false;
	}
	bool AnimatedNode::RemoveTransform(float time)
	{
		TimedTransforms::iterator i = transforms.find(time);
		if(i == transforms.end())
			return false;

		transforms.erase(i);
		return true;
	}
	void AnimatedNode::Swap(AnimatedNode *other)
	{
		transforms.swap(other->transforms);
	}
	
	SceneNode::~SceneNode(){}

	AreaLight *DiffuseLightData::GetAreaLight(const Reference<Shape> &shape, const Reference<Primitive> &accel) const
	{
		return new DiffuseAreaLight(shape, accel, emit);
	}

	GeometricNode::GeometricNode(const Reference<ShapeNode> &shape, const Reference<Material> &material, const AreaLightData *areaLightData)
		:	accelType(NONE)
	{
		auto s = shape->GetShape();
		GeometricPrimitive *p;
		if(!s->CanIntersect())
			p = new RefinablePrimitive(s, material);
		else
			p = new GeometricPrimitive(s, material);

		primitive = p;

		if(areaLightData)
		{
			AreaLight *areaLight;
			if(!p->CanIntersect())
			{
				vector<Reference<Primitive>> prims;
				prims.push_back(primitive);
				areaLight = areaLightData->GetAreaLight(s, new BVHAccel(prims));
			}
			else
				areaLight = areaLightData->GetAreaLight(s, primitive);

			p->areaLight = areaLight;
			light = Reference<Light>(areaLight);
		}
	}
	void GeometricNode::SetAccelType(AccelType type)
	{
		accelType = type;
	}

	void GeometricNode::GetAggregate(vector<Reference<Primitive>> *primitives, vector<Reference<LightCollection>> *lightCollections, vector<Reference<Light>> *lights)
	{
		if(!primitive->CanIntersect() && accelType != NONE)
		{
			if(!accelerator)
			{
				vector<Reference<Primitive>> aPrim(1, primitive);
				switch(accelType)
				{
				case BINNEDKDTREE:
					accelerator = new BinnedKdTreeAccel(aPrim);
					break;
				default:
					accelerator = new BVHAccel(aPrim);
				}
			}
			primitives->emplace_back(accelerator);
		}
		else
			primitives->emplace_back(primitive);

		if(light)
			lights->emplace_back(light);
	}

	ShapeNode::ShapeNode(const Reference<Shape> &shape)
		:	shape(shape)
	{
	}

	const Reference<Shape> &ShapeNode::GetShape() const
	{
		return shape;
	}

	LightNode::LightNode(const Reference<Light> &light)
		:	light(light){}
	void LightNode::GetAggregate(vector<Reference<Primitive>> *primitives, vector<Reference<LightCollection>> *lightCollections, vector<Reference<Light>> *lights)
	{
		lights->push_back(light);
	}

	AnimatedSceneNode::AnimatedSceneNode(AccelType accelType)
		:	accelType(accelType), traversed(false)
	{
	}
	AnimatedSceneNode::AnimatedSceneNode(vector<Reference<SceneNode>> *nodes, AccelType accelType)
		:	accelType(accelType), traversed(false)
	{
		children.swap(*nodes);
	}
	void AnimatedSceneNode::SetAccelType(AccelType type)
	{
		accelType = type;
	}
	void AnimatedSceneNode::GetAggregate(vector<Reference<Primitive>> *primitives, vector<Reference<LightCollection>> *lightCollections, vector<Reference<Light>> *lights)
	{
		if(!traversed)
		{
			traversed = true;
			vector<Reference<Primitive>> prims;
			vector<Reference<LightCollection>> lghtCllctns;
			vector<Reference<Light>> lghts;
			for(size_t i = 0; i < children.size(); ++i)
				children[i]->GetAggregate(&prims, &lghtCllctns, &lghts);

			Reference<Primitive> accel;
			if(prims.size() == 1 && prims.front()->CanIntersect())
				accel = prims.front();
			else if(prims.size() != 0)
			{
				switch(accelType)
				{
					case AccelType::BINNEDKDTREE:
					{
						accel = new BinnedKdTreeAccel(prims);
						break;
					}
					default:
						accel = new BVHAccel(prims);
				}
			}

			if(transforms.size() == 0)
			{
				aggregate = accel;
				this->lights.swap(lghts);
				this->lightCollections.swap(lghtCllctns);
			}
			else
			{
				if(lghts.size() > 0) lghtCllctns.push_back(new LightSet(lghts));
				if(transforms.size() == 1)
				{
					if(lghtCllctns.size() > 0) this->lightCollections.push_back(new TransformedLightCollection(lghtCllctns, *transforms.begin()->second));
					if(accel) aggregate = new TransformedPrimitive(*transforms.begin()->second, accel);
				}
				else
				{
					vector<const Transform *> trans;
					vector<float> times;
					trans.reserve(transforms.size());
					times.reserve(transforms.size());
					Fill(&trans, &times);

					if(lghtCllctns.size() > 0) this->lightCollections.push_back(new AnimatedLightCollection(lghtCllctns, AnimatedTransform(trans, times)));
					if(accel) aggregate = new AnimatedPrimitive(AnimatedTransform(trans, times), accel);
				}
			}
		}

		if(aggregate)
			primitives->push_back(aggregate);

		if(this->lightCollections.size() > 0)
			lightCollections->insert(lightCollections->end(), this->lightCollections.begin(), this->lightCollections.end());

		if(this->lights.size() > 0)
			lights->insert(lights->end(), this->lights.begin(), this->lights.end());
	}

	CameraNode::CameraNode(float sOpen, float sClose, float focald, float lensr)
		:	sOpen(sOpen), sClose(sClose), focald(focald), lensr(lensr){}
	CameraNode::~CameraNode(){}

	PerspectiveCamNode::PerspectiveCamNode(float fov, float sOpen, float sClose, float focald, float lensr)
		:	CameraNode(sOpen, sClose, focald, lensr), fov(fov){}
	Camera *PerspectiveCamNode::MakeCamera(Film *film, float aspect) const
	{
		vector<const Transform *> transforms;
		vector<float> times;
		Fill(&transforms, &times);
		return new PerspectiveCamera(AnimatedTransform(transforms, times), Radians(fov), sOpen, sClose, focald, lensr, film, aspect);
	}

	SamplerNode::~SamplerNode(){}
	Sampler *SamplerNode::MakeSampler(const Film *film) const
	{
		return new RandomSampler(film->width, film->height);
	}

	Sampler *SobolNode::MakeSampler(const Film *film) const
	{
		return new SobolSampler(film->width, film->height);
	}

	RenderInstance::RenderInstance()
		:	film(NULL), camera(NULL), scene(NULL), renderer(NULL), camNode(NULL), sampler(NULL), samplerNode(NULL), surf(NULL), vol(NULL), isValid(false)
	{
		SampledSpectrum::Init();
	}
	RenderInstance::~RenderInstance()
	{
		ClearSceneGraph();
		ClearScene();
	}

	Renderer *RenderInstance::GetRenderer()
	{
		return renderer;
	}

	void RenderInstance::SceneInit()
	{
		if(isValid)
			Info("Clearing existing scene data...");

		ClearScene();
	}
	void RenderInstance::SceneFinish()
	{
		Info("Building Scene...");

		vector<Reference<Primitive>> aggregate;
		vector<Reference<LightCollection>> lightCollections;
		vector<Reference<Light>> lights;
		GetAggregate(&aggregate, &lightCollections, &lights);

		if(lights.size() == 0 && lightCollections.size() == 0)
		{
			Warning("No lights detected in scene.  Stopping.");
			ClearSceneGraph();
			return;
		}

		if(lights.size() > 0)
			lightCollections.push_back(new LightSet(lights));

		Reference<LightCollection> lightRoot = lightCollections.size() > 1 ? new LightAggregate(lightCollections) : lightCollections.front();

		if(camNode)
			camera = camNode->MakeCamera(film);
		else
			camera = PerspectiveCamNode(90, 0, 0, 0, 0).MakeCamera(film);

		if(samplerNode)
			sampler = samplerNode->MakeSampler(film);
		else
			sampler = new SobolSampler(film->width, film->height);

		if(!surf)
			surf = new Render::BidirIntegrator(5, 5);

		if(!vol)
			vol = new Render::SingleIntegrator(0.5f);

		VolumeRegion *volume = NULL;
		if(volumes.size() == 1)
			volume = volumes[0];
		else if(volumes.size() > 1)
			volume = new VolumeAggregate(volumes);

		scene = new Scene(aggregate.size() > 0 ? aggregate.front() : NULL, volume, lightRoot);

		renderer = new ProgressiveRenderer(scene, camera, sampler, surf, vol);

		isValid = true;
		ClearSceneGraph();
	}
	void RenderInstance::ClearScene()
	{
		isValid = false;
		PTR_DELETE(film);
		PTR_DELETE(camera);
		PTR_DELETE(scene);
		PTR_DELETE(renderer);
		PTR_DELETE(sampler);
		PTR_DELETE(surf);
		PTR_DELETE(vol);

		for(auto i = intensityCache.begin(); i != intensityCache.end(); ++i)
			delete i->second;
		for(auto i = imgCache.begin(); i != imgCache.end(); ++i)
			delete i->second;

		intensityCache.clear();
		imgCache.clear();
	}

	void RenderInstance::ClearSceneGraph()
	{
		PTR_DELETE(camNode);
		PTR_DELETE(samplerNode);
		transforms.clear();
		children.clear();
		lightCollections.clear();
		lights.clear();
		volumes.clear();
		aggregate = NULL;
		accelType = AccelType::BVH;
		traversed = false;
	}

	bool RenderInstance::IsValid() const
	{
		return isValid;
	}

	template<typename T> 
	BlockedArray<T> *RenderInstance::getTex(map<TexInfo, BlockedArray<T> *> &cache, const TexInfo &info)
	{
		auto iterator = cache.find(info);
		if(iterator != cache.end())
			return iterator->second;

		int nx, ny, nPixels;
		float *data = ReadImage(info.filename, &nx, &ny);
		nPixels = nx * ny;
		T *mem = new T[nPixels];
		for(int i = 0; i < nPixels; ++i)
			TexInfo::Convert(&data[i * 4], &mem[i], data[i * 4 + 3] * info.scale, info.gamma);

		delete[] data;
		auto elem = cache.emplace(info, new BlockedArray<T>(nx, ny, mem));
		delete[] mem;

		return elem.first->second;
	}

	BlockedArray<float> *RenderInstance::GetFloatImage(const TexInfo &info)
	{
		return getTex<float>(intensityCache, info);
	}
	BlockedArray<RGBSpectrum> *RenderInstance::GetSpectrumImage(const TexInfo &info)
	{
		return getTex<RGBSpectrum>(imgCache, info);
	}
	void RenderInstance::ImageFilm(int width, int height)
	{
		delete film;
		film = new Render::ImageFilm(width, height);
	}
	void RenderInstance::PerspectiveCamera(float fov, float sOpen, float sClose, float focald, float lensr)
	{
		delete camNode;
		camNode = new PerspectiveCamNode(fov, sOpen, sClose, focald, lensr);
	}
	void RenderInstance::BidirIntegrator(uint32_t lightBounces, uint32_t eyeBounces)
	{
		delete surf;
		surf = new Render::BidirIntegrator(lightBounces, eyeBounces);
	}
	void RenderInstance::PathIntegrator(uint32_t bounces)
	{
		delete surf;
		surf = new Render::PathIntegrator(bounces);
	}
	void RenderInstance::SingleIntegrator(float stepSize)
	{
		delete vol;
		vol = new Render::SingleIntegrator(stepSize);
	}
	void RenderInstance::UseRandomSampling()
	{
		delete samplerNode;
		samplerNode = new SamplerNode();
	}
	void RenderInstance::UseSobolSequence()
	{
		delete samplerNode;
		samplerNode = new SobolNode();
	}
	void RenderInstance::HomogeneousVolumeRegion(const Transform &toWorld, const BBox &bbox, const Spectrum &sig_a, const Spectrum &sig_s, const Spectrum &le, float g)
	{
		volumes.push_back(new HomogeneousVolume(toWorld, bbox, sig_a, sig_s, le, g));
	}
	void RenderInstance::AddCameraTransform(const Transform *transform, float time)
	{
		if(!camNode)
		{
			Warning("Camera not initialized.  Ignoring call to RenderInstance::AddCameraTransform.");
			return;
		}

		camNode->AddTransform(transform, time);
	}
	void RenderInstance::Parse(const char *filename)
	{
		if(EndsWithIgnoreCase(filename, ".lua"))
			luaParser.Parse(filename, this);
		else
			Error("Unsupported extension.","RenderInstance","Parse",1);

		if(!isValid)
		{
			ClearSceneGraph();
			ClearScene();
			return;
		}
	}
	void RenderInstance::AddChild(const Reference<SceneNode> &node)
	{
		children.push_back(node);
	}
}