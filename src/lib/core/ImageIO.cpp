/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "ImageIO.h"
#include "Spectrum.h"
#include "Memory.h"

#include <png.h>
#include <ImfInputFile.h>
#include <ImfRgbaFile.h>
#include <ImfChannelList.h>
#include <ImfFrameBuffer.h>
#include <half.h>

#define FileReadErrFMT "Unable to read file \"%s\" - %s."
#define FileWriteErrFMT "Unable to write to file \"%s\" - %s."
#define ReadImageInfoFMT "Read image \"%s\" (%d x %d)."

namespace Render
{
	void WriteImageEXR(const char *filename, float *pixels, float *alpha, int width, int height, int totalWidth, int totalHeight, int xOffset, int yOffset)
	{
		Imf::Rgba * hrgba = new Imf::Rgba[width * height];
		for(int i = 0; i < width * height; ++i)
			hrgba[i] = Imf::Rgba(pixels[3 * i], pixels[3 * i + 1], pixels[3 * i + 2], alpha ? alpha[i] : 1.0f);

		Imath::Box2i displayWindow(Imath::V2i(0, 0), Imath::V2i(totalWidth - 1, totalHeight - 1));
		Imath::Box2i dataWindow(Imath::V2i(xOffset, yOffset), Imath::V2i(xOffset + width - 1, yOffset + height - 1));

		try
		{
			Imf::RgbaOutputFile oFile(filename, displayWindow, dataWindow);
			oFile.setFrameBuffer(hrgba - xOffset - yOffset * height, 1, width);
			oFile.writePixels(height);
		}
		catch(const exception &e)
		{
			Error(FileReadErrFMT, STRNAMESPACE, "WriteImageEXR", 1, filename, e.what());
		}

		delete []hrgba;
	}

	void WriteImagePNG(const char *filename, float *pixels, float *alpha, int width, int height, int totalWidth, int totalHeight, int xOffset, int yOffset)
	{
	}

	float *ReadImageEXR(const char *filename, int *width, int *height)
	{
		float *ret = NULL;
		Imf::Rgba *rgb = NULL;
		try
		{
			Imf::InputFile file(filename);
			Imath::Box2i dw = file.header().dataWindow();
			*width = dw.max.x - dw.min.x + 1;
			*height = dw.max.y - dw.min.y + 1;

			rgb = new Imf::Rgba[*width * *height];
			
			Imf::FrameBuffer frameBuffer;
			frameBuffer.insert("R", Imf::Slice(Imf::HALF, (char *)rgb,						4 * sizeof(half), *width * 4 * sizeof(half), 1, 1, 0.0));
			frameBuffer.insert("G", Imf::Slice(Imf::HALF, (char *)rgb + sizeof(half),		4 * sizeof(half), *width * 4 * sizeof(half), 1, 1, 0.0));
			frameBuffer.insert("B", Imf::Slice(Imf::HALF, (char *)rgb + 2 * sizeof(half),	4 * sizeof(half), *width * 4 * sizeof(half), 1, 1, 0.0));
			frameBuffer.insert("A", Imf::Slice(Imf::HALF, (char *)rgb + 3 * sizeof(half),	4 * sizeof(half), *width * 4 * sizeof(half), 1, 1, 1.f));
			

			file.setFrameBuffer(frameBuffer);
			file.readPixels(dw.min.y, dw.max.y);

			ret = new float[*width * *height * 4];

			for(int i = 0, j = 0; i < *width * *height; ++i, j += 4)
			{
				ret[j] = rgb[i].r;
				ret[j + 1] = rgb[i].g;
				ret[j + 2] = rgb[i].b;
				ret[j + 3] = rgb[i].a;
			}

			Info(ReadImageInfoFMT, filename, *width, *height);
		}
		catch(const exception &e)
		{
			Error(FileWriteErrFMT, STRNAMESPACE, "ReadImageEXR", 1, filename, e.what());
		}

		delete[] rgb;

		return ret;
	}

	float *ReadImagePNG(const char *filename, int *width, int *height)
	{
		float *ret = NULL;
		FILE *iFile = fopen(filename, "rb");
		png_struct *png = NULL;
		png_info *pngInfo = NULL;
		try
		{
			if(!iFile)
				throw exception("File not found");

			png_byte sig[8];

			if( fread(sig, sizeof(png_byte), 8, iFile) != 8 || png_sig_cmp(sig, 0, 8) != 0)
				throw exception("Incorrect or corrupted signature, or file is not a PNG");

			png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
			if(!png)
				throw exception("Failed to read PNG");

			pngInfo = png_create_info_struct(png);
			if(!pngInfo)
				throw exception("Failed to read PNG");

			if(setjmp(png_jmpbuf(png)))
				throw exception("LibPNG encountered an error while reading the file");

			 png_set_sig_bytes(png, 8);

			 png_init_io(png, iFile);
			 png_read_png(png, pngInfo, PNG_TRANSFORM_IDENTITY, NULL);

			 *width = png_get_image_width(png, pngInfo);
			 *height = png_get_image_height(png, pngInfo);

			 int bitDepth = png_get_bit_depth(png, pngInfo);
			 if(bitDepth % 8 != 0)
				 throw exception("Invalid bit elements");

			 int colorType = png_get_color_type(png, pngInfo);
			 int channels = png_get_channels(png, pngInfo);

			 png_byte **rows = png_get_rows(png, pngInfo);

			 ret = new float[*width * *height * 4];

			 int channelLength = bitDepth / 8;
			 for(int y = 0; y < *height; ++y)
			 {
				png_byte *row = rows[y];
				for(int x = 0; x < *width; ++x)
				{
					for(int i = 0; i < 4; ++i)
					{
						int index = (y * *width + x) * 4 + i;
						long element = 0;

						if(i >= channels)
						{
							//If no opacity channel, then default opacity to 1.
							if(i == 3)
								ret[index] = 1.f;
							//If single channel (grayscale), then make all rgb components equal.
							else if(channels == 1)
								ret[index] = ret[index - 1];
							//If only two channels, set blue to 0 by default when i == 2.
							else if(channels == 2)
								ret[index] = 0.f;

							continue;
						}

						for(int j = 0; j < channelLength; ++j)
						{
							element <<= 8;
							element += row[(x * channels + i) * channelLength + j];
						}
						//Normalize to 1.
						ret[index] = float(element) / static_cast<float>((1 << bitDepth) - 1);
					}
				}
			}
		}
		catch(const exception &e)
		{
			Error(FileReadErrFMT, STRNAMESPACE, "ReadImagePNG", 1, filename, e.what());
		}

		if(png)
			png_destroy_read_struct(&png, &pngInfo, NULL);

		if(iFile)
			fclose(iFile);

		return ret;
	}

	void WriteImage(const string &filename, float *pixels, float *alpha, int width, int height, int totalWidth, int totalHeight, int xOffset, int yOffset)
	{
		if(filename.size() > 4 && EndsWithIgnoreCase(filename, ".exr"))
			WriteImageEXR
			(
				filename.c_str(),
				pixels,
				alpha,
				width, height, totalWidth, totalHeight, xOffset, yOffset
			);
		else if(filename.size() > 4 && EndsWithIgnoreCase(filename, ".png"))
			WriteImagePNG
			(
				filename.c_str(),
				pixels,
				alpha,
				width, height, totalWidth, totalHeight, xOffset, yOffset
			);
	}

	float *ReadImage(const string &filename, int *width, int *height)
	{
		float *ret;
		if(filename.size() > 4 && EndsWithIgnoreCase(filename, ".exr"))
			ret = ReadImageEXR(filename.c_str(), width, height);
		else if(filename.size() > 4 && EndsWithIgnoreCase(filename, ".png"))
			ret = ReadImagePNG(filename.c_str(), width, height);
		else
			Error("Unrecognized type \"%s\".", STRNAMESPACE, "ReadImage", 1, filename.c_str());

		if(ret == NULL)
		{
			*width = 1;
			*height = 1;
			ret = new float[4];
			memset(ret, 0, sizeof(float) * 4);
		}

		return ret;
	}
}