/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_PRIMITIVE_H
#define RENDERLIB_CORE_PRIMITIVE_H

#include "Shape.h"

namespace Render
{
	class Primitive : public ReferenceCounted
	{
	public:
		virtual ~Primitive();
		virtual BBox Bounds() const = 0;
		virtual bool CanIntersect() const;
		virtual bool Intersect(const Ray &ray, Intersection *intersection) const = 0;
		virtual bool IntersectP(const Ray &ray) const = 0;
		virtual void Refine(vector<const Primitive *> &refined) const;
		virtual void Refine(vector<Primitive *> &refined) const;
		//void FullyRefine(vector<Primitive *> &refined) const;
		virtual BSDF *GetBSDF(Intersection *intersection, MemoryArena &arena) const;
		virtual const AreaLight *GetAreaLight() const;
		virtual float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
	};

	class GeometricPrimitive : public Primitive
	{
	protected:
		Reference<Shape> shape;
		Reference<Material> material;
	public:
		const AreaLight *areaLight;
		GeometricPrimitive(const Reference<Shape> &shape, const Reference<Material> &material, const AreaLight *areaLight = NULL);
		BBox Bounds() const;
		bool CanIntersect() const;
		bool Intersect(const Ray &ray, Intersection *intersection) const;
		bool IntersectP(const Ray &ray) const;
		BSDF *GetBSDF(Intersection *intersection, MemoryArena &arena) const;
		const AreaLight *GetAreaLight() const;
		float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
	};

	class RefinablePrimitive;
	class RefinedPrimitive : public Primitive
	{
		RefinablePrimitive *parent;
		const Shape *shape;
	public:
		RefinedPrimitive(RefinablePrimitive *parent, const Shape *shape);
		BBox Bounds() const;
		bool CanIntersect() const;
		bool Intersect(const Ray &ray, Intersection *intersection) const;
		bool IntersectP(const Ray &ray) const;
		BSDF *GetBSDF(Intersection *intersection, MemoryArena &arena) const;
		const AreaLight *GetAreaLight() const;
		float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
	};

	class RefinablePrimitive : public GeometricPrimitive
	{
		vector<RefinedPrimitive *> refinedPrims;
	public:
		RefinablePrimitive(const Reference<Shape> &shape, const Reference<Material> &material, const AreaLight *areaLight = NULL);
		~RefinablePrimitive();
		void Refine(vector<const Primitive *> &refined) const;
		void Refine(vector<Primitive *> &refined) const;
	};

	class AnimatedPrimitive : public Primitive
	{
		AnimatedTransform toWorld;
		Reference<Primitive> primitive;
	public:
		AnimatedPrimitive(const AnimatedTransform &toWorld, const Reference<Primitive> &primitive);
		BBox Bounds() const;
		bool Intersect(const Ray &ray, Intersection *intersection) const;
		bool IntersectP(const Ray &ray) const;
	};

	class TransformedPrimitive : public Primitive
	{
		Transform toWorld;
		Reference<Primitive> primitive;
	public:
		TransformedPrimitive(const Transform &toWorld, const Reference<Primitive> &primitive);
		BBox Bounds() const;
		bool Intersect(const Ray &ray, Intersection *intersection) const;
		bool IntersectP(const Ray &ray) const;
	};

	class Aggregate : public Primitive
	{
	protected:
		vector<Reference<Primitive>> rPrims;
		vector<const Primitive *> primitives;
	public:
		virtual float Pdf(const Ray &rayWorld, const Ray &ray, const Transform *toWorld, Intersection *isect = NULL) const;
	};
}

#endif