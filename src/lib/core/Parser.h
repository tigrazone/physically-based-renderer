/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_PARSER_H
#define RENDERLIB_CORE_PARSER_H

#include "Global.h"

namespace Render
{
	class Parser
	{
		const Parser &operator=(const Parser &);
	public:
		virtual ~Parser(){}
		virtual void Parse(const char *filename, RenderInstance *instance) = 0;
	};
}

#endif