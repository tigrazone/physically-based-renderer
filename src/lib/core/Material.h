/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_MATERIAL_H
#define RENDERLIB_CORE_MATERIAL_H

#include "Memory.h"
#include "BxDF.h"
#include "Texture.h"

namespace Render
{
	class Material : public ReferenceCounted
	{
	public:
		static void Bump(const RFloatTexture &bump, const DifferentialGeometry &dgShading, const Normal &ng, DifferentialGeometry *dgBump);
		virtual ~Material();
		virtual BSDF *GetBSDF(DifferentialGeometry *dg, const Normal &ng, MemoryArena &arena) const = 0;
	};
}

#endif