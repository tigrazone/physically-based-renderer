/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Material.h"

namespace Render
{
	void Material::Bump(const RFloatTexture &bump, const DifferentialGeometry &dgShading, const Normal &ng, DifferentialGeometry *dgBump)
	{
		if(!bump)
		{
			if(dgBump != &dgShading)
				*dgBump = dgShading;

			return;
		}
		/*
		float du = 0.5 * (abs(dgShading.dudx) + abs(dgShading.dudy));
		float dv = 0.5 * (abs(dgShading.dvdx) + abs(dgShading.dvdy));

		if(du == 0.f)
			du = 0.001f;
		if(dv == 0.f)
			dv = 0.001f;
		*/

		float du = 0.01f, dv = 0.01f;

		DifferentialGeometry eval(	dgShading.p + du * dgShading.dpdu, 
									dgShading.dpdu, dgShading.dpdv,
									dgShading.n,
									dgShading.u + du, 
									dgShading.v);
		float uDisplace = bump->Evaluate(eval);

		eval.p = dgShading.p + dv * dgShading.dpdv;
		eval.u = dgShading.u;
		eval.v = dgShading.v + dv;
		float vDisplace = bump->Evaluate(eval);

		float displace = bump->Evaluate(dgShading);
		if(dgBump != &dgShading)
			*dgBump = dgShading;

		//Apparently, dndu * displace and dndv * displace cause artifacts.  So completely give up on computing dn* anywhere.
		dgBump->dpdu = dgShading.dpdu + (Vector) (dgShading.n * ((uDisplace - displace) / du));// + displace * dgShading.dndu);
		dgBump->dpdv = dgShading.dpdv + (Vector) (dgShading.n * ((vDisplace - displace) / dv));// + displace * dgShading.dndv);
		dgBump->n = Normalize(Cross(dgBump->dpdu, dgBump->dpdv));

		if(Dot(dgBump->n, ng) < 0)
			dgBump->n *= -1.f;
	}

	Material::~Material(){}
}