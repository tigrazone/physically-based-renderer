/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_VOLUME_H
#define RENDERLIB_CORE_VOLUME_H

#include "RNG.h"
#include "Algebra.h"
#include "Spectrum.h"
#include "Texture.h"

//PhaseIsotropic - Since it's isotropic, is normalized, and must integrate over the entire surface of a sphere, it is equivalent to INV_FOURPI
#define PhaseIsotropic INV_FOURPI

namespace Render
{
	float PhaseRayleigh(const Vector &w, const Vector &wp);
	float PhaseMieHazy(const Vector &w, const Vector &wp);
	float PhaseMieMurky(const Vector &w, const Vector &wp);
	float PhaseHG(const Vector &w, const Vector &wp, float g);
	float PhaseSchlick(const Vector &w, const Vector &wp, float g);

	//Initial tentative implementation based on the PBRT book (may need a bit of tweaking so that geometric primitives can be used to determine shapes)

	class VolumeRegion
	{
	protected:
		Transform toWorld;
		BBox bounds;
	public:
		VolumeRegion();
		VolumeRegion(const Transform &toWorld, const BBox &bounds);
		virtual ~VolumeRegion();
		virtual Spectrum SigmaA(const Point &p, const Vector &w, float time) const = 0;
		virtual Spectrum SigmaS(const Point &p, const Vector &w, float time) const = 0;
		virtual Spectrum SigmaT(const Point &p, const Vector &w, float time) const;
		virtual Spectrum Lve(const Point &p, const Vector &w, float time) const = 0;
		virtual float P(const Point &p, const Vector &w, const Vector &wp, float time) const = 0;
		virtual Spectrum Tau(const Ray &ray, float step = 1.f, float offset = 0.5f) const = 0;
		virtual bool IntersectP(const Ray &ray, float *t0, float *t1) const;
		virtual BBox WorldBound() const;
	};

	class VolumeAggregate : public VolumeRegion
	{
		vector<VolumeRegion *> volumes;
	public:
		VolumeAggregate(const vector<VolumeRegion *> &volumes);
		~VolumeAggregate();
		virtual Spectrum SigmaA(const Point &p, const Vector &w, float time) const;
		virtual Spectrum SigmaS(const Point &p, const Vector &w, float time) const;
		virtual Spectrum SigmaT(const Point &p, const Vector &w, float time) const;
		virtual Spectrum Lve(const Point &p, const Vector &w, float time) const;
		virtual float P(const Point &p, const Vector &w, const Vector &wp, float time) const;
		virtual Spectrum Tau(const Ray &ray, float step = 1.f, float offset = 0.5f) const;
	};
}

#endif