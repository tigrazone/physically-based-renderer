/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Renderer.h"

namespace Render
{
	Renderer::Renderer(Scene *scene, Camera *camera, Sampler *sampler, SurfaceIntegrator *surf, VolumeIntegrator *vol)
		:	scene(scene), camera(camera), sampler(sampler), surf(surf), vol(vol),
			nThreads(opts.nThreads > 0 ? opts.nThreads : NumThreads()),
			samples(sampler->CreateSamples(nThreads)), arenas(new MemoryArena[nThreads])
	{
		Integrator::RequestSamples(sampler, surf, vol, scene);
	}
	Renderer::~Renderer()
	{
		delete[] arenas;
		sampler->FreeSamples(samples, nThreads);
	}
}