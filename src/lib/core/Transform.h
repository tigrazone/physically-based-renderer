/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_TRANSFORM_H
#define RENDERLIB_CORE_TRANSFORM_H

#include "Quaternion.h"
#include <map>

namespace Render
{
	class Transform
	{
	protected:
		Mat4 m;
		Mat4 mInv;
	public:
		Transform();
		Transform(const Mat4 &m);
		Transform(const Mat4 &m, const Mat4 &mInv);
		inline Transform Inverse() const
		{
			return Transform(mInv, m);
		}
		inline void operator()(const Point &p, Point *pTrans, bool useInverse = false) const;
		inline void operator()(const Vector &v, Vector *vTrans, bool useInverse = false) const;
		inline void operator()(const Normal &n, Normal *nTrans, bool useInverse = false) const;
		inline void operator()(const Ray &r, Ray *rTrans, bool useInverse = false) const;
		inline void operator()(const RayDifferential &r, RayDifferential *rTrans, bool useInverse = false) const;
		inline void operator()(const BBox &b, BBox *bTrans, bool useInverse = false) const;
		inline Point operator()(const Point &p, bool useInverse = false) const;
		inline Vector operator()(const Vector &v, bool useInverse = false) const;
		inline Normal operator()(const Normal &n, bool useInverse = false) const;
		inline Ray operator()(const Ray &r, bool useInverse = false) const;
		inline RayDifferential operator()(const RayDifferential &r, bool useInverse = false) const;
		inline BBox operator()(const BBox &b, bool useInverse = false) const;
		inline Transform operator*(const Transform &t) const;
		bool SwapsHandedness() const;
		bool HasScale() const;
		inline bool operator==(const Transform &t) const;
		inline bool operator==(const Mat4 &m) const;
		inline bool operator!=(const Transform &t) const;
		inline void Identity();
		friend struct Quaternion;
		friend class InterpolatedTransform;
	};

	class InterpolatedTransform
	{
		struct DecomposedTransform
		{
			Vector T;
			Quaternion R;
			Mat4 S;
		};
		bool isAnimated;
		Transform startTrans, endTrans;
		float start, end;

		bool translates;
		bool rotates;
		bool scales;
		DecomposedTransform startDec, endDec;
		
		float qCosTheta, qTheta, invTime;
		Quaternion qPerp;
	public:
		static void Decompose(const Mat4 &m, Vector *T, Quaternion *R, Mat4 *S);
		InterpolatedTransform(const Transform &startTrans, float start, const Transform &endTrans, float end);
		void Interpolate(float time, Transform *t) const;
		friend class AnimatedTransform;
	};
	
	class AnimatedTransform
	{
		bool isSingular;
		//Set to true when AnimatedTransform has no transforms, or if it has a singular transform that is the identity matrix.
		bool isIdentity;
		vector<float> times;
		vector<InterpolatedTransform> transforms;
	public:
		AnimatedTransform(const vector<const Transform*> &tfs, const vector<float> &times);
		BBox MotionBounds(const BBox &b, bool useInverse = false) const;
		void Interpolate(float time, Transform *t) const;

		template<typename Geometry>
		Geometry operator()(float time, const Geometry &geom, bool useInverse = false) const
		{
			if(isIdentity)
				return geom;

			if(isSingular || (transforms.size() == 1 && !transforms.front().isAnimated) || time <= times.front())
				return transforms.front().startTrans(geom, useInverse);
			else if(time >= times.back())
				return transforms.back().endTrans(geom, useInverse);

			size_t i = lower_bound(times.begin(), times.end(), time) - times.begin() - 1;
			if(!transforms[i].isAnimated)
				return transforms[i].startTrans(geom, useInverse);

			Transform interp;
			transforms[i].Interpolate(time, &interp);
			return interp(geom, useInverse);
		}

		template<typename Geometry>
		void operator()(float time, const Geometry &geom, Geometry *geomTrans, bool useInverse = false) const
		{
			if(isIdentity)
			{
				*geomTrans = geom;
				return;
			}

			if(isSingular || (transforms.size() == 1 && !transforms.front().isAnimated) || time <= times.front())
			{
				transforms.front().startTrans(geom, geomTrans, useInverse);
				return;
			}
			else if(time >= times.back())
			{
				transforms.back().endTrans(geom, geomTrans, useInverse);
				return;
			}

			size_t i = lower_bound(times.begin(), times.end(), time) - times.begin() - 1;

			if(!transforms[i].isAnimated)
			{
				transforms[i].startTrans(geom, geomTrans, useInverse);
				return;
			}

			Transform interp;
			transforms[i].Interpolate(time, &interp);
			interp(geom, geomTrans, useInverse);
		}

		inline bool HasScale() const
		{
			if(isIdentity)
				return false;

			for(size_t i = 0; i < transforms.size(); ++i)
				if(transforms[i].startTrans.HasScale())
					return true;
			return transforms.back().endTrans.HasScale();
		}

		inline bool IsIdentity() { return isIdentity; }
	};

	inline void Transform::operator()(const Point &p, Point *pTrans, bool useInverse) const
	{
		!useInverse ? m(p, pTrans) : mInv(p, pTrans);
	}
	inline void Transform::operator()(const Vector &v, Vector *vTrans, bool useInverse) const
	{
		!useInverse ? m(v, vTrans) : mInv(v, vTrans);
	}
	inline void Transform::operator()(const Normal &n, Normal *nTrans, bool useInverse) const
	{
		!useInverse ? mInv(n, nTrans) : m(n, nTrans);
	}
	inline void Transform::operator()(const Ray &r, Ray *rTrans, bool useInverse) const
	{
		!useInverse ? m(r, rTrans) : mInv(r, rTrans);
	}
	inline  void Transform::operator()(const RayDifferential &r, RayDifferential *rTrans, bool useInverse) const
	{
		!useInverse ? m(r, rTrans) : mInv(r, rTrans);
	}
	inline void Transform::operator()(const BBox &b, BBox *bTrans, bool useInverse) const
	{
		!useInverse ? m(b, bTrans) : mInv(b, bTrans);
	}
	inline Point Transform::operator()(const Point &p, bool useInverse) const
	{
		Point pTrans;
		!useInverse ? m(p, &pTrans) : mInv(p, &pTrans);
		return pTrans;
	}
	inline Vector Transform::operator()(const Vector &v, bool useInverse) const
	{
		Vector vTrans;
		!useInverse ? m(v, &vTrans) : mInv(v, &vTrans);
		return vTrans;
	}
	inline Normal Transform::operator()(const Normal &n, bool useInverse) const
	{
		Normal nTrans;
		!useInverse ? mInv(n, &nTrans) : m(n, &nTrans);
		return nTrans;
	}
	inline Ray Transform::operator()(const Ray &r, bool useInverse) const
	{
		Ray rTrans;
		!useInverse ? m(r, &rTrans) : mInv(r, &rTrans);
		return rTrans;
	}
	inline RayDifferential Transform::operator()(const RayDifferential &r, bool useInverse) const
	{
		RayDifferential rTrans;
		!useInverse ? m(r, &rTrans) : mInv(r, &rTrans);
		return rTrans;
	}
	inline BBox Transform::operator()(const BBox &b, bool useInverse) const
	{
		BBox bTrans;
		!useInverse ? m(b, &bTrans) : mInv(b, &bTrans);
		return bTrans;
	}
	inline Transform Transform::operator*(const Transform &t) const
	{
		return Transform(m * t.m, t.mInv * mInv);
	}
	inline bool Transform::operator==(const Transform &t) const
	{
		return m == t.m;
	}
	inline bool Transform::operator==(const Mat4 &m) const
	{
		return this->m == m;
	}

	inline bool Transform::operator!=(const Transform &t) const
	{
		return m != t.m;
	}

	inline void Transform::Identity()
	{
		m = Mat4::identity;
		mInv = Mat4::identity;
	}

	Transform Translate(const Vector &d);
	Transform RotateX(float radians);
	Transform RotateY(float radians);
	Transform RotateZ(float radians);
	Transform Rotate(const Vector &v, float radians);
	Transform Scale(float x, float y, float z);
	Transform Scale(const Vector &s);
	Transform LookAt(const Point &p, const Point &at, const Vector &up);
	bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float *x0, float *x1);
}

#endif