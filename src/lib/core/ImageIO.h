/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_IMAGEIO_H
#define RENDERLIB_CORE_IMAGEIO_H

#include "Film.h"
namespace Render
{
	void WriteImage(const string &filename, float *pixels, float *alpha, int width, int height, int totalWidth, int totalHeight, int xOffset, int yOffset);
	RGBSpectrum *ReadImageRGBSpectrum(const string &filename, int *width, int *height);
	SampledSpectrum *ReadImageSampledSpectrum(const string &filename, int *width, int *height);
	float *ReadImage(const string &filename, int *width, int *height);
}

#endif