/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Camera.h"
#include "Film.h"

namespace Render
{
	Transform Camera::RasterToScreen(float width, float height, float aspect)
	{
		if(aspect == 0.f)
			aspect = width / height;

		float screen[4];
		if(aspect >= 1.f)
		{
			screen[0] = -aspect;
			screen[1] = aspect;
			screen[2] = -1.f;
			screen[3] = 1.f;
		}
		else
		{
			screen[0] = -1.f;
			screen[1] = 1.f;
			screen[2] = -1.f / aspect;
			screen[3] = 1.f / aspect;
		}
		return Transform(Mat4(	(screen[1] - screen[0]) / width, 0, 0, screen[0],
								0, (screen[2] - screen[3]) / height, 0, screen[3],
								0, 0, 1.f, 0,
								0, 0, 0, 1.f));
	}

	Camera::Camera(const AnimatedTransform &toWorld, const Transform &persp, float sOpen, float sClose, float focald, float lensr, Film *film, float aspect)
		:	toWorld(toWorld), film(film), sOpen(sOpen), sClose(sClose), focald(abs(focald)), lensr(abs(lensr)),
			rasterToCamera(persp * RasterToScreen(float(film->width), float(film->height), aspect))
	{
		if(toWorld.HasScale())
			Warning("Scaling detected in world-to-camera transform.  Errors will occur.");
	}
	Camera::~Camera(){}

	float Camera::GetTime(float timeU) const
	{
		return Lerp(timeU, sOpen, sClose);
	}
}