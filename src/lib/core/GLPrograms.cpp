/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "GLPrograms.h"

namespace Render
{
	GLShaderSource::GLShaderSource(const char *src, bool isFile, GLShader type)
		:	glsl(src), isFile(isFile), type(type){}

	const char *GLShaderSource::operator()()
	{
		if(isFile)
		{
			ReadFile(&glsl, glsl);
			isFile = false;
		}

		return glsl.c_str();
	}

//---------------------------------------------------------------------------------------------------------

	GLShaderObjectMap::~GLShaderObjectMap()
	{
		for(ShaderMap::iterator i = shaders.begin(); i != shaders.end(); ++i)
			glDeleteShader(i->second);

		shaders.clear();
	}

	GLuint GLShaderObjectMap::Compile(GLShaderSource *key) const
	{
		GLuint handle = glCreateShader(key->type);
		const char *glsl = key->operator()();

		glShaderSource(handle, 1, &glsl, NULL);
		glCompileShader(handle);

		GLint result;
		GLint logLength;

		glGetShaderiv(handle, GL_COMPILE_STATUS, &result);
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLength);

		if(logLength > 0)
		{
			char *msg = new char[logLength];

			glGetShaderInfoLog(handle, logLength, NULL, msg);

			if(!result)
			{
				Error("%sSOURCE\n%s", "GLShaders", "Compile", 0, msg, glsl);
				glDeleteShader(handle);
				handle = 0;
			}
			else
				Warning(msg);

			delete[] msg;
		}

		return handle;
	}

	GLuint GLShaderObjectMap::Find(GLShaderSource *key)
	{
		ShaderMap::iterator i = shaders.find(key);
		if(i == shaders.end())
		{
			GLuint result = Compile(key);
			shaders.emplace(key, result);
			return result;
		}
		
		return i->second;
	}

//---------------------------------------------------------------------------------------------------------

	GLProgramLinkMap::~GLProgramLinkMap()
	{
		DeletePrograms();
	}

	GLuint GLProgramLinkMap::Link(GLShaderObjectMap *shaderMap, GLShaderSource *const *sources, int n)
	{
		GLuint hProgram = glCreateProgram();

		GLint result;
		GLint logLength;
		for(int i = 0; i < n; ++i)
			glAttachShader(hProgram, shaderMap->Find(sources[i]));

		glLinkProgram(hProgram);
		glGetProgramiv(hProgram, GL_LINK_STATUS, &result);
		glGetProgramiv(hProgram, GL_INFO_LOG_LENGTH, &logLength);

		if(logLength > 0)
		{
			char *msg = new char[logLength];
			glGetProgramInfoLog(hProgram, logLength, NULL, msg);

			if(!result)
				Error("Problem linking GL shader program:  %s", "GLShaders", "LinkShader", 0, msg);
			else
				Warning(msg);

			delete[] msg;
		}

		for(int i = 0; i < n; ++i)
			glDetachShader(hProgram, shaderMap->Find(sources[i]));

		return hProgram;
	}

	void GLProgramLinkMap::DeletePrograms()
	{
		if(programs.empty())
			return;

		for(ProgramMap::iterator i = programs.begin(); i != programs.end(); ++i)
			glDeleteProgram(i->second.handle);

		programs.clear();
	}
}