/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Light.h"
#include "Scene.h"
#include "Renderer.h"

namespace Render
{
	Light::~Light(){}
	void Light::Le(const Transform *t, const Ray &ray, Spectrum &s) const {}
	bool Light::Le(const Transform *t, const Scene *scene, const Ray &ray, Spectrum &s, float *pdfDirect, float *pdfEmission, Normal *ng) const { return false; }
	bool Light::IsEnvironment() const { return false; }
	float DeltaLight::Pdf(const Transform *t, const Point &p, const Vector &wi) const
	{
		return 1.f;
	}
	bool DeltaLight::IsDelta() const { return true; }

	bool DeltaLight::EstimateDirect(const Renderer *renderer, const Scene *scene,
									Sample *sample, uint32_t offset, 
									const BSDF *bsdf, BxDFType flags, const Transform &transform,
									const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena) const
	{
		if(bsdf->specularOnly)
			return false;

		Vector wo = -ray.d;
		Vector wi;
		Spectrum ld;
		float pdf;
		if(SampleL(&transform, p, sample, offset, &wi, ld, &pdf, &isect, &ray) && !ld.IsBlack())
		{
			Spectrum f = bsdf->f(wo, wi, flags);
			if(!f.IsBlack() && !scene->IntersectP(ray))
			{
				s = f * ld / pdf;
				renderer->vol->Transmittance(renderer, scene, sample, ray, s, arena);
				return true;
			}
		}

		return false;
	}

	bool SampleLight::IsDelta() const { return false; }

	bool SampleLight::EstimateDirect(	const Renderer *renderer, const Scene *scene,
										Sample *sample, uint32_t offset, 
										const BSDF *bsdf, BxDFType flags, const Transform &transform,
										const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena) const
	{
		if(bsdf->specularOnly && !(flags & SPECULAR))
			return false;

		bool sampled = false;
		Vector wo = -ray.d;
		Vector wi;
		Spectrum li;
		float pdf;

		if(!bsdf->specularOnly && SampleL(&transform, p, sample, offset, &wi, li, &pdf, &isect, &ray) && !li.IsBlack())
		{
			Spectrum f = bsdf->f(wo, wi, flags);
			if(!f.IsBlack() && !scene->IntersectP(ray))
			{
				float pdfF = bsdf->Pdf(wo, wi, flags);
				s = (f * li) * (PowerHeuristic(1, pdf, 1, pdfF) / pdf);
				renderer->vol->Transmittance(renderer, scene, sample, ray, s, arena);
				sampled = true;
			}
		}
		
		offset += 3;
		float u[3];
		sample->sampler->GetSamples(sample, offset, u, 3);
		BxDFType sampledType;
		Spectrum f;
		if(bsdf->Sample_f(wo, &wi, u, &pdf, flags, f, &sampledType) && !f.IsBlack())
		{
			ray = Ray(p, wi, RAYEPSILON, INFINITY, ray.time);
			float weight = 1.f;
			if(!(sampledType & SPECULAR))
			{
				float pdfL = Pdf(&transform, p, wi);
				if(pdfL == 0)
					return sampled;

				weight = PowerHeuristic(1, pdf, 1, pdfL);
			}

			if(scene->Intersect(ray, &isect))
			{
				if(isect.prim->GetAreaLight() == this && isect.toWorld == transform)
				{
					isect.shape->GetDifferentialGeometry(ray, &isect);
					if(!isect.Le(-ray.d, li))
						return sampled;
				}
				else
					return sampled;
			}
			else
			{
				li = 0.f;
				Le(&transform, ray, li);
			}

			if(!li.IsBlack())
			{
				li *= f * (weight / pdf);
				renderer->vol->Transmittance(renderer, scene, sample, ray, s, arena);
				if(!sampled) s = li;
				else s += li;
				sampled = true;
			}
		}

		return sampled;
	}

	ShapeSet::ShapeSet(const Reference<Shape> &shape, const Reference<Primitive> &accel)
		:	accel(accel), totalArea(0.f), invTotalArea(0.f)
	{
		if(!shape->CanIntersect())
			shape->Refine(shapes);
		else
			shapes.push_back(shape.GetPtr());

		float *areas = new float[shapes.size()];
		for(size_t i = 0; i < shapes.size(); ++i)
		{
			areas[i] = shapes[i]->Area();
			totalArea += areas[i];
		}

		distribution1D = new Distribution1D(areas, shapes.size());
		delete[] areas;

		invTotalArea = 1.f / totalArea;
	}
	ShapeSet::~ShapeSet()
	{
		delete distribution1D;
	}

	Point ShapeSet::Sample(float u[3], Intersection *isect) const
	{
		size_t which = distribution1D->SampleDiscrete(u[2], NULL);
		return shapes[which]->Sample(u[0], u[1], isect);
	}
	float ShapeSet::Pdf(const Point &p, const Vector &wi, Intersection *isect, const Transform *toWorld) const
	{
		//float invTransformWeight = InvTransformWeight(toWorld);
		//if(isinf(invTransformWeight)) return 0.f;

		Ray rayW(p, wi, RAYEPSILON);
		Ray ray;
		toWorld->operator()(rayW, &ray, true);

		if(isect)
			isect->shape = NULL;

		float pdf = accel->Pdf(rayW, ray, toWorld, isect) * invTotalArea;// * invTransformWeight;

		if(isect && isect->shape)
			isect->shape->GetDifferentialGeometry(rayW, isect, toWorld);

		return pdf;
	}
	float ShapeSet::Pdf(const Point &p, const Transform *t) const
	{
		return invTotalArea;
	}

	float ShapeSet::Area() const
	{
		return totalArea;
	}

	LightCollection::~LightCollection(){}

	TransformedLight *LightCollection::GetLightInstances() const
	{
		TransformedLight *lights = AllocAligned<TransformedLight>(Size());
		TransformedLight *iter = lights;
		setLight(&iter);
		return lights;
	}
	void LightCollection::ToWorld(TransformedLight *lights, float time) const
	{
		lightsToWorld(&lights, time);
	}

	LightAggregate::LightAggregate(const vector<Reference<LightCollection>> &nodes)
		:	nodes(nodes), size(0)
	{
		for(unsigned int i = 0; i < nodes.size(); ++i)
			size += this->nodes[i]->Size();
	}
	void LightAggregate::setLight(TransformedLight **lightInstances) const
	{
		for(unsigned int i = 0; i < size; ++i)
			nodes[i]->setLight(lightInstances);
	}
	void LightAggregate::lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld) const
	{
		for(unsigned int i = 0; i < size; ++i)
			nodes[i]->lightsToWorld(lights, time, toWorld);
	}
	unsigned int LightAggregate::Size() const
	{
		return size;
	}

	LightSet::LightSet(const vector<Reference<Light>> &lights)
		:	lights(lights)
	{
	}
	unsigned int LightSet::Size() const
	{
		return lights.size();
	}
	void LightSet::lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld) const
	{
		if(toWorld)
		{
			for(unsigned int i = 0; i < this->lights.size(); ++i)
			{
				(*lights)->toWorld = *toWorld;
				++(*lights);
			}
		}
	}
	void LightSet::setLight(TransformedLight **lightInstances) const
	{
		for(unsigned int i = 0; i < this->lights.size(); ++i)
		{
			new (*lightInstances) TransformedLight(lights[i].GetPtr());
			++(*lightInstances);
		}
	}
	void TransformedLightCollection::lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld) const
	{
		if(toWorld)
		{
			Transform t = *toWorld * this->toWorld;
			for(unsigned int i = 0; i < nodes.size(); ++i)
				nodes[i]->lightsToWorld(lights, time, &t);
		}
		else
			for(unsigned int i = 0; i < nodes.size(); ++i)
				nodes[i]->lightsToWorld(lights, time, &this->toWorld);
	}

	TransformedLightCollection::TransformedLightCollection(const vector<Reference<LightCollection>> &nodes, const Transform &toWorld)
		:	LightAggregate(nodes), toWorld(toWorld)
	{
	}

	AnimatedLightCollection::AnimatedLightCollection(const vector<Reference<LightCollection>> &nodes, const AnimatedTransform &toWorld)
		:	LightAggregate(nodes), toWorld(toWorld)
	{
	}
	void AnimatedLightCollection::lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld) const
	{
		Transform t;
		this->toWorld.Interpolate(time, &t);
		if(toWorld) t = *toWorld * t;

		for(unsigned int i = 0; i < size; ++i)
			nodes[i]->lightsToWorld(lights, time, &t);
	}
}