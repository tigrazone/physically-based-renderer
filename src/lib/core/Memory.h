/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_MEMORY_H
#define RENDERLIB_CORE_MEMORY_H

#include "Global.h"

#define ARENA_ALLOC(arena, Type) new (arena.Alloc(sizeof(Type))) Type

namespace Render
{
	void *AllocAligned(size_t size);
	template <typename T> T *AllocAligned(size_t count)
	{
		return (T *)AllocAligned(count * sizeof(T));
	}

	void FreeAligned(void *);

	class ReferenceCounted
	{
		ReferenceCounted(const ReferenceCounted &);
		ReferenceCounted &operator=(const ReferenceCounted &);
	public:
		atomic_count nReferences;
		ReferenceCounted():nReferences(0) { }
	};

	template <typename T>
	class Reference
	{
		T *ptr;
	public:
		Reference(T *ptr = NULL):ptr(ptr)
		{
			if(ptr)
				++ptr->nReferences;
		}

		Reference(const Reference<T> &r)
		{
			ptr = r.ptr;
			if(ptr)
				++ptr->nReferences;
		}

		Reference &operator=(const Reference<T> &r)
		{
			if(r.ptr)
				++r.ptr->nReferences;
			if(ptr && (--ptr->nReferences) == 0)
				delete ptr;

			ptr = r.ptr;
			return *this;
		}

		Reference &operator=(T *p)
		{
			if(p)
				++p->nReferences;
			if(ptr && (--ptr->nReferences) == 0)
				delete ptr;

			ptr = p;
			return *this;
		}

		~Reference()
		{
			if(ptr && (--ptr->nReferences) == 0)
				delete ptr;
		}

		T *operator->(){ return ptr; }

		const T *operator->() const { return ptr; }
		operator bool() const { return ptr != NULL; }
		const T *GetPtr() const { return ptr; }
	};

	class MemoryArena
	{
		size_t blockSize;
		size_t blockPos;
		vector<char *> blocks;
		vector<size_t> blockSizes;
		size_t currentBlock;
	public:
		MemoryArena(uint32_t blockSize = 32768) : blockSize(blockSize), blockPos(0), currentBlock(0)
		{
			blocks.push_back(AllocAligned<char>(blockSize));
			blockSizes.push_back(blockSize);
		}

		~MemoryArena()
		{
			for(size_t i = 0; i < blocks.size(); ++i)
				FreeAligned(blocks[i]);
		}

		void *Alloc(size_t size)
		{
			//Round up to a 4 byte boundary.
			size = ((size + 3) & (~3));
			if(blockPos + size > blockSizes[currentBlock])
			{
				++currentBlock;
				if(currentBlock < blocks.size())
				{
					if(size > blockSizes[currentBlock])
					{
						blocks.push_back(blocks[currentBlock]);
						blockSizes.push_back(blockSizes[currentBlock]);
						blocks[currentBlock] = (AllocAligned<char>(size));
						blockSizes[currentBlock] = size;
					}
				}
				else
				{
					size_t bSize = max(size, blockSize);
					blocks.push_back(AllocAligned<char>(bSize));
					blockSizes.push_back(bSize);
				}

				blockPos = 0;
			}

			void *ret = blocks[currentBlock] + blockPos;
			blockPos += size;
			return ret;
		}

		template<typename T> T *Alloc(size_t count = 1)
		{
			T *ret = (T *)Alloc(count * sizeof(T));
			for(size_t i = 0; i < count; ++i)
				new (&ret[i]) T();

			return ret;
		}

		void FreeAll()
		{
			blockPos = 0;
			currentBlock = 0;
		}
	};

	//Since block size should be a power of 2, logBlockSize specifies the exponent
	template <typename T, int logBlockSize>
	class BlockedArray
	{
		T *data;
		uint32_t uRes, vRes, uBlocks;
	public:
		const uint32_t blockSize;
		BlockedArray(uint32_t nu, uint32_t nv, const T *d = NULL)
			:	uRes(nu), vRes(nv), blockSize(1 << logBlockSize)
		{
			if(nu == 0 || nv == 0)
			{
				data = NULL;
				return;
			}

			uBlocks = RoundUp(uRes) >> logBlockSize;
			uint32_t nAlloc = RoundUp(uRes) * RoundUp(vRes);
			data = AllocAligned<T>(nAlloc);

			for(uint32_t i = 0; i < nAlloc; ++i)
				new (&data[i]) T();

			if(d)
				for(uint32_t v = 0; v < vRes; ++v)
					for(uint32_t u = 0; u < uRes; ++u)
						(*this)(u, v) = d[v * uRes + u];
		}
		~BlockedArray()
		{
			for(uint32_t i = 0; i < uRes * vRes; ++i)
				data[i].~T();
			FreeAligned(data);
		}

		uint32_t BlockSize() const
		{
			return blockSize;
		}

		uint32_t RoundUp(uint32_t x) const
		{
			return (x + blockSize - 1 ) & ~(blockSize - 1);
		}
	
		uint32_t uSize() const { return uRes; }
		uint32_t vSize() const { return vRes; }

		uint32_t Block(uint32_t a) const { return a >> logBlockSize; }
		uint32_t Offset(uint32_t a) const { return a & (blockSize - 1); }

		T &operator()(uint32_t u, uint32_t v)
		{
			uint32_t bu = Block(u), bv = Block(v);
			uint32_t ou = Offset(u), ov = Offset(v);
			uint32_t offset = blockSize * blockSize * (uBlocks * bv + bu);
			offset += blockSize * ov + ou;
			return data[offset];
		}

		const T &operator()(uint32_t u, uint32_t v) const
		{
			uint32_t bu = Block(u), bv = Block(v);
			uint32_t ou = Offset(u), ov = Offset(v);
			uint32_t offset = blockSize * blockSize * (uBlocks * bv + bu);
			offset += blockSize * ov + ou;
			return data[offset];
		}
	};
}
#endif