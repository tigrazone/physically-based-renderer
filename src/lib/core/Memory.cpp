/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Memory.h"

namespace Render
{
	// Memory Allocation Functions
	void *AllocAligned(size_t size)
	{
	#if defined(IS_WINDOWS)
		return _aligned_malloc(size, L1_CACHE_LINE_SIZE);
	#elif defined(IS_OPENBSD) || defined(IS_APPLE)
		// Allocate excess memory to ensure an aligned pointer can be returned
		void *mem = malloc(size + (L1_CACHE_LINE_SIZE-1) + sizeof(void*));
		char *amem = ((char*)mem) + sizeof(void*);
	#if(POINTER_SIZE == 8)
		amem += L1_CACHE_LINE_SIZE - (reinterpret_cast<uint64_t>(amem) &
										   (L1_CACHE_LINE_SIZE - 1));
	#else
		amem += L1_CACHE_LINE_SIZE - (reinterpret_cast<uint32_t>(amem) &
										   (L1_CACHE_LINE_SIZE - 1));
	#endif
		((void**)amem)[-1] = mem;
		return amem;
	#else
		return memalign(L1_CACHE_LINE_SIZE, size);
	#endif
	}

	void FreeAligned(void *ptr)
	{
		if (!ptr) return;
	#if defined(IS_WINDOWS)
		_aligned_free(ptr);
	#elif defined (IS_OPENBSD) || defined(IS_APPLE)
		free(((void**)ptr)[-1]);
	#else
		free(ptr);
	#endif
	}
}