/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Intersection.h"
#include "Primitive.h"
#include "Light.h"

namespace Render
{
	Intersection::Intersection()
		:	prim(NULL), primitiveData(vector<float>(2, 0.f))
	{
	}

	BSDF *Intersection::GetBSDF(const Ray &ray, MemoryArena &arena)
	{
		shape->GetDifferentialGeometry(ray, this);
		return prim->GetBSDF(this, arena);
	}

	bool Intersection::Le(const Vector &w, Spectrum &s) const
	{
		auto arealight = prim->GetAreaLight();
		if(arealight)
			return arealight->L(w, dg, ng, s);

		return false;
	}
}