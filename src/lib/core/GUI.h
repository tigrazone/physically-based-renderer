/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_GUI_H
#define RENDERLIB_CORE_GUI_H

#include <GL/glew.h>
#include "Algebra.h"

#define BORDERPADDING 4

#if defined(IS_WINDOWS)
#	include <GL/wglew.h>
#	include <Windows.h>
#	include <Commdlg.h>
#	include <windowsx.h>
#	include <direct.h>
#	define getcwd _getcwd
#	define chdir _chdir
	typedef HRESULT Response_t;
	typedef UINT Message_t;
#else
#	include <unistd.h>
	typedef long Response_t;
	typedef uint32_t Message_t;
#endif

namespace Render
{
	enum Event
	{
		INIT = 0,
		CREATE = INIT + 1,
		CLOSE = CREATE + 1,
		DESTROY = CLOSE + 1,
		HITTEST = DESTROY + 1,
		SIZE = HITTEST + 1,
		MINIMIZE = SIZE + 1,
		MAXIMIZE = MINIMIZE + 1,
		RESTORE = MAXIMIZE + 1,
		LMOUSEDOWN = RESTORE + 1,
		MMOUSEDOWN = LMOUSEDOWN + 1,
		RMOUSEDOWN = MMOUSEDOWN + 1,
		LMOUSEUP = RMOUSEDOWN + 1,
		MMOUSEUP = LMOUSEUP + 1,
		RMOUSEUP = MMOUSEUP + 1,
		MOUSESCROLL = RMOUSEUP + 1,
		KEYDOWN = MOUSESCROLL + 1,
		KEYUP = KEYDOWN + 1,
		DRAW = KEYUP + 1,
		MOVE = DRAW + 1,
		USER = MOVE + 1,
		CUSTOM = USER,//Must always be second last!
		N_EVENTS = CUSTOM + 1
	};

	//Everything is still in order.  Increments that are greater than 1 are used by the hit test handler as a relative id (for purposes of resizing for example).
	enum EventResponse
	{
		ERR = 0,
		OK = ERR + 1,
		HITLEFT = OK + 1,
		HITRIGHT = HITLEFT + 1,
		HITTOPLEFT = HITLEFT + 2,
		HITTOPRIGHT = HITRIGHT + 2,
		HITBOTTOMLEFT = HITLEFT + 4,
		HITBOTTOMRIGHT = HITRIGHT + 4,
		HITMOVE = HITBOTTOMRIGHT + 1,
		HITMIN = HITMOVE + 1,
		HITTOP = HITMOVE + 2,
		HITMAX = HITTOP + 1,
		HITBOTTOM = HITMOVE + 4,
		HITCLOSE = HITBOTTOM + 1,
		HITXSCROLL = HITCLOSE + 1,
		HITYSCROLL = HITXSCROLL + 1,
		N_RESPONSES = HITYSCROLL + 1
	};

//---------------------------------------------------------------------------------------------------------

	struct SizeData;
	struct ScreenPoint;

	class EventListener
	{
	public:
		virtual ~EventListener();
		virtual bool operator()(void *data, EventResponse *ret) const = 0;
	};

	template<unsigned int eventID, class GUIType, typename Function>
	class BaseEventListener : public EventListener
	{
	protected:
		typedef Function Handler;
		GUIType *obj;
		Function f;
	public:
		static const unsigned int eventID = eventID;
		BaseEventListener(GUIType *obj, Function f) : obj(obj), f(f){}
		inline bool Matches(GUIType *o, Function otherFunction) const { return obj == o && f == otherFunction; }
	};

	//For events that don't require parameters.  Can be useful for various callback functions.
	template<unsigned int customEventID, class GUIType>
	class SimpleCustomEventListener : public BaseEventListener<Event::CUSTOM + customEventID, GUIType, bool (GUIType::*)()>
	{
	protected:
		SimpleCustomEventListener(GUIType *obj, Handler f) : BaseEventListener(obj, f){}
	public:
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(); }
	};

	template<unsigned int customEventID, class GUIType, typename DataType>
	class CustomEventListener : public BaseEventListener<Event::CUSTOM + customEventID, GUIType, bool (GUIType::*)(DataType data)>
	{
	protected:
		CustomEventListener(GUIType *obj, Handler f) : BaseEventListener(obj, f){}
	public:
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(static_cast<DataType>(data)); }
	};

//---------------------------------------------------------------------------------------------------------

	template<Event type, class GUIType>
	class GenericListener : public BaseEventListener<type, GUIType, bool (GUIType::*)()>
	{
	public:
		GenericListener(GUIType *obj, Handler f):BaseEventListener(obj, f){}
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(); }
	};

//---------------------------------------------------------------------------------------------------------

	class GUIEventBinder
	{
		static size_t customEventID;
		typedef vector<const EventListener *> EventPool;
		static const Response_t responseMap[N_RESPONSES];
		EventPool eventPool[N_EVENTS - 1];
		vector<EventPool> customEventPool;

		inline EventPool *GetPool(unsigned int eventID)
		{
			if(eventID >= Event::CUSTOM)
			{
				eventID -= Event::CUSTOM;
				if(eventID >= customEventPool.size())
					return NULL;

				return &customEventPool[eventID];
			}
			else
				return &eventPool[eventID];
		}

		inline const EventPool *GetPool(unsigned int eventID) const
		{
			if(eventID >= Event::CUSTOM)
			{
				eventID -= Event::CUSTOM;
				if(eventID >= customEventPool.size())
					return NULL;

				return &customEventPool[eventID];
			}
			else
				return &eventPool[eventID];
		}

	public:
		GUIEventBinder(unsigned int nCustomEvents = 0);
		~GUIEventBinder();

		template<typename Listener, class GUIType, typename Function>
		inline void Bind(GUIType *obj, Function f)
		{
			if(Listener::eventID >= Event::CUSTOM)
			{
				unsigned int eventID = Listener::eventID - Event::CUSTOM;
				if(eventID >= customEventPool.size())
					customEventPool.resize(eventID + 1);

				customEventPool[eventID].push_back(new Listener(obj, f));
			}
			else
				eventPool[Listener::eventID].push_back(new Listener(obj, f));
		}

		template<typename Listener, class GUIType, typename Function>
		inline void Unbind(GUIType *obj, Function f)
		{
			EventPool *pool = GetPool(Listener::eventID);
			if(!pool)
				return;

			for(EventPool::iterator i = pool->begin(); i != pool->end(); ++i)
				if((*i)->Matches(obj, f))
				{
					delete (*i);
					(*i) = NULL;
				}

			pool->resize(remove(pool->begin(), pool->end(), (const EventListener *) NULL) - pool->begin());
		}

		void ExpandCustomEventPool(size_t n);
		Response_t Signal(unsigned int ev, void *data = NULL) const;
	};

//---------------------------------------------------------------------------------------------------------

	class GUIObj
	{
	protected:
		GUIEventBinder *evb;
	public:
		GUIObj(GUIEventBinder *evb);
		virtual ~GUIObj();
		virtual void Draw() = 0;
	};

//---------------------------------------------------------------------------------------------------------

	/* It is important to note that GUIGLContext, GUIWindow, and signal classes have
	 * various platform dependant implementations.  They should serve as a black box
	 * for other classes, and communicate using event binders and associated event
	 * listeners.
	 */

	struct GUIGLContext
	{
		const bool ownsContext;
		
		GUIGLContext();
		~GUIGLContext();
		const GUIGLContext &Acquire() const;
		void Swap() const;
#if defined(IS_WINDOWS)
		GUIGLContext(HDC hdc, GUIGLContext &ctx);
		GUIGLContext(HWND hwnd);
	private:
		HDC hdc;
		HGLRC hglrc;
#endif
		//This object is tied closely to windows.  So arbitrary copying is a no-no.
		explicit GUIGLContext(const GUIGLContext &ctx);
		GUIGLContext &operator=(const GUIGLContext &ctx);
	};

//---------------------------------------------------------------------------------------------------------

	class GUIWindow : public GUIObj
	{
	public:
		enum State
		{
			NORMAL = 0,
			MINIMIZED = 1,
			MAXIMIZED = MINIMIZED << 1,
			FULLSCREEN = MAXIMIZED << 1
		};

		bool mouseDown[3];
	private:
#if defined(IS_WINDOWS)
		virtual LRESULT proc(UINT msg, WPARAM wp, LPARAM lp);
		HWND hwnd;
#endif
	protected:
		string title;
		GUIWindow *parent;
		GUIGLContext ctx;
		State windowState;

		bool stop;
	public:
#if defined(IS_WINDOWS)
		struct WindowParams
		{
			WPARAM wp;
			LPARAM lp;
		};
		static LRESULT CALLBACK Proc(HWND hwnd, UINT m, WPARAM w, LPARAM l);
#endif
		GUIWindow(const string &title, State windowState, GUIWindow *parent = NULL);
		virtual ~GUIWindow();

		bool GUIWindowCreate();
		bool GUIWindowRestore();
		bool GUIWindowMinimize();
		bool GUIWindowMaximize();
		bool GUIWindowSize(const SizeData *data);
		bool GUIWindowMove(const ScreenPoint *data);
		bool GUIWindowHitTest(const ScreenPoint *data, EventResponse *response);

		virtual void Minimize();
		virtual void FullScreen();
		virtual void Maximize();
		virtual void Normal();

		virtual bool GetScreenSize(int xpos, int ypos, int *pixelsX, int *pixelsY, int *x = NULL, int *y = NULL, bool getWorkArea = false) const;
		virtual void GetCursorPos(ScreenPoint *p) const;
		virtual void GetWindowSize(int *width, int *height) const;

		bool GetFile(const char *extensions, string &filename, const char *title = NULL) const;
		bool SaveFile(const char *extensions, string &filename, const char *title = NULL) const;

		virtual void Run();

		friend class WindowSignal;
	protected: 
		int posX, posY, pixelsX, pixelsY, currentPixelsX, currentPixelsY;
	};

//---------------------------------------------------------------------------------------------------------

	/* Given an event and relevant data, enqueues a message to a given window.
	 */
	class WindowSignal
	{
	protected:
		static Message_t eventMap[Event::N_EVENTS];
		GUIWindow::WindowParams params;
		Message_t ev;
		const GUIWindow *window;
	public:
		WindowSignal(const GUIWindow *window, Message_t ev);
		virtual void Signal() const;
	};

//---------------------------------------------------------------------------------------------------------

	class GenericEventSignal : public WindowSignal
	{
	public:
		GenericEventSignal(const GUIWindow *window, Event ev);
	};

	class UserEventSignal : public WindowSignal
	{
	public:
		UserEventSignal(const GUIWindow *window);
		void SetParams(unsigned int ev, void *);
	};

//---------------------------------------------------------------------------------------------------------

	struct SizeData
	{
		GUIWindow::State state;
		int posx, posy, nx, ny;
	};
	
	template<class GUIType>
	class SizeListener : public BaseEventListener<Event::SIZE, GUIType, bool (GUIType::*)(const SizeData *data)>
	{
	public:
		SizeListener(GUIType *obj, Handler f): BaseEventListener(obj, f){}
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(static_cast<const SizeData *>(data)); }
	};

//---------------------------------------------------------------------------------------------------------

	struct ScreenPoint
	{
		int x, y;
	};

	template<class GUIType>
	class HitTestListener : public BaseEventListener<Event::HITTEST, GUIType, bool (GUIType::*)(const ScreenPoint *data, EventResponse *ret)>
	{
	public:
		HitTestListener(GUIType *obj, Handler f) : BaseEventListener(obj, f){}
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(static_cast<const ScreenPoint *>(data), ret); }
	};

	template<int eventID, class GUIType>
	class MouseEventListener : public BaseEventListener<eventID, GUIType, bool (GUIType::*)(const ScreenPoint *data, EventResponse *ret)>
	{
	public:
		MouseEventListener(GUIType *obj, Handler f) : BaseEventListener(obj, f){}
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(static_cast<const ScreenPoint *>(data), ret); }
	};

	template<class GUIType>
	class MoveListener : public BaseEventListener<Event::MOVE, GUIType, bool (GUIType::*)(const ScreenPoint *data)>
	{
	public:
		MoveListener(GUIType *obj, Handler f): BaseEventListener(obj, f){}
		inline bool operator()(void *data, EventResponse *ret) const { return ((*obj).*f)(static_cast<const ScreenPoint *>(data)); }
	};

//---------------------------------------------------------------------------------------------------------
}
#endif