/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_LIGHT_H
#define RENDERLIB_CORE_LIGHT_H

#include "Primitive.h"
#include "Spectrum.h"
#include "Memory.h"
#include "MonteCarlo.h"
#include "BxDF.h"

namespace Render
{
	class Light : public ReferenceCounted
	{
	public:
		virtual ~Light();
		virtual bool IsDelta() const = 0;
		virtual bool IsEnvironment() const; //That is to say, contributes with Le.
		virtual float Power(const Scene *scene, const Transform *t) const = 0;
		virtual void Le(const Transform *t, const Ray &ray, Spectrum &s) const;
		virtual bool Le(const Transform *t, const Scene *scene, const Ray &ray, Spectrum &s, float *pdfDirect, float *pdfEmission = NULL, Normal *ng = NULL) const;
		virtual bool EstimateDirect(const Renderer *renderer, const Scene *scene,
									Sample *sample, uint32_t offset, 
									const BSDF *bsdf, BxDFType flags, const Transform &transform,
									const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena) const = 0;
		virtual bool SampleL(const Transform *t, const Point &p, Sample *sample, uint32_t offset, Vector *wi, Spectrum &s, float *pdf, Intersection *isect, Ray *visRay = NULL) const = 0;
		virtual bool SampleL(const Transform *t, const Scene *scene, Sample *sample, uint32_t offset, Ray *ray, Intersection *isect, Spectrum &s, float *pdf) const = 0;
		//Returns the pdf for direct lighting.  Delta lights will automatically return 1 so integrators should make note of that.
		virtual float Pdf(const Transform *t, const Point &p, const Vector &wi) const = 0;
		//Returns the pdf when sampling outgoing radiance from the light.  Generally assumes that p and wi are valid because this is likely used for MIS.
		virtual float Pdf(const Transform *t, const Scene *scene, const Point &p, const Vector &wi) const = 0;
	};

	class DeltaLight : public Light
	{
	public:
		bool IsDelta() const;
		bool EstimateDirect(const Renderer *renderer, const Scene *scene,
							Sample *sample, uint32_t offset, 
							const BSDF *bsdf, BxDFType flags, const Transform &transform,
							const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena) const;
		virtual float Pdf(const Transform *t, const Point &p, const Vector &wi) const;
	};

	class SampleLight : public Light
	{
	public:
		bool IsDelta() const;
		bool EstimateDirect(const Renderer *renderer, const Scene *scene,
							Sample *sample, uint32_t offset, 
							const BSDF *bsdf, BxDFType flags, const Transform &transform,
							const Point &p, Ray &ray, Intersection &isect, Spectrum &s, MemoryArena &arena) const;
	};

	class AreaLight : public SampleLight
	{
	public:
		virtual bool L(const Vector &w, const DifferentialGeometry &dg, const Normal &ng, Spectrum &s) const = 0;
	};

	class ShapeSet
	{
		Distribution1D *distribution1D;
		Reference<Primitive> accel;
		vector<const Shape *> shapes;
		float totalArea, invTotalArea;
	public:
		ShapeSet(const Reference<Shape> &shape, const Reference<Primitive> &accel);
		~ShapeSet();
		Point Sample(float u[3], Intersection *isect = NULL) const;
		float Pdf(const Point &p, const Vector &wi, Intersection *isect, const Transform *toWorld) const;
		float Pdf(const Point &p, const Transform *t) const;
		float Area() const;
	};

	struct TransformedLight
	{
		Transform toWorld;
		const Light *light;
		TransformedLight(const Light *light) : light(light){}
	};

	class LightCollection : public ReferenceCounted
	{
	public:
		virtual void setLight(TransformedLight **lightInstances) const = 0;
		virtual void lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld = NULL) const = 0;
		virtual ~LightCollection();
		virtual unsigned int Size() const = 0;
		TransformedLight *GetLightInstances() const;
		void ToWorld(TransformedLight *lights, float time) const;
	};

	class LightSet : public LightCollection
	{
		vector<Reference<Light>> lights;
		void setLight(TransformedLight **lightInstances) const;
		void lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld = NULL) const;
	public: 
		LightSet(const vector<Reference<Light>> &lights);
		unsigned int Size() const;
	};

	class LightAggregate : public LightCollection
	{
	protected:
		vector<Reference<LightCollection>> nodes;
		unsigned int size;
		void setLight(TransformedLight **lightInstances) const;
		virtual void lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld = NULL) const;
	public:
		LightAggregate(const vector<Reference<LightCollection>> &nodes);
		unsigned int Size() const;
	};

	class TransformedLightCollection : public LightAggregate
	{
		Transform toWorld;
		void lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld = NULL) const;
	public:
		TransformedLightCollection(const vector<Reference<LightCollection>> &nodes, const Transform &toWorld);
	};

	class AnimatedLightCollection : public LightAggregate
	{
		AnimatedTransform toWorld;
		void lightsToWorld(TransformedLight **lights, float time, const Transform *toWorld = NULL) const;
	public:
		AnimatedLightCollection(const vector<Reference<LightCollection>> &nodes, const AnimatedTransform &toWorld);
	};
}

#endif