/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Parallel.h"
#include <boost/thread.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>

namespace Render
{
	Task::~Task()
	{
	}

	unsigned int NumThreads()
	{
		return boost::thread::hardware_concurrency();
	}

	class ThreadPoolCallable
	{
		const unsigned int id;
		ThreadPool *pool;
	public:
		ThreadPoolCallable(ThreadPool *pool, unsigned int id)
			:	pool(pool), id(id){}

		void operator()()
		{
			while(true)
			{
				pool->taskWait->wait();
				Task *t;
				{
					boost::lock_guard<boost::mutex> lock(*pool->queueLock);

					if(pool->tasks.size() == 0)
						break;

					t = pool->tasks.back();
					pool->tasks.pop_back();
				}

				t->Run(id);

				{
					boost::unique_lock<boost::mutex> lock(*pool->tasksRunning);
					--pool->unfinishedTasks;
					if(pool->unfinishedTasks == 0)
						pool->tasksWaiting->notify_all();
				}
			}
		}
	};

	ThreadPool::ThreadPool(unsigned int nThreads)
		:	nThreads(nThreads == 0 ? NumThreads() : nThreads), threads(NULL), taskWait(NULL), queueLock(NULL), tasksRunning(NULL), tasksWaiting(NULL), unfinishedTasks(0)
	{
		if(this->nThreads == 1)
			return;

		taskWait = (boost::interprocess::interprocess_semaphore *) arena.Alloc(sizeof(boost::interprocess::interprocess_semaphore));
		new (taskWait) boost::interprocess::interprocess_semaphore(0);
		queueLock = arena.Alloc<boost::mutex>();
		tasksRunning = arena.Alloc<boost::mutex>();
		tasksWaiting = arena.Alloc<boost::condition_variable>();

		threads = (boost::thread *) arena.Alloc(sizeof(boost::thread) * this->nThreads);
		for(unsigned int i = 0; i < this->nThreads; ++i)
			new (&threads[i]) boost::thread(ThreadPoolCallable(this, i));
	}

	ThreadPool::~ThreadPool()
	{
		if(threads == NULL)
			return;

		{
			boost::lock_guard<boost::mutex> lock(*queueLock);
			tasks.clear();
		}

		for(unsigned int i = 0; i < nThreads; ++i)
			taskWait->post();

		for(unsigned int i = 0; i < nThreads; ++i)
		{
			if(threads[i].joinable()) threads[i].join();
			threads[i].~thread();
		}

		{
			boost::unique_lock<boost::mutex>(*tasksRunning);
			tasksWaiting->notify_all();
		}

		taskWait->~interprocess_semaphore();
		queueLock->~mutex();
		tasksRunning->~mutex();
		tasksWaiting->~condition_variable();
	}

	void ThreadPool::Enqueue(vector<Task *> &tasks)
	{
		if(!threads)
		{
			for(vector<Task *>::iterator i = tasks.begin(); i != tasks.end(); ++i)
				(*i)->Run(0);
			return;
		}

		{
			boost::unique_lock<boost::mutex> lock(*tasksRunning);
			unfinishedTasks += tasks.size();
		}

		{
			boost::lock_guard<boost::mutex> lock(*queueLock);
			for(vector<Task *>::iterator i = tasks.begin(); i != tasks.end(); ++i)
			{
				this->tasks.push_back(*i);
				taskWait->post();
			}
		}
	}

	void ThreadPool::WaitForAllThreads()
	{
		if(!threads)
			return;

		boost::unique_lock<boost::mutex> lock(*tasksRunning);

		while(unfinishedTasks)
			tasksWaiting->wait(lock);
	}
	
	class TaskSetCallable
	{
		const unsigned int id;
		TaskSet *taskSet;
	public:
		TaskSetCallable(TaskSet *taskSet, unsigned int id)
			:	taskSet(taskSet), id(id){}

		void operator()()
		{
			Task *task;
			while(true)
			{
				{
					boost::unique_lock<boost::mutex> lock(*taskSet->taskWait);
					if(taskSet->taskIndex >= taskSet->nTasks)
						do
						{
							taskSet->hasTask->wait(lock);
						} while(taskSet->taskIndex >= taskSet->nTasks);

					if(taskSet->nUnfinished == 0)
					{
						taskSet->tasksWaiting->notify_all();
						break;
					}

					task = taskSet->tasks[taskSet->taskIndex++];
				}
				task->Run(id);
				{
					boost::unique_lock<boost::mutex> lock(*taskSet->tasksRunning);
					--taskSet->nUnfinished;
					if(taskSet->nUnfinished == 0)
						taskSet->tasksWaiting->notify_all();
				}
			}
		}
	};

	TaskSet::TaskSet(Task **tasks, unsigned int nTasks, unsigned int nThreads)
		:	nThreads(nThreads == 0 ? NumThreads() : nThreads), tasks(tasks), nTasks(nTasks), taskIndex(nTasks), nUnfinished(0)
	{
		if(this->nThreads == 0)
			return;

		taskWait = new boost::mutex;
		tasksRunning = new boost::mutex;
		hasTask = new boost::condition_variable;
		tasksWaiting = new boost::condition_variable;

		threads = new boost::thread[this->nThreads];
		for(unsigned int i = 0; i < this->nThreads; ++i)
			threads[i] = boost::thread(TaskSetCallable(this, i));
	}
	TaskSet::~TaskSet()
	{
		if(this->nThreads == 0)
			return;

		{
			boost::unique_lock<boost::mutex> lock(*taskWait);
			taskIndex = 0;
			nUnfinished = 0;
			hasTask->notify_all();
		}

		for(unsigned int i = 0; i < nThreads; ++i)
			if(threads[i].joinable()) threads[i].join();

		delete[] threads;

		delete taskWait;
		delete tasksRunning;
		delete hasTask;
		delete tasksWaiting;
	}
	void TaskSet::Work()
	{
		if(threads == NULL)
		{
			for(unsigned int i = 0; i < nTasks; ++i)
				tasks[i]->Run(0);

			return;
		}

		WaitForAllThreads();

		{
			boost::unique_lock<boost::mutex> lock(*taskWait);
			taskIndex = 0;
			nUnfinished = nTasks;
			hasTask->notify_all();
		}
	}
	void TaskSet::WaitForAllThreads()
	{
		if(!threads)
			return;

		boost::unique_lock<boost::mutex> lock(*tasksRunning);
		while(nUnfinished)
			tasksWaiting->wait(lock);
	}
}