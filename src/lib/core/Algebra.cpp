/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Algebra.h"

namespace Render
{
#ifdef _DEBUG
	ostream& operator<<(ostream &c, const Point &p)
	{
		c << p.x << " " << p.y << " " << p.z;
		return c;
	}
	ostream& operator<<(ostream &c, const Vector &v)
	{
		c << v.x << " " << v.y << " " << v.z;
		return c;
	}

	ostream& operator<<(ostream &c, const Normal &n)
	{
		c << n.x << " " << n.y << " " << n.z;
		return c;
	}

	ostream& operator<<(ostream &c, const Mat4 &m)
	{ 
		for(unsigned int i = 0; i < 4; ++i)
		{
			for(unsigned int j = 0; j < 4; ++j)
			{
				c << m[i][j];
				c << " ";
			}
			c << endl;
		}
		return c;
	}
#endif

	const Mat4 Mat4::identity
	(
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	);

	BBox Union(const BBox &b, const Point &p)
	{
		BBox ret = b;
		ret.pMin.x = min(b.pMin.x, p.x);
		ret.pMin.y = min(b.pMin.y, p.y);
		ret.pMin.z = min(b.pMin.z, p.z);
		ret.pMax.x = max(b.pMax.x, p.x);
		ret.pMax.y = max(b.pMax.y, p.y);
		ret.pMax.z = max(b.pMax.z, p.z);
		return ret;
	}

	BBox Union(const BBox &b1, const BBox &b2)
	{
		BBox ret;
		ret.pMin.x = min(b1.pMin.x, b2.pMin.x);
		ret.pMin.y = min(b1.pMin.y, b2.pMin.y);
		ret.pMin.z = min(b1.pMin.z, b2.pMin.z);
		ret.pMax.x = max(b1.pMax.x, b2.pMax.x);
		ret.pMax.y = max(b1.pMax.y, b2.pMax.y);
		ret.pMax.z = max(b1.pMax.z, b2.pMax.z);
		return ret;
	}

	// Code for inversion taken from:
	// http://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
	void Mat4::Inverse(Mat4 &m) const
	{
		float det;

		m.mm[0] =	 mm[5]  * mm[10] * mm[15] -
					 mm[5]  * mm[11] * mm[14] -
					 mm[9]  * mm[6]  * mm[15] +
					 mm[9]  * mm[7]  * mm[14] +
					 mm[13] * mm[6]  * mm[11] -
					 mm[13] * mm[7]  * mm[10];

		m.mm[4] =	-mm[4]  * mm[10] * mm[15] +
					 mm[4]  * mm[11] * mm[14] +
					 mm[8]  * mm[6]  * mm[15] -
					 mm[8]  * mm[7]  * mm[14] -
					 mm[12] * mm[6]  * mm[11] +
					 mm[12] * mm[7]  * mm[10];

		m.mm[8] =	 mm[4]  * mm[9]  * mm[15] -
					 mm[4]  * mm[11] * mm[13] -
					 mm[8]  * mm[5]  * mm[15] +
					 mm[8]  * mm[7]  * mm[13] +
					 mm[12] * mm[5]  * mm[11] -
					 mm[12] * mm[7]  * mm[9];

		m.mm[12] =	-mm[4]  * mm[9]  * mm[14] +
					 mm[4]  * mm[10] * mm[13] +
					 mm[8]  * mm[5]  * mm[14] -
					 mm[8]  * mm[6]  * mm[13] -
					 mm[12] * mm[5]  * mm[10] +
					 mm[12] * mm[6]  * mm[9];

		m.mm[1] =	-mm[1]  * mm[10] * mm[15] +
					 mm[1]  * mm[11] * mm[14] +
					 mm[9]  * mm[2]  * mm[15] -
					 mm[9]  * mm[3]  * mm[14] -
					 mm[13] * mm[2]  * mm[11] +
					 mm[13] * mm[3]  * mm[10];

		m.mm[5] =	 mm[0]  * mm[10] * mm[15] -
					 mm[0]  * mm[11] * mm[14] -
					 mm[8]  * mm[2]  * mm[15] +
					 mm[8]  * mm[3]  * mm[14] +
					 mm[12] * mm[2]  * mm[11] -
					 mm[12] * mm[3]  * mm[10];

		m.mm[9] =	-mm[0]  * mm[9]  * mm[15] +
					 mm[0]  * mm[11] * mm[13] +
					 mm[8]  * mm[1]  * mm[15] -
					 mm[8]  * mm[3]  * mm[13] -
					 mm[12] * mm[1]  * mm[11] +
					 mm[12] * mm[3]  * mm[9];

		m.mm[13] =	 mm[0]  * mm[9]  * mm[14] -
					 mm[0]  * mm[10] * mm[13] -
					 mm[8]  * mm[1]  * mm[14] +
					 mm[8]  * mm[2]  * mm[13] +
					 mm[12] * mm[1]  * mm[10] -
					 mm[12] * mm[2]  * mm[9];

		m.mm[2] =	 mm[1]  * mm[6] * mm[15] -
					 mm[1]  * mm[7] * mm[14] -
					 mm[5]  * mm[2] * mm[15] +
					 mm[5]  * mm[3] * mm[14] +
					 mm[13] * mm[2] * mm[7]  -
					 mm[13] * mm[3] * mm[6];

		m.mm[6] =	-mm[0]  * mm[6] * mm[15] +
					 mm[0]  * mm[7] * mm[14] +
					 mm[4]  * mm[2] * mm[15] -
					 mm[4]  * mm[3] * mm[14] -
					 mm[12] * mm[2] * mm[7]  +
					 mm[12] * mm[3] * mm[6];

		m.mm[10] =	 mm[0]  * mm[5] * mm[15] -
					 mm[0]  * mm[7] * mm[13] -
					 mm[4]  * mm[1] * mm[15] +
					 mm[4]  * mm[3] * mm[13] +
					 mm[12] * mm[1] * mm[7]  -
					 mm[12] * mm[3] * mm[5];

		m.mm[14] =	-mm[0]  * mm[5] * mm[14] +
					 mm[0]  * mm[6] * mm[13] +
					 mm[4]  * mm[1] * mm[14] -
					 mm[4]  * mm[2] * mm[13] -
					 mm[12] * mm[1] * mm[6]  +
					 mm[12] * mm[2] * mm[5];

		m.mm[3] =	-mm[1] * mm[6] * mm[11] +
					 mm[1] * mm[7] * mm[10] +
					 mm[5] * mm[2] * mm[11] -
					 mm[5] * mm[3] * mm[10] -
					 mm[9] * mm[2] * mm[7]  +
					 mm[9] * mm[3] * mm[6];

		m.mm[7] =	 mm[0] * mm[6] * mm[11] -
					 mm[0] * mm[7] * mm[10] -
					 mm[4] * mm[2] * mm[11] +
					 mm[4] * mm[3] * mm[10] +
					 mm[8] * mm[2] * mm[7]  -
					 mm[8] * mm[3] * mm[6];

		m.mm[11] =	-mm[0] * mm[5] * mm[11] +
					 mm[0] * mm[7] * mm[9]  +
					 mm[4] * mm[1] * mm[11] -
					 mm[4] * mm[3] * mm[9]  -
					 mm[8] * mm[1] * mm[7]  +
					 mm[8] * mm[3] * mm[5];

		m.mm[15] =	 mm[0] * mm[5] * mm[10] -
					 mm[0] * mm[6] * mm[9]  -
					 mm[4] * mm[1] * mm[10] +
					 mm[4] * mm[2] * mm[9]  +
					 mm[8] * mm[1] * mm[6]  -
					 mm[8] * mm[2] * mm[5];

		det = mm[0] * m.mm[0] + mm[1] * m.mm[4] + mm[2] * m.mm[8] + mm[3] * m.mm[12];

		if(det == 0)
		{
			memcpy(m.mm, identity.mm, sizeof(float) * 16);
			return;
		}

		det = 1.f / det;

		for (int i = 0; i < 16; ++i)
			m.mm[i] *= det;
	}

	Mat4 Mat4::Inverse() const
	{
		Mat4 inv;
		Inverse(inv);
		return inv;
	}

	void Mat4::operator()(const BBox &b, BBox *bTrans) const
	{
		for(int i = 0; i < 3; ++i)
		{
			bTrans->pMin[i] = bTrans->pMax[i] = m[i][3];
			for(int j = 0; j < 3; ++j)
			{
				float pa = m[i][j] * b.pMin[j];
				float pb = m[i][j] * b.pMax[j];
				bTrans->pMin[i] += min(pa, pb);
				bTrans->pMax[i] += max(pa, pb);
			}
		}
	}

	bool BBox::IntersectP(const Ray &ray, float *hitt0, float *hitt1) const
	{
		float t0 = ray.mint; float t1 = ray.maxt;

		for(int i = 0; i < 3; ++i)
		{
			float invRayDir = 1.f / ray.d[i];
			float tNear = (pMin[i] - ray.o[i]) * invRayDir;
			float tFar = (pMax[i] - ray.o[i]) * invRayDir;

			if(tNear > tFar) swap(tNear, tFar);

			t0 = tNear > t0 ? tNear : t0;
			t1 = tFar < t1 ? tFar : t1;
			if(t0 > t1) return false;
		}
		if(hitt0) *hitt0 = t0;
		if(hitt1) *hitt1 = t1;
		return true;
	}

}