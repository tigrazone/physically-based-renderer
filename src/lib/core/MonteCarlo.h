/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_MONTECARLO_H
#define RENDERLIB_CORE_MONTECARLO_H

#include "RNG.h"
#include "Algebra.h"

namespace Render
{
	struct Distribution1D
	{
		Distribution1D(const float *func, int count)
			:	count(count), func(new float[count]), invCount(1.f / float(count))
		{
			memcpy(this->func, func, sizeof(float)*count);

			cdf = new float[count + 1];
			cdf[0] = 0.f;

			for(int i = 0; i < count; ++i)
				cdf[i+1] = cdf[i] + func[i] * invCount;

			funcInt = cdf[count];
			if(funcInt == 0.f)
			{
				funcInt = 1.f;
				invFuncInt = 1.f;
				invTotal = invCount;
				for(int i = 1; i < count + 1; ++i)
					cdf[i] *= invTotal;
			}
			else
			{
				invFuncInt = 1.f / funcInt;
				for (int i = 1; i < count + 1; ++i)
					cdf[i] *= invFuncInt;

				invTotal = 1.f / (funcInt * float(count));
			}
		}

		//Map a random variable u to the piecewise constant function in 1D.
		float SampleContinuous(float u, float *pdf, int *off = NULL) const
		{
			//Search for the offset.
			float *ptr = upper_bound(cdf, cdf + count + 1, u);
			int offset = Clamp(int(ptr - cdf - 1), 0, count - 1);
			if(off)
				*off = offset;
			//Compute the normalized distance from u to cdf[offset]
			float du = (u - cdf[offset]) / (cdf[offset + 1] - cdf[offset]);

			if(pdf) *pdf = func[offset] * invFuncInt;
			return ((float) offset + du) * invCount;
		}

		size_t SampleDiscrete(float u, float *pdf) const
		{
			float *ptr = upper_bound(cdf, cdf + count + 1, u);
			int offset = Clamp(int(ptr - cdf - 1), 0, count - 1);
			if(pdf) *pdf = func[offset] * invTotal;
			return offset;
		}

		~Distribution1D()
		{
			delete[] func;
			delete[] cdf;
		}
	private:
		friend struct Distribution2D;
		float *func, *cdf;
		float funcInt;
		int count;
		float invTotal;//For the discrete version
		float invCount;
		float invFuncInt;
	};

	struct Distribution2D
	{
		Distribution2D(const float *func, int nu, int nv);
		~Distribution2D();

		void SampleContinuous(float u0, float u1, float uv[2], float *pdf) const
		{
			float pdfs[2];
			int v;
			uv[1] = pMarginal->SampleContinuous(u1, &pdfs[1], &v);
			uv[0] = pConditionalV[v]->SampleContinuous(u0, &pdfs[0]);
			if(pdf)
				*pdf = pdfs[0] * pdfs[1];
		}

		float Pdf(float u, float v) const
		{
			int iu = Clamp(int(u * pConditionalV[0]->count), 0, pConditionalV[0]->count - 1);
			int iv = Clamp(int(v * pMarginal->count), 0, pMarginal->count - 1);

			if(pMarginal->funcInt == 0.f || pConditionalV[iv]->funcInt == 0.f) return 0.f;

			return pConditionalV[iv]->func[iu] * invMarginalFuncInt;
		}
	private:
		vector<Distribution1D *> pConditionalV;
		Distribution1D *pMarginal;
		float invMarginalFuncInt;
	};

	void StratifiedSample1D(float *sample, unsigned int nSamples, RNG &rng, bool jitter = true);
	void StratifiedSample2D(float *sample, unsigned int nx, unsigned int ny, RNG &rng, bool jitter = true);

	template <typename T>
	void Shuffle(T *samp, uint32_t count, uint32_t dims, RNG &rng)
	{
		for (uint32_t i = 0; i < count; ++i) 
		{
			uint32_t other = i + (rng.Rand() % (count - i));
			for (uint32_t j = 0; j < dims; ++j)
				swap(samp[dims*i + j], samp[dims*other + j]);
		}
	}

	inline void GeneratePermutation(uint32_t *buf, uint32_t b, RNG &rng)
	{
		for(uint32_t i = 0; i < b; ++i)
			buf[i] = i;
		Shuffle(buf, b, 1, rng);
	}

	void RejectionSampleDisc(float *x, float *y, RNG &rng);

	void ConcentricSampleDisc(float u1, float u2, float *dx, float *dy);

	//Given two uniform random numbers, generate uniform random samples on a hemisphere w.r.t. solid angle.
	Vector UniformSampleHemisphere(float u1, float u2);
#	define UniformHemispherePDF INV_TWOPI

	Vector UniformSampleSphere(float u1, float u2);
#	define UniformSpherePDF 0.07957747154594766788444188168626f

	void UniformSampleDisc(float u1, float u2, float *x, float *y);
#	define UniformDiscPDF INV_PI

	inline Vector CosineSampleHemisphere(float u1, float u2)
	{
		Vector ret;
		ConcentricSampleDisc(u1, u2, &ret.x, &ret.y);
		ret.z = ::sqrt(max(0.f, 1.f - ret.x * ret.x - ret.y * ret.y));
		return ret;
	}

	inline float CosineHemispherePdf(float cosTheta)
	{
		return cosTheta * INV_PI;
	}

	void UniformSampleTriangle(float u1, float u2, float *u, float *v);

	Vector UniformSampleCone(float u1, float u2, float costhetamax);
	inline float UniformConePdf(float cosThetaMax)
	{
		return 1.f / (TWOPI * (1.f - cosThetaMax));
	}

	inline float BalanceHeuristic(int nf, float pf, int ng, float pg)
	{
		float f = nf * pf;
		float g = ng * pg;
		return f / (f + g);
	}

	inline float PowerHeuristic(int nf, float pf, int ng, float pg)
	{
		float ff = nf * pf;
		ff *= ff;

		float gg = ng * pg;
		gg *= gg;

		return ff / (ff + gg);
	}
}
#endif
