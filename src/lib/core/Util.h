/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_UTIL_H
#define RENDERLIB_CORE_UTIL_H

#include <string>
#include <iostream>
#include <sstream>

using namespace std;

namespace Render
{
	void Severe(const char *str, const char *cName, const char *mName, int code, ...);

	void Error(const char *str, const char *cName, const char *mName, int code, ...);

	void Warning(const char *str, ...);

	void Info(const char *str, ...);
}

#endif
