/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "Transform.h"

namespace Render
{
	Transform::Transform(){}
	Transform::Transform(const Mat4 &m) : m(m), mInv(m.Inverse()) {}
	Transform::Transform(const Mat4 &m, const Mat4 &mInv) : m(m), mInv(mInv) {}

	Transform Translate(const Vector &d)
	{
		return Transform(	Mat4(	1.f, 0, 0, d.x,
									0, 1.f, 0, d.y,
									0, 0, 1.f, d.z,
									0, 0, 0, 1.f),

							Mat4(	1.f, 0, 0, -d.x,
									0, 1.f, 0, -d.y,
									0, 0, 1.f, -d.z,
									0, 0, 0, 1.f));
	}

	Transform RotateX(float radians)
	{
		float sin = ::sin(radians);
		float cos = ::cos(radians);

		return Transform(	Mat4(	1.f, 0, 0, 0,
									0, cos, sin, 0,
									0, -sin, cos, 0,
									0, 0, 0, 1.f),

							Mat4(	1.f, 0, 0, 0,
									0, cos, -sin, 0,
									0, sin, cos, 0,
									0, 0, 0, 1.f));
	}

	Transform RotateY(float radians)
	{
		float sin = ::sin(radians);
		float cos = ::cos(radians);
		
		return Transform(	Mat4(	cos, 0, -sin, 0,
									0, 1.f, 0, 0,
									sin, 0, cos, 0,
									0, 0, 0, 1.f),

							Mat4(	cos, 0, sin, 0,
									0, 1.f, 0, 0,
									-sin, 0, cos, 0,
									0, 0, 0, 1.f));
	}

	Transform RotateZ(float radians)
	{
		float sin = ::sin(radians);
		float cos = ::cos(radians);
		return Transform(	Mat4(	cos, -sin, 0, 0,
									sin, cos, 0, 0,
									0, 0, 1.f, 0,
									0, 0, 0, 1.f),

							Mat4(	cos, sin, 0, 0,
									-sin, cos, 0, 0,
									0, 0, 1.f, 0,
									0, 0, 0, 1.f));
	}

	Transform Rotate(const Vector &v, float radians)
	{
		Vector a = Normalize(v);
		float s = ::sin(radians);
		float c = ::cos(radians);

		/* To rotate a vector v about an axis a, first project v onto a:
		 * vc = a * ( Dot(a, v))
		 * Next, find axes perpendicular to a to form a basis:
		 * v1 = v - vc
		 * v2 = Cross(v1, a)
		 * Finally, compute the rotation for v1, v2 about a:
		 * v' = vc + v1 * cos(rad) + v2 * sin(rad)
		 * Note: Imagine a circle around a where v1 and v2 touch its circumference.  
		 * That circle is given by v1 * cos(t) + v2 * sin(t).  Then applying this
		 * to the basis vectors will convert this to a rotation matrix.
		 */

		Mat4 m(	a.x * a.x + (1.f - a.x * a.x) * c,
				a.x * a.y * (1.f - c) + a.z * s,
				a.x * a.z * (1.f - c) - a.y * s,
				0,
				a.x * a.y * (1.f - c) - a.z * s,
				a.y * a.y + (1.f - a.y * a.y) * c,
				a.y * a.z * (1.f - c) + a.x * s,
				0,
				a.x * a.z * (1.f - c) + a.y * s,
				a.y * a.z * (1.f - c) - a.x * s,
				a.z * a.z + (1.f - a.z * a.z) * c,
				0,
				0, 0, 0, 1.f);

		return Transform(m, m.Transpose());
	}

	Transform Scale(float x, float y, float z)
	{
		return Transform(	Mat4(	x, 0, 0, 0,
									0, y, 0, 0,
									0, 0, z, 0,
									0, 0, 0, 1.f),

							Mat4(	1.f / x, 0, 0, 0,
									0, 1.f / y, 0, 0,
									0, 0, 1.f / z, 0,
									0, 0, 0, 1.f));
	}

	Transform Scale(const Vector &s)
	{
		return Transform(	Mat4(	s.x, 0, 0, 0,
									0, s.y, 0, 0,
									0, 0, s.z, 0,
									0, 0, 0, 1.f),

							Mat4(	1.f / s.x, 0, 0, 0,
									0, 1.f / s.y, 0, 0,
									0, 0, 1.f / s.z, 0,
									0, 0, 0, 1.f));
	}

	Transform LookAt(const Point &p, const Point &at, const Vector &up)
	{
		Vector nForward = Normalize(p - at);
		Vector nRight = Normalize(Cross(Normalize(up), nForward));
		Vector nUp = Cross(nForward, nRight);

		Mat4 m(	nRight.x, nUp.x, nForward.x, p.x,
				nRight.y, nUp.y, nForward.y, p.y,
				nRight.z, nUp.z, nForward.z, p.z,
				0, 0, 0, 1.f);

		return m;
	}

	bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float *x0, float *x1)
	{
		float invDet = A[0][0] * A[1][1] - A[0][1] * A[1][0];
		if(abs(invDet) < EPSILON)
			return false;

		invDet = 1.f / invDet;
		*x0 = (A[1][1] * B[0] - A[0][1] * B[1]) * invDet;
		*x1 = (A[0][0] * B[1] - A[1][0] * B[0]) * invDet;

		return !(isNaN(*x0)||isNaN(*x1));
	}

	bool Transform::SwapsHandedness() const
	{
		return	m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
				m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
				m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]) < 0;
	}

	bool Transform::HasScale() const
	{
		float lA = (Vector(1.f, 0, 0)).Length2();
		float lB = (Vector(0, 1.f, 0)).Length2();
		float lC = (Vector(0, 0, 1.f)).Length2();
#		define NOT_ONE(v) ((v) >= 1.f + EPSILON || (v) <= 1.f - EPSILON)
		return NOT_ONE(lA) || NOT_ONE(lB) || NOT_ONE(lC);
#		undef NOT_ONE
	}

	void InterpolatedTransform::Decompose(const Mat4 &m, Vector *T, Quaternion *R, Mat4 *S)
	{
		// Extract translation _T_ from transformation matrix.
		T->x = m[0][3];
		T->y = m[1][3];
		T->z = m[2][3];

		// Compute new transformation matrix _M_ without translation.
		Mat4 M = m;
		for(int i = 0; i < 3; ++i)
			M[i][3] = M[3][i] = 0.f;
		M[3][3] = 1.f;

		//Extract rotation _R_ from transformation matrix.
		float norm;
		int count = 0;
		Mat4 Rmat = M;
		do
		{
			// Compute next matrix _Rnext_ in series.
			Mat4 Rnext;
			Mat4 Rit = Rmat.Transpose().Inverse();
			// The matrix is affine with no translations, so the 4th rows and columns never change.
			for(int i = 0; i < 3; ++i)
				for(int j = 0; j < 3; ++j)
					Rnext[i][j] = 0.5f * (Rmat[i][j] + Rit[i][j]);

			// Compute norm of difference between _Rmat_ and _Rnext_.
			norm = 0.f;
			for(int i = 0; i < 3; ++i)
			{
				float n =	abs(Rmat[i][0] - Rnext[i][0]) +
							abs(Rmat[i][1] - Rnext[i][1]) +
							abs(Rmat[i][2] - Rnext[i][2]);

				norm = max(norm, n);
			}

			Rmat = Rnext;
		} while( ++count < 100 && norm > .0001f );

		*R = Quaternion(Rmat);

		// Compute scale _S_ using rotation and original matrix.
		*S = Rmat.Inverse() * M;
	}

	InterpolatedTransform::InterpolatedTransform(const Transform &startTrans, float start, const Transform &endTrans, float end)
		:	startTrans(startTrans), start(start), endTrans(endTrans), end(end), invTime(1.f / (end - start))
	{
		Decompose(startTrans.m, &startDec.T, &startDec.R, &startDec.S);
		Decompose(endTrans.m, &endDec.T, &endDec.R, &endDec.S);

		isAnimated = startTrans != endTrans;
		if(!isAnimated)
			return;

		translates = memcmp(&startDec.T.x, &endDec.T.x, sizeof(float) * 3) != 0;
		rotates = memcmp(&startDec.R.v.x, &endDec.R.v.x, sizeof(float) * 3) != 0 || startDec.R.w != endDec.R.w;
		scales = startDec.S != endDec.S;

		if(rotates)
		{
			qCosTheta = Dot(startDec.R, endDec.R);
			qTheta = acosf(Clamp(qCosTheta, -1.f, 1.f));
			qPerp = Normalize(endDec.R - startDec.R * qCosTheta);
			invTime = 1.f / (end - start);
		}
	}

	void InterpolatedTransform::Interpolate(float time, Transform *t) const
	{
		if(!isAnimated || time <= start)
		{
			*t = startTrans;
			return;
		}
		else if(time >= end)
		{
			*t = endTrans;
			return;
		}

		float dt = (time - start) * invTime;
		*t = Translate(translates ? (1.f - dt) * startDec.T + dt * endDec.T : startDec.T);

		if(rotates)
		{
			if(qCosTheta > 0.9995f)
				*t = *t * Normalize((1.f - dt) * startDec.R + dt * endDec.R).ToTransform();
			else
			{
				float thetap = dt * qTheta;
				*t = *t * (startDec.R * cos(thetap) + qPerp * sin(thetap)).ToTransform();
			}
		}
		else
			*t = *t * startDec.R.ToTransform();

		if(scales)
		{
			Mat4 s;
			for(int i = 0; i < 4; ++i)
				for(int j = 0; j < 4; ++j)
					s[i][j] = Lerp(dt, startDec.S[i][j], endDec.S[i][j]);

			*t = *t * Transform(s);
		}
		else
			*t = *t * Transform(startDec.S);
	}
	AnimatedTransform::AnimatedTransform(const vector<const Transform*> &tfs, const vector<float> &times)
		:	times(times)
	{
		if(tfs.size() == 0)
		{
			isIdentity = isSingular = true;
			return;
		}

		isSingular = tfs.size() == 1 || ( tfs.size() == 2 && tfs.front() == tfs.back() );
		if(isSingular)
		{
			isIdentity = *tfs.front() == Mat4::identity;
			if(!isIdentity)
				transforms.push_back(InterpolatedTransform(*tfs.front(), times.front(), *tfs.front(), times.front()));

			return;
		}
		else
			isIdentity = false;

		bool warned = false;
		for(size_t i = 0; i < tfs.size() - 1; ++i)
		{
			if(times[i] > times[i + 1] && warned)
			{
				warned = true;
				Warning("Unsorted timing detected in AnimatedTransform::AnimatedTransform.  The system assumes animation times are sorted.  Errors may occur.");
			}
			transforms.push_back(InterpolatedTransform(*tfs[i], times[i], *tfs[i + 1], times[i + 1]));
		}
	}

	void AnimatedTransform::Interpolate(float time, Transform *t) const
	{
		if(isIdentity)
		{
			*t = Transform();
			return;
		}

		if(isSingular || (transforms.size() == 1 && !transforms.front().isAnimated) || time <= times.front())
		{
			*t = transforms.front().startTrans;
			return;
		}
		else if(time >= times.back())
		{
			*t = transforms.back().endTrans;
			return;
		}

		size_t i = lower_bound(times.begin(), times.end(), time) - times.begin() - 1;
		transforms[i].Interpolate(time, t);
	}

	BBox AnimatedTransform::MotionBounds(const BBox &b, bool useInverse) const
	{
		if(isIdentity)
			return b;

		if(isSingular || transforms.size() == 1 && !transforms.front().isAnimated)
			return transforms.front().startTrans(b, useInverse);

		BBox motionBounds;
		Transform t;
		for(size_t i = 0; i < transforms.size(); ++i)
		{
			if(!transforms[i].isAnimated)
			{
				motionBounds = Union(motionBounds, transforms[i].startTrans(b, useInverse));
				continue;
			}
			
			//Note: 1 / 128 = 0.0078125.
			float increment = (transforms[i].end - transforms[i].start) * 0.0078125f;
			for(float j = transforms[i].start; j < transforms[i].end + increment; j += increment)
			{
				transforms[i].Interpolate(j, &t);
				motionBounds = Union(motionBounds, t(b, useInverse));
			}
		}

		return motionBounds;
	}
}