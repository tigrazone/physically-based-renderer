/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_TEXTURE_H
#define RENDERLIB_CORE_TEXTURE_H

#include "Transform.h"
#include "DifferentialGeometry.h"
#include "Memory.h"

//2 * pi ^ 2
#define TWOPI2 19.73920880217871723767f

namespace Render
{
	typedef Reference<Texture<Spectrum>> RSpectrumTexture;
	typedef Reference<Texture<float>> RFloatTexture;

	class TextureMapping2D
	{
	public:
		virtual void Map(const DifferentialGeometry &dg, float *s, float *t) const = 0;
		virtual ~TextureMapping2D();
	};

	class UVMapping2D : public TextureMapping2D
	{
		float su, sv, du, dv;
	public:
		UVMapping2D(float su = 1.f, float sv = 1.f, float du = 0.f, float dv = 0.f);
		void Map(const DifferentialGeometry &dg, float *s, float *t) const;
	};

	class SphericalMapping2D : public TextureMapping2D
	{
		float su, sv, du, dv;
		Transform toTexture;
	public:
		SphericalMapping2D(const Transform &toTexture, float su = 1.f, float sv = 1.f, float du = 0.f, float dv = 0.f);
		void Map(const DifferentialGeometry &dg, float *s, float *t) const;
	};

	class CylindricalMapping2D : public TextureMapping2D
	{
		float su, sv, du, dv;
		Transform toTexture;
	public:
		CylindricalMapping2D(const Transform &toTexture, float su = 1.f, float sv = 1.f, float du = 0.f, float dv = 0.f);
		void Map(const DifferentialGeometry &dg, float *s, float *t) const;
	};

	class PlanarMapping2D : public TextureMapping2D
	{
		Vector vu, vv;
		float du, dv;
	public:
		PlanarMapping2D(const Vector &vu, const Vector &vv, float du = 0.f, float dv = 0.f);
		void Map(const DifferentialGeometry &dg, float *s, float *t) const;
	};

	template<typename T>
	class Texture : public ReferenceCounted
	{
	public:
		virtual T Evaluate(const DifferentialGeometry &dg) const = 0;
		virtual T Avg() const = 0;
		virtual ~Texture() {}
	};

	template<typename T>
	class ConstantTexture : public Texture<T>
	{
		const T val;
	public:
		ConstantTexture(const T &val) : val(val){}
		T Evaluate(const DifferentialGeometry &dg) const { return val; }
		T Avg() const { return val; }
	};

	class EnvironmentMapping
	{
	public:
		virtual ~EnvironmentMapping();
		//Certain mappings (eg vertical cross) don't use the entire image.  However they can adjust the mapping to use the full extent of uniform samples.
		virtual void Map(float s, float t, float *sm, float *tm, Vector *wh, float *pdf = NULL) const = 0;
		virtual void Map(const Vector &wh, float *s, float *t, float *pdf = NULL) const = 0;
		virtual float Pdf(float s, float t, float *sm, float *tm) const = 0;
		virtual ImageWrap GetWrapMode() const;
		virtual void GetMapExtent(uint32_t width, uint32_t height, uint32_t *nu, uint32_t *nv) const;
	};

	class LatLongMapping : public EnvironmentMapping
	{
	public:
		void Map(float s, float t, float *sm, float *tm, Vector *wh, float *pdf) const;
		void Map(const Vector &wh, float *s, float *t, float *pdf) const;
		float Pdf(float s, float t, float *sm, float *tm) const;
	};

	class VerticalCrossMapping : public EnvironmentMapping
	{
	public:
		void Map(float s, float t, float *sm, float *tm, Vector *wh, float *pdf) const;
		void Map(const Vector &wh, float *s, float *t, float *pdf) const;
		float Pdf(float s, float t, float *sm, float *tm) const;
		void GetMapExtent(uint32_t width, uint32_t height, uint32_t *nu, uint32_t *nv) const;
	};
}
#endif