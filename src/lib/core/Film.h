/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef RENDERLIB_CORE_FILM_H
#define RENDERLIB_CORE_FILM_H

#include "Spectrum.h"
#include "Sampler.h"

namespace Render
{
	/* Class that stores resulting render data */
	class Film
	{
	public:
		const unsigned int width, height;
		Film(unsigned int width, unsigned int height);
		virtual ~Film();

		virtual void WriteImage(const char *filename) const = 0;

		/* Return XYZ tristimulus color coordinates, where the fourth channel is the number of samples. */
		virtual const float *GetPixelData() const = 0;
		virtual void AddSample(const Sample *sample, const Spectrum &s) = 0;
		virtual void SplatSample(float x, float y, const Spectrum &s) = 0;
	};
}

#endif