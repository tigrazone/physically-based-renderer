/* Copyright (C) 2014 by authors (see AUTHORS.txt)
 * For conditions of distribution and use, see copyright notice in Global.h.
 */

#include "TexImage.h"

namespace Render
{
	void TexInfo::Convert(const float s[3], float *out, float scale, float gamma)
	{
		*out = pow(Spectrum::FromRGB(s).y() * scale, gamma);
	}

	void TexInfo::Convert(const float s[3], RGBSpectrum *out, float scale, float gamma)
	{
		*out = Pow(RGBSpectrum::FromRGB(s) * scale, gamma);
	}
}