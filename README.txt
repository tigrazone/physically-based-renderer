Physically Based Renderer

This project is a physically based renderer and is unimaginatively
named as such for the time being.  It uses Monte Carlo integration to
estimate the solution to the lighting integral.  It sports a very 
primitive GUI based on OpenGL to preview renders, and as such, still
uses a console to display various pieces of information.  Binaries
exist for both x86 and x64 architectures.

Despite using OpenGL for GUI purposes, it is still a CPU renderer.

Latest source and binaries can be found at: https://bitbucket.org/yjd/physically-based-renderer

--- Sample Scenes ---

The sample .lua scenes will not include geometry or texture files (on
account of them being large).  For details, go to the Organization
section.

--- Requirements ---

- GLEW 1.10.0 (http://glew.sourceforge.net)
- BOOST 1.53 (http://www.boost.org)
- C++ 11 compiler
- Image viewer capable of reading .exr files

--- Setup - Microsoft Visual Studio Express 2012 ---

This project uses boost and GLEW.  They are linked via property
sheets so these steps may be necessary:

- Make sure glew32.dll files are placed in the proper system folders for both x86 and x64 architectures.
- Compile or download static libraries for boost, as well as the headers.
- Property sheets can be found at https://bitbucket.org/yjd/physically-based-renderer/downloads.  Their directories need to be updated to correspond to the local setup.
- The solution file needs to point to the property sheets.

--- Organization ---

src/ - All source code, including resources for icons and Visual 
Studio projects.  The solution file is in the root directory however.

scenes/ - Sample scenes.  3D meshes are not included due to their 
large file size but are usually found at 
http://graphics.stanford.edu/data/3Dscanrep/.

Likewise for environment maps.  Most of those can be found at
http://www.hdrlabs.com/sibl/archive.html though will need to be 
converted to .exr format.  Others are prevalent from university sites.

bin/ - Compiled binaries.

--- Authors ---

See AUTHORS.txt.

--- License ---

See LICENSE.txt.

--- Credits ---

This project is based on Physically Based Rendering Second Edition by
Matt Pharr and Greg Humphreys - pbrt.org

GLEW Authors - http://glew.sourceforge.net

OpenEXR authors - http://openexr.com

The Stanford 3D Scanning Repository - http://graphics.stanford.edu/data/3Dscanrep/

RPly author Diego Nehab - http://w3.impa.br/~diego/software/rply/

zlib authors - http://zlib.net

libpng authors - http://libpng.org

lua authors - http://lua.org

HDR map source - http://www.hdrlabs.com/sibl/archive.html